<?php

set_time_limit(0);

define( 'DS' , DIRECTORY_SEPARATOR );
// define( 'APP_ROOT' , dirname( dirname( __FILE__ ) ) . DS  );
define( 'APP_ROOT' , dirname( __FILE__ ) . DS  );

date_default_timezone_set("Asia/Shanghai");
$action = empty( $_REQUEST['ajax'] ) ? '' : strtolower( $_REQUEST['ajax'] );
if($action){
	if($action=='js'){
		$ContentType ='application/x-javascript';
	}else{
		$ContentType ='text/plain';
	}
}else{
	$ContentType ='text/html';
}
header("Content-Type:".$ContentType.";charset=utf-8");

// define( 'KIS_APP_ROOT' , dirname( APP_ROOT ) . DS );
define( 'KIS_APP_ROOT' , APP_ROOT . 'php'.DS);
// include_once KIS_APP_ROOT.'ext_lib'. DS.'comm.function.php';
define( 'AROOT' , KIS_APP_ROOT);
define( 'CROOT' , KIS_APP_ROOT.'_lp'. DS.'core'. DS  );
include_once CROOT. 'lib'.DS.'core.function.php';
// include_once( KIS_APP_ROOT. 'ext_lib'.DS .'mail.config.inc.php' );
include_once KIS_APP_ROOT.'lib'. DS.'app.function.php';
include_once KIS_APP_ROOT.'model'. DS.'api.function.php';

include_once 'php/comm.inc.php';
include_once 'php/map.inc.php';
include_once 'php/func.inc.php';
include_once 'php/web.inc.php';
// include_once 'php/comm.function.php';

define('TBS_ROOT', KIS_APP_ROOT.'_lp'.DS );
define('TPLROOT', dirname( __FILE__ ) . DS. 'html'.DS );

define('APP_ENVIRONMENT', 'DEVELOPMENT');
// define('APP_ENVIRONMENT', 'PRODUCT');
// define('KIS_DEBUG_MODE', 'DISABLE_DEBUG');

define( 'CACHE_DIR' , 'tmp'.DS);
define( 'CACHE_ROOT' , APP_ROOT . CACHE_DIR);


$process_step_id=0;
$new_load_page_count=0;
/**

**/

function load_book_page($pageUrl){
  global $configArr,$new_load_page_count;
  $cacheFN = get_cache_FN($pageUrl);
  if(file_exists($cacheFN)){
    $listSrc=file_get_contents($cacheFN);
  }else{
    sleep(3);
    $listSrc=retry_load_page($pageUrl,$cacheFN,3);
    if(!empty($listSrc)){
      $new_load_page_count+=1;
      file_put_contents($cacheFN,$listSrc);
    }else{
      die(__METHOD__.'() Error #'.$pageUrl);
    }
  }
  if(!isset($configArr['pageArr'])){
    $configArr['pageCount']=0;
    $configArr['pageArr']=array();
  }
  $miniCacheFN=get_mini_cache_FN($pageUrl);
  if(!array_key_exists($miniCacheFN, $configArr['pageArr'])){
    $configArr['pageArr'][$miniCacheFN]=$pageUrl;
    $configArr['pageCount']=count($configArr['pageArr']);
  }
  return $listSrc;
}

function wbd_config($key){
	global $configArr;
	if(isset($configArr[$key])){
		return $configArr[$key];
	}
	return false;
}

function show_config(){
	global $configArr;
	debug_span(print_r($configArr,true));
	$line_count=count($configArr);
	echo '<textarea class="comments" rows="'.($line_count+3).'" >'.PHP_EOL;		
	echo print_r($configArr,true);
	echo '</textarea>'.PHP_EOL;
	return false;
}

function save_config(){
	global $configArr,$configFN;
	file_put_contents($configFN,json_encode($configArr));
}

function main(){
	global $debugMode, $c,$a, $outVar;
    global $process_step_id,$new_load_page_count;

	$process_step_id=100;
	$listUrl=wbd_config('listUrl');
	$cacheFN=wbd_config('cacheFN');
	if(file_exists($cacheFN)){
		$listSrc=file_get_contents($cacheFN);
	}else{
		$listSrc=file_get_contents($listUrl);
		if(!empty($listSrc)){
			file_put_contents($cacheFN,$listSrc);
		}else{
			die(__METHOD__.'() Error #'.$process_step_id);
		}
	}

/*
	$kk=$tmp_src;
	$kk=addslashes($kk);
	$kk=func_str_compress($kk);

    $html_src = func_str_un_compress($ret_sql['html_src']);
    $html_src = stripslashes($html_src);
*/	

// INSERT INTO `wbd_jobs`(`uuid`, `is_done`, `ud1`, `ud2`, `ud3`, `ud4`, `ud5`, `ud6`, `ud7`, `ud8`, `ud9`, `cf1`, `cf2`, `job_url`, `html_src`) VALUES ([value-1],[value-2],[value-3],[value-4],[value-5],[value-6],[value-7],[value-8],[value-9],[value-10],[value-11],[value-12],[value-13],[value-14],[value-15])

	// $kk=$listSrc;
	// $kk=addslashes($kk);
	// $kk=func_str_compress($kk);
	// $tmpSql = prepare("INSERT INTO `wbd_jobs`(`uuid`, `is_done`, `ud1`,`cf1`, `cf2`, `job_url`, `html_src`) VALUES (?s,?s,?s,?s,?s,?s,?s",array(md5($listUrl),1,$cf_now,$cf_now,$listUrl,$kk));
	// run_sql($tmpSql);

	add_jobs($listUrl,$listSrc,0,1);

	$process_step_id +=1;

	$site_root_url=wbd_config('site_root_url');

	//cut body
	$FGF=wbd_config('body_start_html');
	$tmpSrcArr=explode($FGF,$listSrc);
	if(count($tmpSrcArr)>1){
		$tmpSrc=$tmpSrcArr[1];
	}else{
		die(__METHOD__.'() Error #'.$process_step_id);
	}
	$process_step_id +=1;

	//cut body
	$FGF=wbd_config('body_end_html');
	$tmpSrcArr=explode($FGF,$tmpSrc);
	if(count($tmpSrcArr)>1){
		$tmpSrc=$tmpSrcArr[0];
	}else{
		die(__METHOD__.'() Error #'.$process_step_id);
	}
	$process_step_id +=1;


	//echo $listSrc;
	$FGF=wbd_config('list_fgf');
	$key1=wbd_config('list_url_start');
	$key2=wbd_config('list_url_end');
	$book_key=wbd_config('book_key');
	$srcArr=explode($FGF,$tmpSrc);
	//<dd><a style="" href="/id34100/1817834.html">第二百九十九章 杀向冰火岛(文)</a></dd>  
	$line_idx=0;
	foreach($srcArr as $tmpSrc){
		if(stripos($tmpSrc,$book_key)!==false){
			$tmpScrPart=$FGF.$tmpSrc;
			// echo '#'.$line_idx.' '.$tmpScrPart.PHP_EOL;
			echo '<br/>#'.$line_idx.' '.PHP_EOL;
			debug_span($tmpScrPart);
			$srcUrl=_cut_middle_str($tmpScrPart,$key1,$key2);
			echo red_span($srcUrl).PHP_EOL;
			$pageUrl=$site_root_url.$srcUrl;
			// $pageUrl=str_replace('//', '/', $pageUrl);
			$book_page_src=load_book_page($pageUrl);
			$pageCacheLink = get_cache_link($pageUrl);

			add_jobs($pageUrl,$book_page_src,0,2);
			
			echo '<a href="'.$pageCacheLink.'" target="_blank">cache #'.$line_idx.'</a>'.PHP_EOL;
			echo '<a href="'.$pageUrl.'" target="_blank">原文 #'.$line_idx.'</a>'.PHP_EOL;
			$line_idx++;
			if($new_load_page_count>100) sleep(6);
		}
	}

	save_config();
	show_config();
}



/**

*/
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>WBD 网络小说下载</title>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<!-- <link href="style.css" rel="stylesheet" type="text/css" media="screen" /> -->
	<link rel="stylesheet" type="text/css" href="html/res/css/style.css" />
</head>
<body>

<?php
/**

*/

$configFN=CACHE_ROOT.'config.json';

$tt=v('a');
$a=strtolower($tt);
switch ($a) {
	case 'config':
/**

**/	
		$configArr=array();

		$listUrl='http://www.mangg.com/id34100/';
		$configArr['listUrl']=$listUrl;

		$cacheFN=get_cache_FN($listUrl);
		$configArr['cacheFN']=$cacheFN;

		//<div class="book_article_title">小说介绍：</div>

		$configArr['site_root_url']='http://www.mangg.com/';

		$configArr['book_key']='id34100';

		$configArr['body_start_html']='book_article_title';
		$configArr['body_end_html']='class="footer';

		$configArr['list_fgf']='<dd><a style="';
		$configArr['list_url_start']='href="';
		$configArr['list_url_end']='"';

		save_config();
		show_config();
		break;
	
	case 'site_config':
/**
<SiteSign>
	<name>纵横中文</name>
	<url>http://book.zongheng.com/</url>
	<ListStart>&lt;div class="chapter"&gt;</ListStart>
	<ListEnd>&lt;!-- 章节列表 结束 --&gt;</ListEnd>
	<ContentStart>itemprop="acticleBody"&gt;</ContentStart>
	<ContentEnd>&lt;/div&gt;</ContentEnd>
	<NeedDelStr>[[&lt;span c[\s\S]+?&lt;/span&gt;]]||[[&lt;img class="wb.*?fudai.gif"/&gt;]]</NeedDelStr>
	<VolumeStart>&lt;h2&gt;</VolumeStart>
	<VolumeEnd>&lt;/h2&gt;</VolumeEnd>
	<BriefUrlStart>&lt;h1&gt;</BriefUrlStart>
	<BriefUrlEnd>&lt;/h1&gt;</BriefUrlEnd>
	<AuthorStart>作者：</AuthorStart>
	<AuthorEnd>&lt;/a&gt;</AuthorEnd>
	<BriefStart>&lt;div class="jianjie tabpanel" tabId="1"&gt;</BriefStart>
	<BriefEnd>&lt;div</BriefEnd>
	<BookImgUrlStart>书籍作品信息 --&gt;</BookImgUrlStart>
	<BookImgUrlEnd>&lt;/div&gt;</BookImgUrlEnd>
</SiteSign>
**/	
		$siteConfigArr=array();
		$siteConfigArr['name']='';
		$siteConfigArr['url']='';
		$siteConfigArr['ListStart']='';
		$siteConfigArr['ListEnd']='';
		$siteConfigArr['ContentStart']='';
		$siteConfigArr['ContentEnd']='';
		$siteConfigArr['NeedDelStr']='';
		$siteConfigArr['VolumeStart']='';
		$siteConfigArr['VolumeEnd']='';
		$siteConfigArr['BriefUrlStart']='';
		$siteConfigArr['BriefUrlEnd']='';
		$siteConfigArr['AuthorStart']='';
		$siteConfigArr['AuthorEnd']='';
		$siteConfigArr['BriefStart']='';
		$siteConfigArr['BriefEnd']='';
		$siteConfigArr['BookImgUrlStart']='';
		$siteConfigArr['BookImgUrlEnd']='';

		// if(!empty($siteConfigArr['url'])){
		// 	$uuid=md5($siteConfigArr['url']);
		// 	$tmp_sql = prepare('SELECT count(*) as num FROM `'.$table_name.'` WHERE `code`=?s order by `cf1`, `cf2` limit 1' ,array($major_id));
		// }
				

		save_config();
		show_config();
		break;
	case 'index':
		$json=file_get_contents($configFN);
		$configArr=json_decode($json,true);
		main();
		break;

	case 'token':
		$api_url = 'http://longmenxuezi.com/api2016.php';
		$info = json_decode(file_get_contents( $api_url . '?c=api&a=user_get_token&password=***&email=13912349609' ) , 1);
		// print_r( $info ); 
		$token = $info['data']['token'];
		$todos = json_decode(file_get_contents( $api_url . '?c=api&a=todo_list&token=' . $token ) , 1);
		print_r( $todos ); 
		break;

	case 'loadsite':
/**
	<SiteSign><name>纵横中文</name><url>http://book.zongheng.com/</url><ListStart>&lt;div class="chapter"&gt;</ListStart><ListEnd>&lt;!-- 章节列表 结束 --&gt;</ListEnd><ContentStart>itemprop="acticleBody"&gt;</ContentStart><ContentEnd>&lt;/div&gt;</ContentEnd><NeedDelStr>[[&lt;span c[\s\S]+?&lt;/span&gt;]]||[[&lt;img class="wb.*?fudai.gif"/&gt;]]</NeedDelStr><VolumeStart>&lt;h2&gt;</VolumeStart><VolumeEnd>&lt;/h2&gt;</VolumeEnd><BriefUrlStart>&lt;h1&gt;</BriefUrlStart><BriefUrlEnd>&lt;/h1&gt;</BriefUrlEnd><AuthorStart>作者：</AuthorStart><AuthorEnd>&lt;/a&gt;</AuthorEnd><BriefStart>&lt;div class="jianjie tabpanel" tabId="1"&gt;</BriefStart><BriefEnd>&lt;div</BriefEnd><BookImgUrlStart>书籍作品信息 --&gt;</BookImgUrlStart><BookImgUrlEnd>&lt;/div&gt;</BookImgUrlEnd></SiteSign>
**/
		$tmpFN='misc/sitesign_utf8.xml';
		if(file_exists($tmpFN)){
			$sitesign=file_get_contents($tmpFN);
			if(empty($sitesign)){
				echo red_span(__METHOD__.'()'.$a. ' Error !!! #2').PHP_EOL;	
				break;
			}
			$delimiter='<SiteSign>';
			$siteArr=explode($delimiter, $sitesign);
			$line_idx=0;

			ob_implicit_flush(true);

			$buffFN='tmp/buff.json';
			if(file_exists($buffFN)){
				$json=file_get_contents($buffFN);
				$buffArr=json_decode($json,true);
			}else{
				$buffArr=array();	
			}			

			foreach ($siteArr as $key => $value) {
				if(stripos($value, '<url>')!=false){
					$flag=0;
					$tmpName=_cut_middle_str($value,'<name>','</name>');
					$tmpUrl=_cut_middle_str($value,'<url>','</url>');
					$tmpUrl2=$tmpUrl;
					if(stripos($tmpUrl, '*.')!==false){
						echo red_span(__METHOD__.'(*.)'.$a. $tmpUrl).PHP_EOL;	
						$tmpUrl=str_ireplace('*.', 'www.', $tmpUrl);
					}
					$miniCacheFN = get_mini_cache_FN($tmpUrl);
					$cacheFN = get_cache_FN($tmpUrl);
					if(!in_array($miniCacheFN, $buffArr)){
						$buffArr[]=$miniCacheFN;
						file_put_contents($buffFN, json_encode($buffArr));

						$cacheFN = get_cache_FN($tmpUrl);
						$tmpSrc=retry_load_page($tmpUrl,$cacheFN,3,0);
						
						if(!empty($tmpSrc) && strlen($tmpSrc)>5000){
							echo $tmpName . ' check ok! ' .PHP_EOL;
							$flag=1;
						}else{
							echo $tmpName . ' check Error! ' .PHP_EOL;
						}
					}

					if($flag==0){

						if(file_exists($cacheFN)){
							$tmpSrc = file_get_contents($cacheFN);
							
							if(!empty($tmpSrc) && strlen($tmpSrc)>5000){
								echo $tmpName . ' check ok!  #2' .PHP_EOL;
								$flag=2;
							}else{
								echo $tmpName . ' check Error! #2 ' .PHP_EOL;
							}							
						}else{
							echo $tmpName . ' check Error! #3 ' .PHP_EOL;
						}

					}else{
						//skip , $flag=1 is ok.
					}
					
					$pageCacheLink = get_cache_link($tmpUrl);
					$cf_now = date('Y-m-d H:i:s');
					
/*
UPDATE `wbd_sitesign` SET `id`=[value-1],`url_md5`=[value-2],`name`=[value-3],`url`=[value-4],`ListStart`=[value-5],`ListEnd`=[value-6],`ContentStart`=[value-7],`ContentEnd`=[value-8],`NeedDelStr`=[value-9],`VolumeStart`=[value-10],`VolumeEnd`=[value-11],`BriefUrlStart`=[value-12],`BriefUrlEnd`=[value-13],`AuthorStart`=[value-14],`AuthorEnd`=[value-15],`BriefStart`=[value-16],`BriefEnd`=[value-17],`BookImgUrlStart`=[value-18],`BookImgUrlEnd`=[value-19],`cf1`=[value-20],`is_his`=[value-21] WHERE 1
*/
					if($flag>0){
						echo red_span('<a href="'.$pageCacheLink.'" target="_blank">cache #'.$line_idx.'</a>').PHP_EOL;
						
						$tmpSql = prepare("UPDATE `wbd_sitesign` SET `url_md5`=?s,`cf1`=?s,`is_his`=?i WHERE `url`=?s limit 1 ",array(md5($tmpUrl2),$cf_now,0,$tmpUrl2));
					}else{
						$tmpSql = prepare("UPDATE `wbd_sitesign` SET `url_md5`=?s,`cf1`=?s,`is_his`=?i WHERE `url`=?s limit 1 ",array(md5($tmpUrl2),$cf_now,1,$tmpUrl2));
					}
					run_sql($tmpSql);
					echo '<a href="'.$tmpUrl.'" target="_blank">原文 #'.$line_idx.'</a><br/><br/>'.PHP_EOL;
					ob_flush();
					$line_idx++;
				}
			}

			echo red_span(__METHOD__.'()'.$a. ' Process End!').PHP_EOL;	

		}else{
			echo red_span(__METHOD__.'()'.$a. ' Error !!! #1').PHP_EOL;	
			break;
		}

		break;

	case 'autoload':			//not ok
		$json=file_get_contents($configFN);
		$configArr=json_decode($json,true);

		if(!isset($configArr['pageDoneArr'])){
			$configArr['pageDoneArr']=array();
		}
		$autoload_max_count=10;
		foreach ($configArr['pageArr'] as $pageUrlKey => $pageUrlValue) {
			if(!array_key_exists($pageUrlKey, $configArr['pageDoneArr'])){
				$configArr['pageArr'][$miniCacheFN]=$pageUrl;
				$configArr['pageCount']=count($configArr['pageArr']);
				$autoload_max_count--;
				if($autoload_max_count<0){
					break;
				}
			}
		}
		break;

	default:
		// $json=file_get_contents($configFN);
		// $configArr=json_decode($json,true);
		// main();
		echo red_span(__METHOD__.'() Error !!!').PHP_EOL;
		break;
}

/**

*/
?>
</body>
</html>