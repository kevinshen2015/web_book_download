<?php

ini_set('session.cookie_path', '/');
ini_set('session.cookie_domain', '.longmenxuezi.com'); //注意***.com换成你自己的域名
ini_set('session.cookie_lifetime', '1800');


/* lp app root */
// ↑____ for aoi . Do Not Delete it.
/****  load lp framework  ***/
define( 'DS' , DIRECTORY_SEPARATOR );

define( 'APP_ROOT' , dirname( __FILE__ ) . DS  );
define( 'KIS_APP_ROOT' , APP_ROOT . DS .'php'.DS);
define( 'AROOT' , KIS_APP_ROOT);
// define( 'CROOT' , KIS_APP_ROOT.'_lp'. DS.'core'. DS  );

date_default_timezone_set("Asia/Shanghai");
// define('TBS_ROOT', KIS_APP_ROOT.'_lp'.DS );
define('TPLROOT', dirname( __FILE__ ) . DS. 'html'.DS );

session_start();

include_once 'php/map.inc.php';
include_once 'php/func.inc.php';
include_once 'php/comm.inc.php';
include_once 'php/web.inc.php';

//ini_set('include_path', dirname( __FILE__ ) . DS .'_lp' ); 
include_once( KIS_APP_ROOT.'_lp'.DS .'lp.init.php' );
/**** lp framework init finished ***/