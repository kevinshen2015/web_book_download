<?php
// if(defined('SERVER_SOFTWARE')){
// 	//SERVER_SOFTWARE = bae/3.0
// 	$runtime_env_check=strtolower(SERVER_SOFTWARE);
// 	if(stripos($runtime_env_check,'bae')!==false){
// 		include_once 'php/session.inc.php';
// 	}
// }

ini_set('session.cookie_path', '/');
ini_set('session.cookie_domain', '.longmenxuezi.com'); //注意***.com换成你自己的域名
ini_set('session.cookie_lifetime', '1800');


define( 'DS' , DIRECTORY_SEPARATOR );
// define( 'APP_ROOT' , dirname( dirname( __FILE__ ) ) . DS  );
define( 'APP_ROOT' , dirname( __FILE__ ) . DS  );

date_default_timezone_set("Asia/Shanghai");
$action = empty( $_REQUEST['ajax'] ) ? '' : strtolower( $_REQUEST['ajax'] );
if($action){
	if($action=='js'){
		$ContentType ='application/x-javascript';
	}else{
		$ContentType ='text/plain';
	}
}else{
	$ContentType ='text/html';
}
header("Content-Type:".$ContentType.";charset=utf-8");

// define( 'KIS_APP_ROOT' , dirname( APP_ROOT ) . DS );
define( 'KIS_APP_ROOT' , APP_ROOT . DS .'php'.DS);
// include_once KIS_APP_ROOT.'ext_lib'. DS.'comm.function.php';
define( 'AROOT' , KIS_APP_ROOT);
define( 'CROOT' , KIS_APP_ROOT.'_lp'. DS.'core'. DS  );
include_once CROOT. 'lib'.DS.'core.function.php';
// include_once( KIS_APP_ROOT. 'ext_lib'.DS .'mail.config.inc.php' );
include_once KIS_APP_ROOT.'lib'. DS.'app.function.php';
include_once KIS_APP_ROOT.'model'. DS.'api.function.php';

include_once 'php/comm.inc.php';
include_once 'php/map.inc.php';
include_once 'php/func.inc.php';
include_once 'php/web.inc.php';
// include_once 'php/comm.function.php';

define('TBS_ROOT', KIS_APP_ROOT.'_lp'.DS );
define('TPLROOT', dirname( __FILE__ ) . DS. 'html'.DS );

define('APP_ENVIRONMENT', 'DEVELOPMENT');
// define('APP_ENVIRONMENT', 'PRODUCT');
// define('KIS_DEBUG_MODE', 'DISABLE_DEBUG');


/**

*/

// $db_table_prefix = 'shdic_***_'; // in func.inc.php

function adm_fix_new_school_code(){

	if($ret_count_sql=get_var('SELECT count(*) as num from school_ruanyingjian WHERE length(code)!=32')){
		if(intval($ret_count_sql)==0){
			echo __METHOD__.'() school_ruanyingjian ok!'.PHP_EOL;
		}else{
			echo __METHOD__.'() school_ruanyingjian need fix count='.intval($ret_count_sql).PHP_EOL;
			$sql='SELECT * from school_ruanyingjian WHERE length(code)!=32';
			if($ret_sql=get_data($sql)){
				$last_code=null;
				foreach ($ret_sql as $key => $value) {
					$code=$value['code'];
					$newcode=md5($code);
					if($last_code!=$newcode){

						adm_add_new_school_sub($newcode,$value['title']);

						$tmp_sql="UPDATE school_ruanyingjian SET code=?s WHERE (code=?s) LIMIT 1";
						if($ret_sql2=run_sql(prepare($tmp_sql,array($newcode,$code)) ) ){
							echo $code.'>>'.$newcode.PHP_EOL;
						}
						$last_code=$newcode;
					}
				}
			}
		}
	}


	if($ret_count_sql=get_var('SELECT count(*) as num from school_admission_score WHERE length(code)!=32')){
		if(intval($ret_count_sql)==0){
			echo __METHOD__.'() school_admission_score ok!'.PHP_EOL;
		}else{
			echo __METHOD__.'() school_admission_score need fix count='.intval($ret_count_sql).PHP_EOL;
			$sql='SELECT * from school_admission_score WHERE length(code)!=32';
			if($ret_sql=get_data($sql)){
				$last_code=null;
				foreach ($ret_sql as $key => $value) {			
					$code=$value['code'];
					$newcode=md5($code);
					if($last_code!=$newcode){

						adm_add_new_school_sub($newcode,$value['school_title']);

						$tmp_sql="UPDATE school_admission_score SET code=?s WHERE (code=?s) LIMIT 100";
						if($ret_sql2=run_sql(prepare($tmp_sql,array($newcode,$code)) ) ){
							echo $code.'>>'.$newcode.PHP_EOL;
						}
						$tmp_sql="UPDATE school_admission_score_real SET code=?s WHERE (code=?s) LIMIT 1000";
						if($ret_sql2=run_sql(prepare($tmp_sql,array($newcode,$code)) ) ){
							echo $code.'>>'.$newcode.PHP_EOL;
						}
						$last_code=$newcode;
					}
				}
			}
		}
	}
	
	die(__METHOD__.'() done!');
	
}


function adm_add_new_school_sub($code,$title_full,$title=null){
	if(empty($code)) return false;
	if(empty($title_full)) return false;
	
	if(empty($title)){
		$title = str_ireplace('(本科)', '', $title_full);
		$title = str_ireplace('（本科）', '', $title);
		$title = str_ireplace('(专科)', '', $title_full);
		$title = str_ireplace('（专科）', '', $title);	
	}

	$cf_now = date('Y-m-d H:i:s');

	if(strlen($code)!=32){
		$code=md5($code);
	}
	$flag=true;

	$sql_tpl="INSERT INTO `school_info` (`code`, `is_done`, `verkey`, `title`, `ud1`, `ud2`, `ud3`, `ud4`, `ud5`, `ud6`, `ud7`, `ud8`, `ud9`, `cf1`, `cf2`, `majorInfo`, `sch_id`, `min_score`, `min_score1`, `min_score2`, `is_subject_cat1`, `is_subject_cat2`, `yd1`, `yd2`, `yd3`, `yd4`, `yd_tips1`, `yd_tips2`, `yd_tips3`, `yd_tips4`) VALUES (?s, '0', '0', ?s, '', '', '', '', '1', ?s, '', '', '', ?s, ?s, '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '');";
	$tmp_sql=prepare($sql_tpl,array($code,$title_full,$title,$cf_now,$cf_now));
	if($ret=run_sql($tmp_sql)){
		echo __METHOD__.'() sql ok ! '.$tmp_sql.PHP_EOL;		
	}else{
		$flag=false;		
	}


	$sql_tpl2="INSERT INTO `shdic_crx_p2015qq176806817_school` (`code`, `title`, `ud6`, `cf1`, `cf2`) VALUES (?s, ?s, ?s,?s,?s);";
	$tmp_sql=prepare($sql_tpl2,array($code,$title_full,$title,$cf_now,$cf_now));
	if($ret=run_sql($tmp_sql)){
		echo __METHOD__.'() sql ok ! '.$tmp_sql.PHP_EOL;		
	}else{
		$flag=false;
	}


	if($flag){
		return true;
	}
	return false;
}

function adm_add_new_school(){
	$code_str='dalidaxuebenke
jiningxueyuanbenke

wuchangshouyixueyuanbenke

zhongguodizhidaxuejiangchengxueyuanbenke

shanghaijiankangyixueyuanbenke

gannanshifanxueyuankejixueyuanbenke

nantongligongxueyuanbenke


sichuanyikedaxuebenke

nanfangkejidaxuebenke

beijingyuyandaxuebenke

shandongshifandaxuebenke
';

	$title_str='大理大学（本科）
济宁学院（本科）

武昌首义学院（本科）

武汉工程科技学院（中国地质大学江城学院）(本科)

上海健康医学院（本科）

赣南师范学院科技学院（本科）

南通理工学院（本科）


四川医科大学（本科）

南方科技大学（本科）

北京语言大学（本科）

山东师范大学（本科）
';

	$code_arr=explode("\n", $code_str);
	$tit_arr=explode("\n", $title_str);

	$code_arr=array_filter($code_arr,"adm_sub_func_is_not_empty");
	$tit_arr=array_filter($tit_arr,"adm_sub_func_is_not_empty");

	if(count($code_arr)!=count($tit_arr)){
		die(__METHOD__.'() count err!');
	}

	foreach ($code_arr as $key => $value) {
		$code=$value;
		$title_full=$tit_arr[$key];
		$title_full = str_ireplace("\r", '', $title_full);		
		adm_add_new_school_sub($code,$title_full);
	}

	die(__METHOD__.'() end');
}

function adm_sub_func_is_not_empty($a){
	return !empty($a);
}

/**



*/


$pagestartime=microtime(); 
$time_log='';

$c='adf403';
$a='adf403';
$html_tpl='';
$outVar=array();

$tt = v('c') ;  if($tt) $c=$tt;
$tt = v('a') ;  if($tt) $a=$tt;
if(stripos($a, '.')!=false){
	$a=str_ireplace('.', '', $a);
}
$a=strtolower($a);
if(strlen($c)>=3 && strlen($a)>=3){
    //base check
}else{
    die('403 Access Denied/Forbidden');
}

$debugMode = 0;
$outVar['debugMode']=0;

$tt = intval(v('debug'))  ;
if ($tt>0 ){
    $debugMode=1;
    $outVar['debugMode']=1;
}

$map_uuid = null;
$map_type = null;
$grids = null;
$limit = 0;
$offset = 15;
$page=0;
$tt = intval(v('start'))  ;
if ($tt>0 ){
    $limit=$tt;
}
$tt = intval(v('limit'))  ;
if ($tt>0 && $tt<=500){
    $offset=$tt;
}
$tt = intval(v('page'))  ;
if ($tt>0 && $tt<=100){
    $page=$tt;
}
// init_db_info($db_table_prefix);

$user_key=null;

$html_tpl='base_tpl';
$html_data='';

switch ($c) {
	case 'test':
        switch ($a) {
            case 'frame':
                $html_tpl='main_frame_tpl';
                $outVar['page_title'] = 'main_frame';
				$outVar['page_desc'] = 'page_desc';
				$html_data = '';
                break;

            case 'render':
                $outVar['page_title'] = 'main_frame';
				$outVar['page_desc'] = 'page_desc';
				$outVar['html_data'] = 'html_data is nothing';
				echo render( $outVar , 'web' , 'frame');
				exit;
                break;

            case '0321':
        		die();
            	break;

            default:
                die('403 Access Denied/Forbidden '.$c);
        }
        break;

	// ********* ********* ********* ********* *********
	// ********* ********* ********* ********* *********
	case 'adm':
		set_time_limit(0);
        switch ($a) {
        	case 'phpinfo':
	        	$tt = v('admin')  ;
				if (!empty($tt) && md5($tt)=='081865360c8448b290629d6b1a2eddc6' ){
				    phpinfo();
				}        		
        		die();

        	case 'adm_update_major_stat_all_without_ind':
        		$ret=adm_update_major_stat_all_without_ind();
        		echo print_r($ret,true);
        		die();

        	case 'adm_init_school_admission_score_real':
        		adm_init_school_admission_score_real();
        		die();

			case 'autoloop':
                $html_tpl='js';
                $html_tpl='js.tpl.html';
                $outVar['page_title'] = 'auto loop';
				$debug_src='';
				$jobName=v('joburl');
				if(!empty($jobName)){
					// print_r($_SERVER);
					$jobUrl=$jobName;
					$jobMode=v('jobMode');
					if(!empty($jobMode)){
						$jobMode=strtolower($jobMode);
						if($jobMode=='api2016'){							
							$tmp_arr=explode('/', $_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME']);
							array_splice($tmp_arr, -1, 1); 
							$jobUrl	=implode('/', $tmp_arr);
							$jobUrl = 'http://'.$jobUrl;
							$jobUrl.='/api2016.php?c=adm&adm=2016&a='.$jobName;
							$jobUrl.='&randseed='.mt_rand(1000,9999);
						}
					}
					$tt='<a href="'.$jobUrl.'" target="_blank">'.$jobUrl.'</a>';
					$html_src=red_span($tt).'<br/>'.PHP_EOL;
					// $html_src.=file_get_contents($jobUrl);
					$html_src.='<iframe class="ifr_rightlist" marginheight="0" marginwidth="0" frameborder="0" scrolling="auto"  height="600px" width="99%" id="iframepage" name="iframepage" onLoad="Javascript:reinitIframeEND();" src="'.$jobUrl.'"></iframe>'.PHP_EOL;  
					// $tt=v('set_pre');
					// if(!empty($tt)){
					// 	$html_src = '<pre>' .$html_src. '</pre>';
					// }
				}else{
					$html_src='skip';
				}
				$outVar['html_data']=$html_src;

				
				$sleepTime=16;
				$tt=intval(v('sleep'));
				if($tt>=3){
					$sleepTime=$tt;
				}


				// if(stripos($html_src,'UPDATE')!==false){}
				if(1){
					// $outVar['js_str']='setInterval("location.reload()", 6000); ';
					$cf_now = date('Y-m-d H:i:s');
					$outVar['js_str']='setTimeout("location.reload()", '.$sleepTime.'000);'.PHP_EOL;
					$outVar['js_str'].='console.log("'.$cf_now.'")';
				}
				html_render( $outVar , array('ui_tpl' => $html_tpl 
					,'debug_src'=>$debug_src));
                break;
/*
        	case 'adm_add_new_school':
        		adm_add_new_school();

        	case 'adm_fix_new_school_code':
        		adm_fix_new_school_code();
        		die();        	

        	case 'adm_get_school_major_info_yd':
        		$ret=adm_get_school_major_info_YD();
        		echo print_r($ret,true);
        		die();

			case 'autoloop':
                $html_tpl='js';
                $html_tpl='js.tpl.html';
                $outVar['page_title'] = 'auto loop';
				$debug_src='';				
				$html_src=adm_get_school_major_info_YD_fix_all();
				$outVar['html_data']=$html_src;
				if(stripos($html_src,'UPDATE')!==false){
					// $outVar['js_str']='setInterval("location.reload()", 6000); ';
					$cf_now = date('Y-m-d H:i:s');
					$outVar['js_str']='setTimeout("location.reload()", 9000); console.log("'.$cf_now.'")';
				}
				html_render( $outVar , array('ui_tpl' => $html_tpl 
					,'debug_src'=>$debug_src));
                break;
*/
/*        	
            case 'update_zengfu5':
                $html_tpl='base_tpl';
                $outVar['page_title'] = '更新五年薪酬增幅数据';
				$debug_src='';
				$debug_src=adm_update_zengfu5($outVar);
				html_render( $outVar , array('ui_tpl' => $html_tpl ,'debug_src'=>$debug_src));
                break;


*/
			// case 'autoloop':
			// 	$html_tpl='js';
			// 	$html_tpl='js.tpl.html';
			// 	$outVar['page_title'] = 'auto loop';
			// 	$debug_src='';
			// 	echo 'aaa';
			// 	$html_src=fix_school_major_field_job_func();
			// 	$outVar['html_data']=$html_src;
			// 	if(stripos($html_src,'UPDATE')!==false){
			// 		$outVar['js_str']='setInterval("location.reload()", 16000); ';
			// 	}
			// 	html_render( $outVar , array('ui_tpl' => $html_tpl 
			// 		,'debug_src'=>$debug_src));
			// 	break;

            default:
                die('403 Access Denied/Forbidden '.$c);
        }
        break;

	// ********* ********* ********* ********* *********
	// ********* ********* ********* ********* *********
	case 'guest':
        switch ($a) {
            case 'render':
                $outVar['page_title'] = 'main_frame';
				$outVar['page_desc'] = 'page_desc';
				$outVar['html_data'] = 'html_data is nothing';
				echo render( $outVar , 'web' , 'frame');
				exit;
                break;

            default:
                die('403 Access Denied/Forbidden '.$c);
        }
        break;

	// ********* ********* ********* ********* *********
	// ********* ********* ********* ********* *********
	case 'school':
        switch ($a) {
            case 'search':			

                $html_tpl='school.search';
                $outVar['page_title'] = '学校搜索';
				// $outVar['page_desc'] = 'page_desc';
				// $outVar['html_data'] = 'not ready!!!';
				$debug_src=school_search($outVar);
				html_render( $outVar , array('ui_tpl' => $html_tpl ,'debug_src'=>$debug_src));
                break;

            default:
                die('403 Access Denied/Forbidden '.$c);
        }
        break;
	// ********* ********* ********* ********* *********
    // ********* ********* ********* ********* *********
	case 'major':
        switch ($a) {
            case 'search':			

                $html_tpl='zhuanyeshouye';
                $outVar['page_title'] = '专业搜索';
				// $outVar['page_desc'] = 'page_desc';
				// $outVar['html_data'] = 'not ready!!!';
				$debug_src=major_search();
				html_render( $outVar , array('ui_tpl' => $html_tpl ,'debug_src'=>$debug_src));
                break;

            default:
                die('403 Access Denied/Forbidden '.$c);
        }
        break;
	// ********* ********* ********* ********* *********
    // ********* ********* ********* ********* *********
    case 'api':
        switch ($a) {
              
            case 'dingwei_v1':
            	@session_start();
                $uid=uid();
				// echo '<xmp>';
				// print_r($_SESSION);
				// echo '</xmp>';
                if($uid==false) die(json_encode(array('ret_msg'=>'未登录！','ret_code'=>110)));
                echo dingwei_v1($uid,v('paiming'),v('zrs'),v('sc'),v('sid'),v('district'),v('subj'));
                exit;
                break;

			// case 'major_list':
			// 	if(intval(v('ajax'))>0 ){
			// 		major_search(array('ajax'=>1));
			// 	}			
			// 	api_major_list();
			// 	break;

			case 'ruanyingjian':
				$code=v('scode');				
				$tmp_sql=prepare("SELECT * FROM `school_ruanyingjian` WHERE `code` =?s limit 1 ",array($code));
                $ret_sql=get_data($tmp_sql);
				if($ret_sql!==false){
					echo json_encode($ret_sql);
				}
				break;

            default:
                die('403 Access Denied/Forbidden '.$c);
        }
        break;

	// ********* ********* ********* ********* *********
    // ********* ********* ********* ********* *********
	default:
		die('403 Access Denied/Forbidden');
}

/**


*/

if($c=='api' && $action){
    echo $html_data;

}else if($a){

    $time_log.= "页面运行时间: ". used_time($pagestartime) ."秒.<br/>".PHP_EOL; 

	$out = array(
		'ver' => 'v20151110', 
        'app_version' => 'v20151110', 
		'html_res_root' => 'html/res2', 
		'res_root' => 'html/res', 

		'user_key' => $user_key==null?'new':$user_key,
        'data'=> $html_data,

        'time_log'=> $time_log,
        'page_count'=>$offset,

        'js_css_ver'=>'20151110',

        'last_modify_time'=>date('Y-m-d H:i:s'),
		'author' => '492607291#qq.com'
	);
	
    foreach ($outVar as $key => $value) {
        $out[$key] = $value;
    }
	/* Load and display map */
    if(empty($html_tpl)){
        $html_tpl='base_tpl';
    }
	$tbs = template_load($html_tpl);
	$tbs->Show();
}

?>

