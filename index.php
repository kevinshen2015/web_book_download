<?php
// if(defined('SERVER_SOFTWARE')){
// 	//SERVER_SOFTWARE = bae/3.0
// 	$runtime_env_check=strtolower(SERVER_SOFTWARE);
// 	if(stripos($runtime_env_check,'bae')!==false){
// 		include_once 'php/session.inc.php';
// 	}
// }

set_time_limit(0);

ini_set('session.cookie_path', '/');
ini_set('session.cookie_domain', '.shdic.com'); //注意***.com换成你自己的域名
ini_set('session.cookie_lifetime', '1800');

session_start();
//在页首先要开启session,


define( 'DS' , DIRECTORY_SEPARATOR );
// define( 'APP_ROOT' , dirname( dirname( __FILE__ ) ) . DS  );
define( 'APP_ROOT' , dirname( __FILE__ ) . DS  );

date_default_timezone_set("Asia/Shanghai");
$action = empty( $_REQUEST['ajax'] ) ? '' : strtolower( $_REQUEST['ajax'] );
if($action){
	if($action=='js'){
		$ContentType ='application/x-javascript';
	}else{
		$ContentType ='text/plain';
	}
}else{
	$ContentType ='text/html';
}
header("Content-Type:".$ContentType.";charset=utf-8");

// define( 'KIS_APP_ROOT' , dirname( APP_ROOT ) . DS );
define( 'KIS_APP_ROOT' , APP_ROOT .'php'.DS);
// include_once KIS_APP_ROOT.'ext_lib'. DS.'comm.function.php';
define( 'AROOT' , KIS_APP_ROOT);
define( 'CROOT' , KIS_APP_ROOT.'_lp'. DS.'core'. DS  );
include_once CROOT. 'lib'.DS.'core.function.php';
// include_once( KIS_APP_ROOT. 'ext_lib'.DS .'mail.config.inc.php' );
include_once KIS_APP_ROOT.'lib'. DS.'app.function.php';

include_once 'php/map.inc.php';
include_once 'php/func.inc.php';
include_once 'php/comm.inc.php';
include_once 'php/web.inc.php';
// include_once 'php/lib/Snoopy.class.php';

define('TBS_ROOT', KIS_APP_ROOT.'_lp'.DS );
define('TPLROOT', dirname( __FILE__ ) . DS. 'html'.DS );

define('APP_ENVIRONMENT', 'DEVELOPMENT');
// define('APP_ENVIRONMENT', 'PRODUCT');
// define('KIS_DEBUG_MODE', 'DISABLE_DEBUG');

define( 'CACHE_DIR' , 'tmp'.DS);
define( 'CACHE_ROOT' , APP_ROOT . CACHE_DIR);

/**

*/

// $db_table_prefix = 'shdic_sc2015_'; // in func.inc.php

function is_exclude_site($url){
	//需要排除的网站
	$siteArr=explode(';', 'zongheng.com;chuangshi.qq.com');
	foreach ($siteArr as $siteUrl) {
		if(!empty($siteUrl)){
			if(stripos($url, $siteUrl)!==false){
				return true;
			}
		}
	}
	return false;
}

function has_charset_utf8_html($src){
	$tt=explode('</head>', $src);
	$tt=$tt[0];
	// <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	if(stripos($tt, 'charset=utf-8')!==false){
		return true;
	}
	return false;
}

/**



*/

// $tt = intval(v('gm'))  ;
// if ($tt>0 ){
//     $gm=$tt;
//     $outVar['globalMode']=$gm;
//     $kv_key='gm_count_'.$gm;
//     $flag=false;
//     if($tt=get_redis($kv_key)){
//     	$tt=intval($tt)+1;
//     	if($tt==1){
//     		set_redis('last_renew_gm_count_time',date('Y-m-d H:i:s'));	
//     	}
// 		$flag=set_redis($kv_key,$tt);
//     }
//     if(!$flag){
//     	$tt=kget($kv_key);
//     	$tt=intval($tt)+1;
//     	kset($kv_key,$tt); 	
//     }    
// }

$pagestartime=microtime(); 
$time_log='';

$c='guest';
$a='index';
// $a='demo-frame';
$html_tpl='';
$outVar=array('js_css_ver'=>'20160521');
// $outVar['js_css_ver']= date('YmdHis');

$tt = v('c') ;  if($tt) $c=$tt;
$tt = v('a') ;  if($tt) $a=$tt;

if(stripos($a, '.')!=false){
	$a=str_ireplace('.', '', $a);
}
$a=strtolower($a);

if(strlen($c)>=3 && strlen($a)>=3){
    //base check
}else{
    die('403 Access Denied/Forbidden');
}

$debugMode = 0;
$outVar['debugMode']=0;

$tt = intval(v('debug'))  ;
if ($tt>0 ){
    $debugMode=1;
    $outVar['debugMode']=1;    
}

$outVar['adminMode']=false;
// $outVar['adminMode']=true;		//enable build_static_html()

$tt = v('admin')  ;
if (!empty($tt) && md5($tt)=='081865360c8448b290629d6b1a2eddc6' ){
    $outVar['adminMode']=true;
}

$tt = v('build_type')  ;
if (!empty($tt)){
	if ($tt=='build_static_html' ){
		$outVar['build_static_html']=1;	    	
	}else if($tt=='Domino_Server_Page' ){
    	$outVar['build_static_html']=1;	
    	$outVar['dsp_mode']=1;
	}
}


$map_uuid = null;
$map_type = null;
$grids = null;
$limit = 0;
$offset = 15;
$page=0;

$tt = intval(v('start'))  ;
if ($tt>0 ){
    $limit=$tt;
}
$tt = intval(v('limit'))  ;
if ($tt>0 && $tt<=500){
    $offset=$tt;
}
$tt = intval(v('page'))  ;
if ($tt>0 && $tt<=100){
    $page=$tt;
    $outVar['curr_page']=$tt;
}else{
	$outVar['curr_page']=0;
}

// init_db_info($db_table_prefix);

$user_key=null;

$html_tpl='demo_tpl';
$html_data='';
$snoopy=null;
$sql_table_name = get_sql_table_name();

switch ($c) {
	case 'guest':
		switch ($a) {

			case 'dashboard':
				$html_src='';
				$outVar['page_nav']='';
				$outVar['page_title'] = '仪表板';
				$debug_src='';
								
				dashboard();
				// pc_auto_work();
				html_render( $outVar , array('ui_tpl' => $a ,'debug_src'=>$debug_src));
				break;
		
			case 'zhuxiao':	//注销
			case 'logout':
				foreach( $_SESSION as $key=>$value ){
					unset( $_SESSION[$key] );
				}			
				session_destroy();
				header('Location: index.php?a=index');
				break;

			case 'login':
			case 'denglu':	//登陆
				// @session_start();
				//在页首先要开启session,
				//error_reporting(2047);
				@session_destroy();
				//将session去掉，以每次都能取新的session值;
				html_render( $outVar);
				break;

			case 'site':	//
				$outVar['page_nav']='';
				$outVar['page_title'] = '站点配置';
				$debug_src='';
				if(!is_login()) {
					header('Location: index.php?a=denglu');
				}
				// if(!is_admin()) {
				// 	die('only for admin user!');
				// }

				// SELECT `id`, `url_md5`, `name`, `url`, `ListStart`, `ListEnd`, `ContentStart`, `ContentEnd`, `NeedDelStr`, `VolumeStart`, `VolumeEnd`, `BriefUrlStart`, `BriefUrlEnd`, `AuthorStart`, `AuthorEnd`, `BriefStart`, `BriefEnd`, `BookImgUrlStart`, `BookImgUrlEnd`, `cf1`, `is_his` FROM `shdic_wbd2016_sitesign` WHERE 1

	            $tt='`id`, `name`, `url`, `ListStart`, `ListEnd`, `ContentStart`, `ContentEnd`, `NeedDelStr`, `VolumeStart`, `VolumeEnd`, `BriefUrlStart`, `BriefUrlEnd`, `AuthorStart`, `AuthorEnd`, `BriefStart`, `BriefEnd`, `BookImgUrlStart`, `BookImgUrlEnd`, `cf1`';
				$outVar['column_title_def']=$tt;
				$outVar['column_type_def']='';
				
				html_render( $outVar , array('ui_tpl' => 'site.list' ,'debug_src'=>$debug_src));
				break;

			case 'task2link':	//
				$outVar['page_nav']='';
				$outVar['page_title'] = 'crx';
				$debug_src='';
				$html_data=v('url');
				$outVar['html_data']=$html_data;
				html_render( $outVar , array('ui_tpl' => 'task2link' ,'debug_src'=>$debug_src));
				break;

			case 'task_do':	//
				$outVar['page_nav']='';
				$outVar['page_title'] = 'crx';
				$debug_src='';
				$task_id=v('id');
				$task_id=intval($task_id);
				
				if($task_id>0){
					$tmp_sql = prepare('SELECT * FROM `shdic_wbd2016_task` WHERE `is_his`!=?i AND id=?i limit 1' ,array(1,$task_id));
					if($ret_sql = get_line($tmp_sql)){
						do_a_task($ret_sql,$task_id);	
					}else{
						$outVar['html_data']='无效的任务ID! #2';
					}
				}else{
					$outVar['html_data']='无效的任务ID!';
				}
				html_render( $outVar , array('ui_tpl' => 'task_masonry' ,'debug_src'=>$debug_src));
				break;

			case 'downthisbook':	//downThisBook
				$outVar['page_nav']='';
				$outVar['page_title'] = 'downThisBook';
				$debug_src='';
				$task_id=v('id');
				$task_id=intval($task_id);
				
				if($task_id>0){
					$tmp_sql = prepare('SELECT * FROM `shdic_wbd2016_task` WHERE `is_his`!=?i AND id=?i limit 1' ,array(1,$task_id));
					if($ret_sql = get_line($tmp_sql)){
						down_this_book($ret_sql,$task_id);	
					}else{
						$outVar['html_data']='无效的任务ID! #2';
					}
				}else{
					$outVar['html_data']='无效的任务ID!';
				}
				html_render( $outVar , array('ui_tpl' => 'task' ,'debug_src'=>$debug_src));
				break;

			case 'downthisbook2':	//downThisBook2
				$outVar['page_nav']='';
				$outVar['page_title'] = 'downThisBook';
				$debug_src='';
				$task_id=v('id');
				$task_id=intval($task_id);
				
				if($task_id>0){
					$tmp_sql = prepare('SELECT * FROM `shdic_wbd2016_task` WHERE `is_his`!=?i AND id=?i limit 1' ,array(1,$task_id));
					if($ret_sql = get_line($tmp_sql)){
						down_this_book2($ret_sql,$task_id);	
					}else{
						$outVar['html_data']='无效的任务ID! #2';
					}
				}else{
					$outVar['html_data']='无效的任务ID!';
				}
				html_render( $outVar , array('ui_tpl' => 'task_masonry' ,'debug_src'=>$debug_src));
				break;

			case 'task':	//
				$outVar['page_nav']='';
				$outVar['page_title'] = '任务列表';
				$debug_src='';
				if(!is_login()) {
					header('Location: index.php?a=denglu');
				}
				// if(!is_admin()) {
				// 	die('only for admin user!');
				// }

				$tmp_sql = prepare('SELECT * FROM `shdic_wbd2016_task` WHERE `is_his`!=?i order by `cf1` limit 100' ,array(1));
				$ret_sql = get_data($tmp_sql);

				$skip_item_str='';
				$show_table_head=true;
				$delimiter=null;
				$opt=array();
				// $opt['cn_head']=array('cc'=>'分类','category'=>'大分类','sub_category'=>'小分类','count_num'=>'记录数');
				$opt['link_arr']=array('book_title'=>'index.php?ks=1&c=guest&a=task_do&id=[id]|link'
					,'id'=>'index.php?ks=1&c=guest&a=task_do&id=[id]|link');
				$html = kis_debug_table($ret_sql,$skip_item_str,$show_table_head, $delimiter,$opt);

				$outVar['html_data']=$html;
				
				html_render( $outVar , array('ui_tpl' => 'task' ,'debug_src'=>$debug_src));
				break;

			default:
				// die('403 Access Denied/Forbidden '.$c);
				$outVar['page_nav']='';
				$debug_src='';
				html_render( $outVar , array('ui_tpl' => $a ,'debug_src'=>$debug_src));

				exit;
		}
		break;

	case 'site':
		switch ($a) {
			case 'details':
				$outVar['page_nav']='';
				$outVar['page_title'] = '站点详情';
				$debug_src='';
				if(!is_login()) {
					header('Location: index.php?a=denglu');
				}
				// if(!is_admin()) {
				// 	die('only for admin user!');
				// }
				show_simple_character_card(null);

				html_render( $outVar , array('ui_tpl' => 'site.details' ,'debug_src'=>$debug_src));
				break;
			
			default:
				# code...
				break;
		}

		break;
	
	default:
		die('403 Access Denied/Forbidden');
}

/**


*/

if($c=='api' && $action){
    echo $html_data;

}else if($a){

    $time_log.= "页面运行时间: ". used_time($pagestartime) ."秒.<br/>".PHP_EOL; 

    $js_css_ver='20160510';
    if(isset($outVar['js_css_ver']) && !empty($outVar['js_css_ver'])){
    	$js_css_ver=$outVar['js_css_ver'];
    }

	$out = array(
		'ver' => 'v'.$js_css_ver, 
        'app_version' => 'v'.$js_css_ver, 
		'html_res_root' => 'html/res2', 
		'res_root' => 'html/res', 

		'user_key' => $user_key==null?'new':$user_key,
        'data'=> $html_data,

        'time_log'=> $time_log,
        'page_count'=>$offset,

        'js_css_ver'=>$js_css_ver,

        'last_modify_time'=>date('Y-m-d H:i:s'),
		'author' => 'kevinshen2014@163.com;492607291@qq.com'
	);
	
    foreach ($outVar as $key => $value) {
        $out[$key] = $value;
    }
	/* Load and display map */
    if(empty($html_tpl)){
        $html_tpl='base_tpl';
    }
	$tbs = template_load($html_tpl);
	$tbs->Show();
}

?>

