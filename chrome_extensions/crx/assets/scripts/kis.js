/*
Copyright (c) 2015 Shdic (http://shdic.com)
 */
"use strict";"strict mode";

function close_this_win() { //这个不会提示是否关闭浏览器
	// try {
	// 	if (!m & opener)
	// 		opener.location.reload();
	// } catch (e) {};
	try {
		window.opener = null;
		window.open("", "_self");
		window.close();
	} catch (e) {};
}

function appendZero(n){return(("00"+ n).substr(("00"+ n).length-2));}//日期自动补零程序
 
//默认时间格式
function getFormatDate(){
    var now = new Date();
    //year = now.getYear();
    var year = now.getFullYear();
    var month = now.getMonth();
    var mdate = now.getDate();
    var month = month +1;
    return year+"-"+appendZero(month)+"-"+appendZero(mdate);
}
 
function getFormatTime(){
    var now = new Date();
    var h = now.getHours(), m = now.getMinutes(), s = now.getSeconds();
  return  appendZero(h) +":"+ appendZero(m) +":"+ appendZero(s);
}
 
function getFullTime(){
    return getFormatDate()+' '+getFormatTime();
}
 
function getFullTimeCN(){
    var myDate=new Date();
    var arrayDay=["日","一","二","三","四","五","六"]; 
    return myDate.getFullYear()+"年"+appendZero(myDate.getMonth()+1)+"月"+appendZero(myDate.getDate())+"日 "+"星期"+arrayDay[myDate.getDay()];
}


function GetCookieValue(name) {
	var cookieValue = null;
	if (document.cookie && document.cookie != '') {
		var cookies = document.cookie.split(';');
		for (var i = 0; i < cookies.length; i++) {
			var cookie = jQuery.trim(cookies[i]);
			//PYYH=USERNAME=steven&PASSWORD=111111&UserID=1&UserType=1
			if (cookie.substring(0, name.length + 1) == (name + '=')) {
				cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
				//USERNAME=steven&PASSWORD=111111&UserID=1&UserType=1
				break;
			}
		}
	}
	return cookieValue;
}

function DelCookie(name) {
	var exp = new Date();
	exp.setTime(exp.getTime() + (-1 * 24 * 60 * 60 * 1000));
	// var cval = GetCookieValue(name);
	// document.cookie = name + "=" + cval + "; expires=" + exp.toGMTString();
	document.cookie = name + "=0; path=/;expires = " + date.toGMTString();
}

function setCookie(cookiename, cookievalue, hours) {
	var date = new Date();
	var h=3;
	if(hours){
		h=+hours;
	}
	date.setTime(date.getTime() + Number(h) * 3600 * 1000);
	document.cookie = cookiename + "=" + escape(cookievalue) + "; path=/;expires = " + date.toGMTString();
}

function jqCookie(cookieName,cookieValue,domain){
	return $.cookie(cookieName,cookieValue,{expires:1,path:'/',domain:domain,secure:false,raw:false});
	//	http://www.cnblogs.com/crazy-fox/archive/2012/01/08/2316499.html
	// 注：domain：创建cookie所在网页所拥有的域名；secure：默认是false，如果为true，cookie的传输协议需为https；raw：默认为false，读取和写入时候自动进行编码和解码（使用encodeURIComponent编码，使用decodeURIComponent解码），关闭这个功能，请设置为true。
}

function delQQCookie(cookieName){
	return jqCookie(cookieName,null,'.qq.com');
}

/*

*/
// function SimpleQuene(len){
//     var capacity=len;
//     var list=[];
    
//     this.in=function(data){
//         if(!data){return false;}
//         if(list.length == capacity){
//             this.out();
//         }
//         list.push(data);
//         return true;
//     };

//     this.out=function(){
//     	if(list == null) return false;
//     	if(list.length==0) return false;
//         return list.shift();
//     };

//     this.isEmpty=function(){
//     	if(list == null) return false;
//    		return list.length>0;
//     };

//     this.size=function(){
//     	if(list == null) return 0;
//         return list.length;
//     };
// }


/**
 * JS判断一个值是否存在数组中
 * 琼台博客
 */
// 定义一个判断函数
var in_array = function (arr) {
	// 判断参数是不是数组

	// var t1 = typeof arr === 'object' ? arr[0]:false;
	// var t2 = arr.constructor === Array ? arr.join(','):false;
	// var t3 = arr.length ? 'an empty array':false;
	// var t4 = arr.length === 1 ? arr.constructor:false;
	// var t5 = typeof arr;

	// var isArr = arr && console.log(	typeof arr === 'object' ? arr.constructor === Array ? arr.length ? arr.length === 1 ? arr[0] : arr.join(',') : 'an empty array' : arr.constructor : typeof arr);
	var isArr = arr && (arr.constructor === Array ? true:false);
	// 不是数组则抛出异常
	if (!isArr) {
		throw "arguments is not Array";
	}
	// 遍历是否在数组中
	for (var i = 0, k = arr.length; i < k; i++) {
		if (this == arr[i]) {
			return true;
		}
	}
	// 如果不在数组中就会返回false
	return false;
}
// 给字符串添加原型
String.prototype.in_array = in_array;
// 给数字类型添加原型
Number.prototype.in_array = in_array;
// // 声明一个数组
// var arr = Array('blue', 'red', '110', '120');
// // 字符串测试
// var str = 'red';
// var isInArray = str.in_array(arr);
// alert(isInArray); // true
// // 数字测试
// var num = 119;
// var isInArray = num.in_array(arr);
// alert(isInArray); // false
// 如果传入的不是数组则会抛出异常
 
/*


*/

function len(s) {
	// if(typeof(s)!= 'string'){
	var s2 = s.toString();
	return s2.length();
	// }
	// return s.length();
}

var kisjs = {};

kisjs.strUtil = {
	
	cutMidStr : function(s,a,b){
		if(typeof(s)!='string'){
			s = s.innerHTML;
		}
		var p1=s.indexOf(a);
		if(p1==-1) return false;
		var s2 = s.substr(p1+a.length);
		var p2=s2.indexOf(b);
		if(p2==-1) return false;
		return s2.substr(0,p2);
	},

	encode : function (s) {
		return escape(encodeURIComponent(s));
	},

	decode : function (s) {
		return decodeURIComponent(unescape(s));
	},

	//全部替换
	replaceAll : function (s, f, t) {
		var r = s;
		try {
			r = s.split(f).join(t);
		} catch (e) {};
		return r;
	},

	//计算长度(中文长度修正)
	getBytes : function (s) {
		if (s == null) {
			return 0;
		}
		try {
			var cArr = s.match(/[^\x00-\xff]/ig);
			return s.length + (cArr == null ? 0 : cArr.length);
		} catch (e) {
			return s.length;
		};
	},
	GetLength : function (str) {
		////<summary>获得字符串实际长度，中文2，英文1</summary>
		////<param name="str">要获得长度的字符串</param>
		var realLength = 0,
		len = str.length,
		charCode = -1;
		for (var i = 0; i < len; i++) {
			charCode = str.charCodeAt(i);
			if (charCode >= 0 && charCode <= 128)
				realLength += 1;
			else
				realLength += 2;
		}
		return realLength;
	},
	// 去左空格
	ltrim : function (s) {
		return s.replace(/^\s*/, "");
	},
	// 去右空格
	rtrim : function (s) {
		return s.replace(/\s*$/, "");
	},
	// 去左右空格
	trim : function (s) {
		return rtrim(ltrim(s));
	},
	//获取文件扩展名
	get_ext : function (f_path) {
		var ext = '';
		if (f_path != null && trim(f_path).length > 0) {
			f_path = trim(f_path);
			ext = f_path.substring(f_path.lastIndexOf(".") + 1, f_path.length);
		}
		return ext;
	},
	//验证文件扩展名
	chk_img_ext : function (f_path) {
		var ext = this.get_ext(f_path);
		ext = ext.toLowerCase();
		//根据需求定制
		//var accept_ext = new Array("doc", "pdf", "bpm", "jpeg", "jpg", "gif", "ppt", "xls");
		var accept_ext = new Array("jpg", "gif", "jpeg", "png");
		var flag = false;
		if (ext != '') {
			for (var i = 0; i < accept_ext.length; i++) {
				if (ext == accept_ext[i])
					flag = true;
			}
		}
		return flag;
	},
	cint : function (n) {
		var r = 0;
		try {
			r = parseInt(n + 0.50);
		} catch (e) {};
		return r;
	},

//补零程序
/* 查表法(过程式版本)  by aimingoo */
	padZero : function() {
	  var tbl = [];
	  return function(num, n) {
	    var len = n-num.toString().length;
	    if (len <= 0) return num;
	    if (!tbl[len]) tbl[len] = (new Array(len+1)).join('0');
	    return tbl[len] + num;
	  }
	},

};


// padZero = function() {
//   var tbl = [];
//   return function(num, n) {
//     var len = n-num.toString().length;
//     if (len <= 0) return num;
//     if (!tbl[len]) tbl[len] = (new Array(len+1)).join('0');
//     return tbl[len] + num;
//   }
// }();

kisjs.arrUtil = {
	unique : function (arr1) {	//去除数组中重复的项 
		var arr=[]; 
		var m; 
		while(arr1.length>0){ 
		m=arr1[0]; 
		arr.push(m); 
		arr1=$.grep(arr1,function(n,i){ 
			return n==m; 
			},true); 
		}
		return arr; 
	},
};


/********************** JavaScript common code *****************************/

// The Central Randomizer 1.3 (C) 1997 by Paul Houle (houle@msc.cornell.edu)
// See:  http://www.msc.cornell.edu/~houle/javascript/randomizer.html
rnd.today = new Date();
rnd.seed = rnd.today.getTime();
function rnd() {
	rnd.seed = (rnd.seed * 9301 + 49297) % 233280;
	return rnd.seed / (233280.0);
};
function rand(number) {
	return Math.ceil(rnd() * number);
};
// end central randomizer.