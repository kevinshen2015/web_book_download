﻿/*
Copyright (c) 2015 Shdic (http://shdic.com)
 */
"use strict";"strict mode";

//Seconds
var taskDelayTimeMin=3;
var taskDelayTimeMax=5;


var timeoutHandle;
var lastOpenUrl=[];
var lastAddUrl=[];
var lastOpCheck=[];
var bigDelayCount=0;
var bigDelayTime=30;	//Seconds
var bigDelayMaxCount=50;
var workMode=0;
var firstTime=null;
var jid='shdic_jid';

taskDelayTimeMin = taskDelayTimeMin*1000;
taskDelayTimeMax = taskDelayTimeMax*1000;

var shdic={};

shdic.ApiDebug = {  
  api_name : [],    // array of api's name.
  api_count : [],    // array of api's running times.

  add : function(f1, a1) {
  	var func = f1.toLowerCase();
  	var api = a1.toLowerCase();
  	if(func!=api){
  		api=func+'-'+api;
  	}
  	var tmpid=-1;
	for (var idx in this.api_name){
		var item=this.api_name[idx];
		if(item==api){
			tmpid=idx;
			break;
		}
	}
	if(tmpid<0){
		this.api_name.push(api);
		this.api_count.push(1);
	}else{
		this.api_count[tmpid]++;
	}
  },

  show : function(cb) {  	
	for (var idx in this.api_name){
		var item=this.api_name[idx];
		var count=this.api_count[idx];
		if(cb){
			cb(item+':'+count);
	  	}else{
	  		jobMonitor(item+':'+count);
	  	}
	}
  },
}

$(document).ready(function(){
	
	var loc = location.href;
	if(loc.indexOf("web_book_download") == -1) {
	// 	// alert('xx 1');
	// 	return false;
	}

	// if(loc.indexOf("school") == -1) return false;

	// for_office();

	firstTime=getFullTime();

	// web_book_download/index.php?a=task
	// web_book_download/index.php?a=task2link&url=http%3A%2F%2Fwww.baidu.com%2Flink%3Furl%3DeLF49vgxcmjpuwH6nb6BCvzp37qbHZpcjv6eO0W1gYrJ5QsZWdBOQ4ZMDPjiJVpkXvOYOB6rYTTVt2eFQxDnT_
	if(loc.indexOf("?a=task2link") != -1){

		// alert('web_book_download xx 2');

		//代码打开的窗口
		// if(loc.toLowerCase().indexOf("getMajorInfo.do".toLowerCase()) != -1) {
			if(timeoutHandle) clearTimeout(timeoutHandle);
			timeoutHandle=setTimeout('process_htmls()',3500+task_rnd(1500));
			return false;	
		// }

	}

	// alert('xx 3');

	// if(timeoutHandle) clearTimeout(timeoutHandle);
	// timeoutHandle=setTimeout('init()',2000);

});

function for_office(){
	//hide all img
	$("img").each( function() {  
        // if ($(this).width() > 10) {
            $(this).hide();
        // }
    });

	$(".resume_key2").each( function() {  
        $(this).hide();  
    });

}

function init_div(pos){
	var btn="<button onclick=\"$('#process_current_page_url_flag').val(0);\">Stop</button><br/>"
		+"<button onclick=\"$('#process_current_page_url_flag').val(1016);\">Major1016</button>"
		+"<button onclick=\"$('#process_current_page_url_flag').val(1);\">School</button><button onclick=\"$('#process_current_page_url_flag').val(2);\">Major</button><br/>"
		+"<button onclick=\"$('#process_current_page_url_flag').val(3);\">Major2</button><button onclick=\"$('#process_current_page_url_flag').val(5);\">tools</button><br/>"
		+"<button onclick=\"$('#process_current_page_url_flag').val(6);\">down-xls</button>";
	var boarddiv = "<div style='background:#cce8cf;width:200px;height:60px;z-index:999;position:absolute;top:0;margin-top:100px;'>插件工作情况说明<br/><div id='app_msg' style='background:#C7EDCC'></div><div id='job_msg' style='background:#cecece'></div><div id='job_status' style='background:#C7EDCC'></div><div id='op_bar'><input id='process_current_page_url_flag' value='0' size='2'>"+btn+"</div></div>"; 
	$(pos).before(boarddiv); 
}

function init_monitor_div(pos){
	var boarddiv = "<div style='background:#cce8cf;width:200px;height:60px;z-index:999;position:absolute;top:0;margin-top:100px;'>插件工作情况监控<br/><textarea id='job_monitor' style='background:#C7EDCC;width:196px;height:360px'></textarea></div>"; 
	$(pos).before(boarddiv); 
}

function init(mode){
	var kk=3000;
	var auto_start=false;
	// if (window.confirm('你确定要[自动开始]吗？')) {
	// 	auto_start=true;
	// }

	init_div(".feedback");
	if(mode!=2){		
		init_monitor_div("#pipe_1");
		showCookie();
	}else{
		kk=kk+task_rnd(2000);
	}

	var tt=0;
	var flag=0;	
	var tt2=0;
	var maxi=10;
	tt=GetCookieValue(jid);
	if(tt && tt.length>10 ){
		tt2=GetCookieValue(jid+'_mode');
		if(tt2 && +tt2>0){
			$("#process_current_page_url_flag").val(+tt2);
		}
	}else{
		//new start
		setCookie(jid,firstTime);		//will removed , when jobs done .
		setCookie(jid+'_count',0);		//当前各工作窗口累计处理了多少个
	}

	// if(auto_start){
	// 	$("#process_current_page_url_flag").val('2');
	// }

	if(timeoutHandle) clearTimeout(timeoutHandle);
	timeoutHandle=setTimeout('check_process_flag();',kk);
}

function check_process_flag(){

	showCookie();

	var a = $("#process_current_page_url_flag").val();
	if(a=='1'){
		workMode=1;
		bigDelayMaxCount=0;
		updateCookie(1);
		// process_current_page_url();
		// post_link_page_school();			//process ok 08/02
		return load_one_job();
	}

	if(a=='2'){
		workMode=2;
		updateCookie(1);
		return load_one_job2015('api20150804getjob4major');
	}

	if(a=='3'){
		workMode=3;
		updateCookie(1);
		return load_one_job2015('api20150804reget4major');
	}

	if(a=='5'){
		workMode=5;
		return local_tools_js();
	}

	if(a=='6'){
		workMode=6;
		return local_tools_js();
	}

	if(a=='1016'){
		workMode=1016;
		updateCookie(1);
		return load_one_job2015('api20151016reget4major');
	}

	if(timeoutHandle) clearTimeout(timeoutHandle);
	timeoutHandle=setTimeout('check_process_flag();',3000);
	return timeoutHandle;
}

var lastBigDelayCount=0;
function updateCookie(env) {
	var tt=0;
	var kk=0;
	if(env){
		if(+env==1){	//init
			tt=GetCookieValue(jid+'_mode');
			if(workMode && +workMode>0 && tt!=null && +tt!=+workMode){
				setCookie(jid+'_mode',workMode);		
			}
		}

		if(+env==2){	//stop
			tt=GetCookieValue(jid+'_count');
			setCookie(jid+'_stop',+tt);
			setCookie(jid,'0');
			setCookie(jid+'_count','0');

			//reset qq cookie
			delQQCookie('RK');
			delQQCookie('pgv_pvi');
			delQQCookie('pgv_si');
			delQQCookie('pt2gguin');
			delQQCookie('pt_clientip');
			delQQCookie('pt_serverip');
			delQQCookie('ptcz');
			delQQCookie('ptisp');
			delQQCookie('ptui_loginuin');
			delQQCookie('skey');
			delQQCookie('uin');

			// $("#process_current_page_url_flag").val('0');

			if(timeoutHandle) clearTimeout(timeoutHandle);
			var tt=bigDelayTime*1000+task_rnd(3000);
			jobStatus('wait load_one_user() setTimeout:'+tt);
			timeoutHandle=setTimeout('load_one_user();',tt);
		}

		if(+env==3){	//in loop ++
			kk=1;
		}
		
	}else{
		kk=1;
	}

	if(kk==1){	//update count
		tt=GetCookieValue(jid+'_count');
		if(lastBigDelayCount<bigDelayCount){
			tt=+tt+(bigDelayCount-lastBigDelayCount);
			setCookie(jid+'_count',+tt);
		}
		bigDelayCount=+tt;
		lastBigDelayCount=bigDelayCount;
	}
}

function process_current_page_url(){

	showMsg('开始处理当前页的链接地址');
	var a = $(".col1td");
	var all_url = '';
	var delimiter = '$$$$$$';
	for (var i = 0; i < a.length; i++) {
		try{
			if(a[i] && a[i].innerHTML){
				var b=a[i].innerHTML;
				// process_one_url(b);
				if(check_url_already_processed_add(b)==false){
					all_url = all_url+delimiter+b;
				}
			}			
		}catch(e){};
	};

	if(all_url.length>16){
		showMsg('提交当前页的链接地址');
		api_post_data_page('20141217addjob',all_url);

		process_to_next_page();	
		if(timeoutHandle) clearTimeout(timeoutHandle);
		timeoutHandle=setTimeout('load_one_job();',3000);	
	}else{
		showMsg('已经无法获取更多连接！');
	}
	
}

//**检查是否已经添加过了
function check_url_already_processed_add(tmp_url){
	if(tmp_url.in_array(lastAddUrl)){
		// jobMsg('skip task !');
		return true;
	}else{
		lastAddUrl.push(tmp_url);
		return false;
	}
}

function post_link_page_school(){
	jobMsg('开始处理任务');
	//当前页
	// var k=kisjs.strUtil;
	var htm = $("html")[0];
	jobMsg('开始提交数据');
	api_post_data_page2015('20150802',htm.innerHTML,false,function(){
		// alert('callback is run?');
		$("#process_current_page_url_flag").val('0');
		if(timeoutHandle) clearTimeout(timeoutHandle);
		timeoutHandle=setTimeout('reset_init();',2000);
	});

	return false;
}

function reset_init(){

	if (!window.confirm('你确定要 rest-init 吗？')) {		
		alert("用户取消了操作！");
		return false;
	}
	$("#process_current_page_url_flag").val('0');
	if(timeoutHandle) clearTimeout(timeoutHandle);
	timeoutHandle=setTimeout('check_process_flag();',2000);
}


function process_htmls(){
	init(2);
	jobMsg('开始处理任务');
	var htm = $("html")[0];
	jobMsg('开始提交数据');
	return api_post('2016putJobsHtml',htm.innerHTML,false,false);
}

function load_one_user() {
	jobMsg('准备切换用户');
	return load_job_api_call('api20150825getuser',load_user_callback);
}

function load_one_job2015(api) {
	// return load_job_api(api);
	return load_job_api_call(api,load_job_callback);
}

function load_one_job() {
	return load_job_api_call('api20150803getjob4school',load_job_callback);
	// return load_job_api('api20150803getjob4school');
}

function last_op_check (pos,url) {
	// lastOpCheck
	var max = 5;
	if(lastOpCheck.length==0){
		lastOpCheck[pos]=url;
		lastOpCheck[pos+1]=1;
	}else{
		if(lastOpCheck[pos]==url) {
			if (lastOpCheck[pos+1]>max){
				return true;	
			}else{
				lastOpCheck[pos+1]=+lastOpCheck[pos+1]++;
			}			
		}else{
			lastOpCheck[pos]=url;
			lastOpCheck[pos+1]=1;	
		}
	}	
	return false;
}

function check_task_url (turl) {
	var real_url = turl || '';
	var i = real_url.indexOf('mathRandom');
	if(i!=-1){
		real_url = real_url.substr(0,i);
	}
	return real_url;
}

function process_to_next_page(){
	// 下一页
	// var next_page = $(".paging_r")[0];
	// if(next_page){
	// 	showMsg('稍后(2s)转下一页');
	// 	if(timeoutHandle) clearTimeout(timeoutHandle);
	// timeoutHandle=setTimeout('goto_next_page();',2000);
	// }
	return false;
}

function goto_next_page(){
	showMsg('转到下一页，并稍后(3s)处理本页链接地址');
	$(".paging_r")[0].click();	
	if(timeoutHandle) clearTimeout(timeoutHandle);
	timeoutHandle=setTimeout('process_current_page_url();',3000);
}

function process_one_url_v2(txturl){
	// alert(htm);	
	if(txturl){
		shdic.ApiDebug.add('process_one_url_v2','process_one_url_v2');
		jobMsg('处理任务'+txturl);
		var k=kisjs.strUtil;
		var turl=k.replaceAll(txturl,'&amp;','&') ;
		// alert(turl);
		turl=url_add_mathRandom(turl);
		turl='http://www.ipin.com'+turl;
		jobMsg('打开任务任务窗口'+turl);
		window.open(turl);
		// setTimeout('load_one_job();',task_rnd(50000));
	}
}

// function process_one_url(htm){
// 	// alert(htm);
// 	var k=kisjs.strUtil;
// 	var txturl = k.cutMidStr(htm,'<a href="','"');
// 	if(txturl){
// 		var turl=k.replaceAll(txturl,'&amp;','&') ;
// 		// alert(turl);
// 		turl=url_add_mathRandom(turl);
// 		window.open(turl);
// 	}
// }


/********* ********* ********* ********* ********* 

********* ********* ********* ********* *********/

function api_post(api,dat,turl,callback){
	return api_post_data_page(api,dat,turl,callback);
}

function api_post_data_page(api,dat,turl,callback){
	if(callback && typeof(callback) == "function"){
		return api_post_data_page2015(api,dat,turl,callback);
	}else{
		return api_post_data_page2014(api,dat,turl,callback);
	}
}

function api_post_data_page2015(api,dat,turl,callback){
	var loc = encodeURIComponent(location.href);
	var newurl=API_URL+'?api=api'+api+"&verkey=7&loc_url=";
	if(turl){
		newurl = newurl +turl;
	}else{
		newurl = newurl +loc;
	}	
	newurl=url_add_mathRandom(newurl);
	var formParam=null;
	if(dat){
		if(typeof(dat)!='string'){
			dat = dat.serialize();//序列化表格内容为字符串 
		}
		formParam = {p1:dat};
	}else{
		alert('error!');
		return false;
	}

	shdic.ApiDebug.add('api_post_data_page2015',api);

	var aj = $.ajax( {
			url:newurl,
			data:formParam, 
			type:'post',
			cache:false,
			dataType:'json',
			success:function(data){				
				if(data.return_code && data.run_time){
					console.log(getFullTime()+'api_post_data_page:api'+data.return_code+' ,'+data.run_time+'秒.');
				}
				if(data.return_message){
					console.log(data.return_message);
				}
				if( callback ){
					callback();
				}
			}
		});
	return aj;
}


function url_add_mathRandom(surl){
	var newurl=surl;
	if(newurl.indexOf('?')==-1){
		newurl = newurl + "?mathRandom="+Math.random();
	}else{
		newurl = newurl + "&mathRandom="+Math.random();
	}
	return newurl;

}

function api_post_data_page2014(api,dat,turl,autocolse){
	var loc = encodeURIComponent(location.href);
	var newurl=API_URL+'?api=api'+api+"&verkey=7&loc_url=";
	if(turl){
		newurl = newurl +turl;
	}else{
		if(args["url"]!=undefined){
			newurl = newurl +args["url"];
		}else{
			newurl = newurl +loc;
		}
	}	
	newurl=url_add_mathRandom(newurl);
	var formParam=null;
	if(dat){
		if(typeof(dat)!='string'){
			dat = dat.serialize();//序列化表格内容为字符串 
		}
		formParam = {p1:dat};
	}else{
		alert('error!');
		return false;
	}

	shdic.ApiDebug.add('api_post_data_page2014',api);

	var aj = $.ajax( {
			url:newurl,
			data:formParam, 
			type:'post',
			cache:false,
			dataType:'json',
			success:function(data){				
				if(data.return_code && data.run_time){
					console.log(getFullTime()+'api_post_data_page:api'+data.return_code+' ,'+data.run_time+'秒.');
				}
				if(data.return_message){
					console.log(data.return_message);
				}
				if( autocolse ){
					// alert('autocolse !');
					if(timeoutHandle) clearTimeout(timeoutHandle);
					timeoutHandle=setTimeout('close_this_win()',500+task_rnd(1000));
				}
			}
		});
	return aj;
}


function showMsg(msg){	
	if(typeof(msg)!="undefined"){
		$("#app_msg").html(msg);	
	}else{
		return $("#app_msg").html();
	}
}

function jobMsg(msg){
	if(typeof(msg)!="undefined"){
		$("#job_msg").html(msg);	
	}else{
		return $("#job_msg").html();
	}
}

function jobStatus(msg){
	if(typeof(msg)!="undefined"){
		$("#job_status").html(msg);	
	}else{
		return $("#job_status").html();
	}
}

function jobMonitor(msg){
	if($("#job_monitor").length=0){			
			return false;
		}
	if(typeof(msg)!="undefined"){
		var br='\n';
		var kk=$("#job_monitor").html();
		if(kk.length==0){
			$("#job_monitor").html(msg);
			return 1;
		}
		if(kk.length>3000){
			kk='...';
		}
		$("#job_monitor").html(kk+br+msg);
	}else{
		return $("#job_monitor").html();
	}
}

function showCookie(){
	$("#job_monitor").html('firstTime:'+firstTime);
	// jobMonitor('_u_='+GetCookieValue('_u_')+';');
	// jobMonitor('_t_='+GetCookieValue('_t_')+';');
	
	jobMonitor('jid='+GetCookieValue(jid)+';');
	jobMonitor('count='+GetCookieValue(jid+'_count')+';');

	shdic.ApiDebug.show();
}

function api_user_login(datas){

	shdic.ApiDebug.add('api_user_login','api_user_login');

	if(datas && datas.next_uid && datas.next_upw){
		//basic check
	}else{
		console.log('api_user_login() miss UID or UPW !');
	}

	var newurl='http://www.ipin.com/account/callback/mobile?code=~~';
	var formParam=null;
	formParam = {login_back:"http://www.ipin.com/school/schoolFilter.do",
	mobile:datas.next_uid,password:datas.next_upw};
	var aj = $.ajax( {
			url:newurl,
			data:formParam, 
			type:'post',
			cache:false,
			dataType:'json',
			success:function(data){				
				console.log('api_user_login() success !');
				if(timeoutHandle) clearTimeout(timeoutHandle);
				timeoutHandle=setTimeout('check_process_flag();',5500+task_rnd(1500));
			}
		});
	return aj;
}

function load_job_api(api) {
	jobMsg('取任务');

	if(bigDelayMaxCount>0){
		bigDelayCount++;
		updateCookie(3);
		if(bigDelayCount>bigDelayMaxCount){
			bigDelayCount=0;
			updateCookie(2);
			// if(timeoutHandle) clearTimeout(timeoutHandle);
			// timeoutHandle=setTimeout('check_process_flag();',bigDelayTime*1000+task_rnd(3000));
			return ;
		}		
	}


	shdic.ApiDebug.add('load_job_api',api);

	var loc = encodeURIComponent(location.href);
	var newurl=API_URL+'?api='+api+"&verkey=7&loc_url=";
	newurl = newurl +loc;
	newurl=url_add_mathRandom(newurl);
	
	var aj = $.ajax( {
			url:newurl,
			data:{p1:''}, 
			type:'post',
			cache:false,
			dataType:'json',
			success:function(data){
				try{
					if(data.return_code && data.run_time){
						console.log(getFullTime()+'load_job_api:api'+data.return_code+' ,'+data.run_time+'秒.');
					}
					if(data.return_message){
						console.log(data.return_message);
					}

					if(data.datas.wait){
						if(timeoutHandle) clearTimeout(timeoutHandle);
						timeoutHandle=setTimeout('check_process_flag();',5500+task_rnd(1500));
						return ;
					}

					jobMsg('取到任务'+data.datas.next_url);

					var tmp_url= check_task_url(data.datas.next_url);
					if(tmp_url == ''){
						jobMsg('没有任务要处理了! process_ok');
						$("#job_status").html('没有任务要处理了! process_ok');

						$("#process_current_page_url_flag").val('0');

						// alert("process ok!");
						return ;
					}
					//处理过的链接，不提交，skip，取下一个任务
					if(tmp_url.in_array(lastOpenUrl)){
						jobMsg('skip task !');
						if(last_op_check(0,tmp_url)){
							jobMsg('连续多次无法取到新任务了! process_end');
							jobStatus('没有任务要处理了! process_end');
							// alert("process End!");
							return ;
						}
						// if(timeoutHandle) clearTimeout(timeoutHandle);
						// timeoutHandle=setTimeout('check_process_flag();',11000+task_rnd(1500));
						return ;
					}else{
						lastOpenUrl.push(tmp_url);
						process_one_url_v2(tmp_url);	
					}
				}catch(ex){
					console.log(ex.name + ": " + ex.message);

					if(timeoutHandle) clearTimeout(timeoutHandle);
					timeoutHandle=setTimeout('check_process_flag();',5500+task_rnd(1500));
					return ;
				};
				
			}
		});

	var tt=jobStatus();
	tt = tt.indexOf('process_');
	if(tt!=-1){
		return;
	}

	if(timeoutHandle) clearTimeout(timeoutHandle);
	var tt=6000+task_rnd(5600);
	jobStatus('setTimeout:'+tt);
	timeoutHandle=setTimeout('check_process_flag();',tt);
		
	return aj;	
}

function load_job_api_call(api,callback) {
	jobMsg('取任务');

	if(bigDelayMaxCount>0){
		bigDelayCount++;
		updateCookie(3);
		if(bigDelayCount>bigDelayMaxCount){
			bigDelayCount=0;
			updateCookie(2);
			// if(timeoutHandle) clearTimeout(timeoutHandle);
			// timeoutHandle=setTimeout('check_process_flag();',bigDelayTime*1000+task_rnd(3000));
			return ;
		}
	}

	shdic.ApiDebug.add('load_job_api_call',api);

	var loc = encodeURIComponent(location.href);
	var newurl=API_URL+'?api='+api+"&verkey=7&loc_url=";
	newurl = newurl +loc;
	newurl=url_add_mathRandom(newurl);
	
	var aj = $.ajax( {
			url:newurl,
			data:{p1:''}, 
			type:'post',
			cache:false,
			dataType:'json',
			success:function(data){
				callback(data);				
			}
		});

	var tt=jobStatus();
	tt = tt.indexOf('process_');
	if(tt!=-1){
		return;
	}

	if(timeoutHandle) clearTimeout(timeoutHandle);
	var tt=6000+task_rnd(5600);
	jobStatus('api.setTimeout:'+tt);
	timeoutHandle=setTimeout('check_process_flag();',tt);	
		
	return aj;	
}

function load_job_callback(data){
	try{
		if(data.return_code && data.run_time){
			console.log(getFullTime()+'load_job_callback:api'+data.return_code+' ,'+data.run_time+'秒.');
		}
		if(data.return_message){
			console.log(data.return_message);
		}

		if(data.datas.wait){
			if(timeoutHandle) clearTimeout(timeoutHandle);
			var tt=5500+task_rnd(1500);
			jobStatus('api.job.callback.wait.setTimeout='+tt);
			timeoutHandle=setTimeout('check_process_flag();',tt);
			return ;
		}

		jobMsg('取到任务'+data.datas.next_url);

		var tmp_url= check_task_url(data.datas.next_url);
		if(tmp_url == ''){
			jobMsg('没有任务要处理了! process_ok');
			jobStatus('没有任务要处理了! process_ok');

			$("#process_current_page_url_flag").val('0');
			// alert("process ok!");
			return ;
		}
		//处理过的链接，不提交，skip，取下一个任务
		if(tmp_url.in_array(lastOpenUrl)){
			jobMsg('skip task !');
			if(last_op_check(0,tmp_url)){
				jobMsg('连续多次无法取到新任务了! process_end');
				jobStatus('没有任务要处理了! process_end');
				// alert("process End!");
				return ;
			}
			return ;
		}else{
			lastOpenUrl.push(tmp_url);
			process_one_url_v2(tmp_url);	
		}
	}catch(ex){
		console.log("load_job_callback() error! "+ex.name + ": " + ex.message);

		if(timeoutHandle) clearTimeout(timeoutHandle);
		var tt=5500+task_rnd(1500);
		jobStatus('api.job.callback.error.setTimeout='+tt);
		timeoutHandle=setTimeout('check_process_flag();',tt);
		return ;
	};
}


function load_user_callback(data){
	try{
		if(data && data.return_code && data.run_time){
			console.log(getFullTime()+'load_user_callback:api'+data.return_code+' ,'+data.run_time+'秒.');
		}
		if(data.return_message){
			console.log(data.return_message);
		}

		if(data.datas && data.datas.next_uid && data.datas.next_upw){
			jobMsg('取到uid'+data.datas.next_uid);
			api_user_login(data.datas);

		}else{
			jobMsg('没有可用的用户了! process_ok');
			jobStatus('没有可用的用户了! process_ok');
			$("#process_current_page_url_flag").val('0');
			return ;
		}

	}catch(ex){
		console.log("load_user_callback() error! "+ex.name + ": " + ex.message);

		if(timeoutHandle) clearTimeout(timeoutHandle);
		timeoutHandle=setTimeout('check_process_flag();',5500+task_rnd(1500));
		return ;
	};
}

function local_tools_js(){
	var root='http://www.shdic.com/webapp/p2015eafahjjflheajmehbbnalifjgpakbpj/exp.php?';
	var turl=root+'ajax=js&c=admin&a=chkschmaj';

	if(workMode==6){
		turl=root+'c=admin&a=toxls';
	}

	var auto=1;
	if(bigDelayMaxCount>0){
		bigDelayCount++;
		if(bigDelayCount>10){
			bigDelayCount=0;
			auto=0;
		}		
	}


	if (auto || window.confirm('你确定要[自动开始]吗？')) {
		process_open_url(turl);

		if(timeoutHandle) clearTimeout(timeoutHandle);
		var tt=3000+task_rnd(1000);
		//jobStatus('setTimeout:'+tt);
		timeoutHandle=setTimeout('local_tools_js();',tt);
	}else{
		$("#process_current_page_url_flag").val('0');
		if(timeoutHandle) clearTimeout(timeoutHandle);
		timeoutHandle=setTimeout('check_process_flag();',5500+task_rnd(1500));
	}
	return ;
}

function process_open_url(txturl){
	if(txturl){
		shdic.ApiDebug.add('process_open_url','process_open_url');
		jobMsg('处理任务'+txturl);
		var k=kisjs.strUtil;
		var turl=k.replaceAll(txturl,'&amp;','&') ;
		// alert(turl);
		turl=url_add_mathRandom(turl);
		jobMsg('打开任务任务窗口'+turl);
		window.open(turl);
	}
}

function task_rnd(num){
	return(taskDelayTimeMin+rand(num)+rand(taskDelayTimeMax-taskDelayTimeMin));
}

//获取url中的参数
function GetUrlParms()
{

    var args=new Object();   

    var query=location.search.substring(1);//获取查询串   

    var pairs=query.split("&");//在逗号处断开   

    for(var   i=0;i<pairs.length;i++)   

    {   

        var pos=pairs[i].indexOf('=');//查找name=value   

            if(pos==-1)   continue;//如果没有找到就跳过   

            var argname=pairs[i].substring(0,pos);//提取name   

            var value=pairs[i].substring(pos+1);//提取value   

            args[argname]=unescape(value);//存为属性   

    }

    return args;

}

var args = new Object();	args = GetUrlParms();
