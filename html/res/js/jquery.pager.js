﻿/*
* jQuery pager plugin
* Version 1.0 (12/22/2008)
* @requires jQuery v1.2.6 or later
* @refacted by zz (charles.zz.life@gmail.com)
*  var pager = $('#pager').pager({
*		limit:15,
*		buttonClickCallback:function(start,pager){
*			loadPage(start,pager);
*		}
*	});
*	loadPage(0,pager);
*    function loadPage(start,pager){
*	....
*	pager.setPageData({
*		start:start,
*		total:result.data
*	});
* Example at: http://jonpauldavies.github.com/JQuery/Pager/PagerDemo.html
*
*/
(function($) {
	//refact by zz(charles.zz.life@gmail.com)
    $.fn.pager = function(options) {

        this.opts = $.extend({}, $.fn.pager.defaults, options);
        this.limit = this.opts.limit;
        this.start = this.opts.start;
        
        // specify correct cursor activity
        $('.pages li').mouseover(function() { document.body.style.cursor = "pointer"; }).mouseout(function() { document.body.style.cursor = "auto"; });
        this.setPageData = function(config){
        	var options = $.extend(this.opts,config);
        	 // empty out the destination element and then render out the pager with the supplied options
            var pagenumber = getPageNO(options.start,options.limit,options.total);
            var pagecount = getPageCount(options.start,options.limit,options.total);
        	$(this).empty().append(this.renderpager(pagenumber,pagecount, options.buttonClickCallback));
           
        }
        this.renderpager = function (pagenumber, pagecount, buttonClickCallback) {
        	// setup $pager to hold render
            var $pager = $('<ul class="pages"></ul>');

            // add in the previous and next buttons
            $pager.append(this.renderButton('first', pagenumber, pagecount, buttonClickCallback)).append(this.renderButton('prev', pagenumber, pagecount, buttonClickCallback));

            // pager currently only handles 10 viewable pages ( could be easily parameterized, maybe in next version ) so handle edge cases
            var startPoint = 1;
            var endPoint = 9;

            if (pagenumber > 4) {
                startPoint = pagenumber - 4;
                endPoint = pagenumber + 4;
            }

            if (endPoint > pagecount) {
                startPoint = pagecount - 8;
                endPoint = pagecount;
            }

            if (startPoint < 1) {
                startPoint = 1;
            }
            

            // loop thru visible pages and render buttons
            for (var page = startPoint; page <= endPoint; page++) {
            	var start = getStart(page,this.opts.limit,pagecount);
            	(function(start,pager){
                    
    	    			
    	            var currentButton = $('<li class="page-number">' + (page)+ '</li>');
    	            page == pagenumber ? currentButton.addClass('pgCurrent') : currentButton.click(function(){
    	            	pager.displayContainer.html(pager.opts.loadingMsg);
    	            	buttonClickCallback(start,pager); 
    	            	
    	            });
    	            currentButton.appendTo($pager);
            	}(start,this));
            }

            // render in the next and last buttons before returning the whole rendered control back.
            $pager.append(this.renderButton('next', pagenumber, pagecount, buttonClickCallback)).append(this.renderButton('last', pagenumber, pagecount, buttonClickCallback));
            this.displayContainer = this.renderDisplayMsg(this.opts.total);
            
            $pager.append(this.displayContainer);
            return $pager;
        }
        
     // renders and returns a 'specialized' button, ie 'next', 'previous' etc. rather than a page number button
        this.renderButton = function(buttonLabel, pagenumber, pagecount, buttonClickCallback) {

            var destPage = 1;
            var label = buttonLabel;
            // work out destination page for required button type
            switch (buttonLabel) {
                case "first":
                    destPage = 1;
                    label = this.opts.firstLabel;
                    break;
                case "prev":
                    destPage = pagenumber - 1;
                    label = this.opts.prevLabel;
                    break;
                case "next":
                    destPage = pagenumber + 1;
                    label = this.opts.nextLabel;
                    break;
                case "last":
                    destPage = pagecount;
                    label = this.opts.lastLabel;
                    break;
            }
            var $Button = $('<li class="pgNext">' + label + '</li>');

            
            var pager = this;
            var start = getStart(destPage,pager.opts.limit,pagecount);
            // disable and 'grey' out buttons if not needed.
            if (buttonLabel == "first" || buttonLabel == "prev") {
            	pagenumber <= 1 ? $Button.addClass('pgEmpty') : $Button.click(function() { 
            		pager.displayContainer.html(pager.opts.loadingMsg);
            		buttonClickCallback(start,pager); 
            	});
            }
            else {
                pagenumber >= pagecount ? $Button.addClass('pgEmpty') : $Button.click(function() { 
                	pager.displayContainer.html(pager.opts.loadingMsg);
                	buttonClickCallback(start,pager); 
                });
            }

            return $Button;
        }
        
        this.renderDisplayMsg=function(total){
	       	var $display = $('<li class="page-display">'+formatString(this.opts.displayMsg,total)+'</li>');
	       	return $display;
	    }
        var displayMsg = this.opts.displayMsg;
        this.setPageData({displayMsg:this.opts.loadingMsg});
        this.opts.displayMsg = displayMsg;
        return this;
    };
    function formatString(){
    	var s = arguments[0];
    	for (var i = 0; i < arguments.length - 1; i++) {
    		var reg = new RegExp("\\{" + i + "\\}", "gm");
    		s = s.replace(reg, arguments[i + 1]);
    	}
    	return s;
    }
    function getPageNO(start,limit,total){
    	var pageNO = start/limit;
    	if(pageNO<1)pageNO=1;
    	if(pageNO*limit==start)pageNO++;
    	return pageNO;
    }
    function getPageCount(start,limit,total){
    	var pageCount = Math.round(total/limit);
    	if(pageCount<1)pageCount=1;
    	if(pageCount*limit<total) pageCount++;
    	return pageCount;
    }
    
    function getStart(pageNO,limit,pageCount){
    	var start = (pageNO-1)*limit;
    	if(start<0) start =0;
    	return start;
    }
    

    // pager defaults. hardly worth bothering with in this case but used as placeholder for expansion in the next version
    $.fn.pager.defaults = {
        //pagenumber: 1,
        //pagecount: 1,
        start:0,
        limit:15,
        total:0,
        lazy:false,
        firstLabel:'首页',
        prevLabel:'上一页',
        nextLabel:'下一页',
        lastLabel:'末页',
        displayMsg:'共{0}条数据',
        loadingMsg:'正在加载...'
    };
    
    

})(jQuery);

