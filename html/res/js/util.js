var Util = (function(Util){

	function _format(pattern,num,z){
		var j =	pattern.length >= num.length ? pattern.length : num.length ;
		var p = pattern.split("");
		var n = num.split("");
		var bool = true,nn = "";
		for(var i=0;i<j;i++){
			var x = n[n.length-j+i];
			var y = p[p.length-j+i];
			if( z == 0){
				if(bool){
					if( ( x && y && (x != "0" || y == "0")) || ( x && x != "0" && !y ) || ( y && y == "0" && !x )  ){
						nn += x ? x : "0";
						bool = false;
					}	
				} else {
					nn +=  x ? x : "0" ;
				}
			} else {
				if( y && ( y == "0" || ( y == "#" && x ) ))
					nn += x ? x : "0" ;								
			}
		}
		return nn;
	}
	function _formatNumber(numChar,pattern){
		var patterns = pattern.split(".");
		var	numChars = numChar.split(".");
		var z = patterns[0].indexOf(",") == -1 ? -1 : patterns[0].length - patterns[0].indexOf(",") ;
		var num1 = _format(patterns[0].replace(","),numChars[0],0);
		var num2 = _format(	patterns[1]?patterns[1].split('').reverse().join(''):"", numChars[1]?numChars[1].split('').reverse().join(''):"",1);
		num1 =	num1.split("").reverse().join('');
		var reCat = eval("/[0-9]{" + (z-1) + "," + (z-1) + "}/gi");
		var arrdata = z > -1 ? num1.match(reCat) : undefined ;
		if( arrdata && arrdata.length > 0 ){
			var w =	num1.replace(arrdata.join(''),'');
			num1 = arrdata.join(',') + ( w == "" ? "" : "," ) + w ;
		}
		num1 = num1.split("").reverse().join("");
		return (num1 == "" ? "0" : num1) + (num2 != "" ? "." + num2.split("").reverse().join('') : "" ); 					
	} 
	
	Util.formatNumber = function(num,opt){
		var reCat = /[0#,.]{1,}/gi;
		var zeroExc = opt.zeroExc == undefined ? true : opt.zeroExc ;
		var pattern = opt.pattern.match(reCat)[0];
		var numChar = num.toString();
		return !(zeroExc && numChar == 0) ? opt.pattern.replace(pattern,_formatNumber(numChar,pattern)) : opt.pattern.replace(pattern,"0");
	}
	Util.ip2long=function(IP) {
	  // http://kevin.vanzonneveld.net
	  // +   original by: Waldo Malqui Silva
	  // +   improved by: Victor
	  // +    revised by: fearphage (http://http/my.opera.com/fearphage/)
	  // +    revised by: Theriault
	  // *     example 1: ip2long('192.0.34.166');
	  // *     returns 1: 3221234342
	  // *     example 2: ip2long('0.0xABCDEF');
	  // *     returns 2: 11259375
	  // *     example 3: ip2long('255.255.255.256');
	  // *     returns 3: false
	  var i = 0;
	  // PHP allows decimal, octal, and hexadecimal IP components.
	  // PHP allows between 1 (e.g. 127) to 4 (e.g 127.0.0.1) components.
	  IP = IP.match(/^([1-9]\d*|0[0-7]*|0x[\da-f]+)(?:\.([1-9]\d*|0[0-7]*|0x[\da-f]+))?(?:\.([1-9]\d*|0[0-7]*|0x[\da-f]+))?(?:\.([1-9]\d*|0[0-7]*|0x[\da-f]+))?$/i); // Verify IP format.
	  if (!IP) {
	    return false; // Invalid format.
	  }
	  // Reuse IP variable for component counter.
	  IP[0] = 0;
	  for (i = 1; i < 5; i += 1) {
	    IP[0] += !! ((IP[i] || '').length);
	    IP[i] = parseInt(IP[i]) || 0;
	  }
	  // Continue to use IP for overflow values.
	  // PHP does not allow any component to overflow.
	  IP.push(256, 256, 256, 256);
	  // Recalculate overflow of last component supplied to make up for missing components.
	  IP[4 + IP[0]] *= Math.pow(256, 4 - IP[0]);
	  if (IP[1] >= IP[5] || IP[2] >= IP[6] || IP[3] >= IP[7] || IP[4] >= IP[8]) {
	    return false;
	  }
	  return IP[1] * (IP[0] === 1 || 16777216) + IP[2] * (IP[0] <= 2 || 65536) + IP[3] * (IP[0] <= 3 || 256) + IP[4] * 1;
	};
	Util.long2ip=function(ip) {
	  // http://kevin.vanzonneveld.net
	  // +   original by: Waldo Malqui Silva
	  // *     example 1: long2ip( 3221234342 );
	  // *     returns 1: '192.0.34.166'
	  if (!isFinite(ip))
	    return false;
	
	  return [ip >>> 24, ip >>> 16 & 0xFF, ip >>> 8 & 0xFF, ip & 0xFF].join('.');
	}
	return Util;
})(window.Util||{})