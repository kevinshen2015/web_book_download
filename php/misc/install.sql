
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `app_survival_camp_v1`
--

-- --------------------------------------------------------

--
-- Table structure for table `activecode`
--

CREATE TABLE IF NOT EXISTS `shdic_sc2015_activecode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL,
  `creator_uid` int(11) NOT NULL,
  `timeline` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `keyvalue`
--

CREATE TABLE IF NOT EXISTS `shdic_sc2015_keyvalue` (
  `key` varchar(64) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  UNIQUE KEY `key` (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `online`
--
CREATE TABLE IF NOT EXISTS `shdic_sc2015_online` (
  `uid` int(11) NOT NULL,
  `last_active` datetime NOT NULL,
  `session` varchar(32) NOT NULL,
  `device` varchar(32) DEFAULT NULL,
  `place` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  KEY `last_active` (`last_active`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--


CREATE TABLE IF NOT EXISTS `shdic_sc2015_user` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `pinyin` varchar(255) NOT NULL,
  
  `email` varchar(255) NOT NULL,
  `password` varchar(32) NOT NULL,
  
  `tel` varchar(32) NOT NULL,
  `mobile` varchar(32) NOT NULL,
    
  `avatar_small` varchar(255) NOT NULL,
  `avatar_normal` varchar(255) NOT NULL,
  
  `level` tinyint(1) NOT NULL default '1',
  `is_closed` tinyint(1) NOT NULL default '0',
  `feed_settings` varchar(255) NOT NULL,
  `allow_mail` tinyint(1) NOT NULL default '1' COMMENT '是否接收邮件',
  `timeline` datetime NOT NULL,
  `memo` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10000 ;

-- INSERT INTO `shdic_sc2015_user` (`id`, `name`, `pinyin`, `email`, `password`, `avatar_small`, `avatar_normal`, `level`, `timeline`, `feed_settings`, `is_closed`, `mobile`, `tel`, `allow_mail`, `memo`) VALUES
-- (1, 'admin', 'admin', 'scadm@shdic.com', '{password}', '', '', 9, '2015-03-09 21:37:03', '', 0, '', '', '', '');

INSERT INTO `shdic_sc2015_user` (`id`, `name`, `pinyin`, `email`, `password`, `tel`, `mobile`, `avatar_small`, `avatar_normal`, `level`, `is_closed`, `feed_settings`, `allow_mail`, `timeline`, `memo`) VALUES
(1, 'admin', 'admin', 'scadm@shdic.com', 'af87caa09ce7373b53f0581156fd3f', '', '', '', '', 9, 0, '', 0, '2015-03-09 21:37:03', ''),
(10000, 'test1', 'test1', 'test1@qq.com', 'c6b5f0a06e361ea1c6debb9a27582145', '', '', '', '', 1, 0, '', 1, '2015-03-10 10:38:40', ''),
(10001, 'test3', 'test3', 'test3@qq.com', 'd3643da373bac892ca3fd12cd69fdda4', '', '', '', '', 1, 0, '', 1, '2015-03-10 10:48:23', '');

drop table shdic_sc2015_user_info;

CREATE TABLE IF NOT EXISTS `shdic_sc2015_user_info` (
  `uid` int(11) unsigned NOT NULL ,

  `level` int(11) unsigned NOT NULL default '1',

  `exp` int(11) unsigned NOT NULL default '0',
  `exp_max` int(11) unsigned NOT NULL default '100',

  `status` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,

  `pos_x` int(11) unsigned NOT NULL default '1000',
  `pos_y` int(11) unsigned NOT NULL default '1000',

  `attr1` int(11) unsigned NOT NULL default '0' COMMENT '属性1',
  `attr2` int(11) unsigned NOT NULL default '0' COMMENT '属性2',
  `attr3` int(11) unsigned NOT NULL default '0' COMMENT '属性3',
  `attr4` int(11) unsigned NOT NULL default '0' COMMENT '属性4',
  `attr5` int(11) unsigned NOT NULL default '0' COMMENT '属性5',

  `skill1` tinyint(1) unsigned NOT NULL default '1' COMMENT '技能1数值',
  `skill2` tinyint(1) unsigned NOT NULL default '0' COMMENT '技能2数值',
  `skill3` tinyint(1) unsigned NOT NULL default '0' COMMENT '技能3数值',
  `skill4` tinyint(1) unsigned NOT NULL default '0' COMMENT '技能4数值',
  `skill5` tinyint(1) unsigned NOT NULL default '0' COMMENT '技能5数值',

  `skill1_id` tinyint(1) unsigned NOT NULL default '2' COMMENT '技能1的ID',
  `skill2_id` tinyint(1) unsigned NOT NULL default '0' COMMENT '技能2的ID',
  `skill3_id` tinyint(1) unsigned NOT NULL default '0' COMMENT '技能3的ID',
  `skill4_id` tinyint(1) unsigned NOT NULL default '0' COMMENT '技能4的ID',
  `skill5_id` tinyint(1) unsigned NOT NULL default '0' COMMENT '技能5的ID',
  
  `memo` varchar(255) NOT NULL,
  PRIMARY KEY  (`uid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8  ;

ALTER TABLE  `shdic_sc2015_user_info` ADD  `sp` TINYINT( 11 ) NOT NULL DEFAULT '100' COMMENT  'Stamina Points' AFTER  `pos_y` ;

ALTER TABLE  `shdic_sc2015_user_info` ADD  `hp` TINYINT( 11 ) NOT NULL DEFAULT '1' COMMENT  '生命值' AFTER  `pos_y` ;
ALTER TABLE  `shdic_sc2015_user_info` ADD  `combat` TINYINT( 11 ) NOT NULL DEFAULT '0' COMMENT  '战斗力' AFTER  `pos_y` ;
ALTER TABLE  `shdic_sc2015_user_info` ADD  `defense` TINYINT( 11 ) NOT NULL DEFAULT '0' COMMENT  '防御力' AFTER  `pos_y` ;



INSERT INTO `shdic_sc2015_user_info` (`uid`, `exp`, `exp_max`, `status`, `position`, `attr1`, `attr2`, `attr3`, `attr4`, `attr5`) VALUES
(10000, 1, 100, '休息', '营地', 5, 4, 3, 2, 1),
(10001, 50, 200, '受伤|兴奋', 'map3', 11, 12, 13, 14, 15);



CREATE TABLE IF NOT EXISTS `shdic_sc2015_simple_config` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `title` varchar(50) NOT NULL,
    `code` char(32) NOT NULL,
    `is_his` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=his',
    `category` int(11) unsigned NOT NULL,
    `ud1` varchar(32) NOT NULL,
    `ud2` varchar(32) NOT NULL,
    `ud3` varchar(32) NOT NULL,
    `ud4` varchar(32) NOT NULL,
    `ud5` varchar(32) NOT NULL,
    `ud6` varchar(32) NOT NULL,
    `ud7` varchar(32) NOT NULL,
    `ud8` varchar(32) NOT NULL,
    `ud9` varchar(32) NOT NULL,
    `create_date` char(20) NOT NULL,
    `modify_date` char(20) NOT NULL,
    `parent_id` int(11) unsigned NOT NULL DEFAULT '0',
    `sort` tinyint(4) unsigned NOT NULL DEFAULT '99',
    PRIMARY KEY (`id`),
    UNIQUE KEY `code` (`code`)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

INSERT INTO `shdic_sc2015_simple_config` (`id`, `title`, `code`, `is_his`, `category`, `ud1`, `ud2`, `ud3`, `ud4`, `ud5`, `ud6`, `ud7`, `ud8`, `ud9`, `create_date`, `modify_date`, `parent_id`, `sort`) VALUES ('1', 'skill config', 'skill-root', '0', '1', '', '', '', '', '', '', '', '', '', '', '', '0', '99');

INSERT INTO `shdic_sc2015_simple_config` (`title`, `code`, `is_his`, `category`, `ud1`, `ud2`, `ud3`, `ud4`, `ud5`, `ud6`, `ud7`, `ud8`, `ud9`, `create_date`, `modify_date`, `parent_id`, `sort`) VALUES 
('通用', 'skill-General', '0', '1', '', '', '', '', '', '', '', '', '', '', '', '1', '99'),
('格斗', 'skill-Combat', '0', '1', '', '', '', '', '', '', '', '', '', '', '', '1', '99'),
('射击', 'skill-Shooting', '0', '1', '', '', '', '', '', '', '', '', '', '', '', '1', '99'),
('器械', 'skill-Apparatus', '0', '1', '', '', '', '', '', '', '', '', '', '', '', '1', '99'),
('医疗', 'skill-Medical', '0', '1', '', '', '', '', '', '', '', '', '', '', '', '1', '99');



CREATE TABLE IF NOT EXISTS `shdic_sc2015_exp_buff` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `uid` int(11) unsigned NOT NULL COMMENT 'user_id',

    `title` varchar(50) NOT NULL COMMENT '提示',    
    `is_his` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=his',
    `category` int(11) unsigned NOT NULL COMMENT '分类',
    `op` tinyint(4) unsigned NOT NULL COMMENT '操作',
    `num` tinyint(4) unsigned NOT NULL COMMENT '变动值',

    `create_date` char(20) NOT NULL,
    `modify_date` char(20) NOT NULL,
    PRIMARY KEY (`id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;



CREATE TABLE IF NOT EXISTS `shdic_sc2015_item_info` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `ffuid` int(11) unsigned NOT NULL COMMENT '提交者user_id',

    `title` varchar(50) NOT NULL COMMENT '名称',
    `desc` varchar(250) NOT NULL COMMENT '描述',

    `is_his` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=his',
    `category` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '分类1',
    `sub_category` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '分类2',

    `max_holding` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '最大持有数量',
    `max_stack` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '最大堆叠数',
    `single_weight` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '单个重量g',
    `quality` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '品质',

    `in_use` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '正在使用/装备',

    `num1` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '主数值',
    `num2` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '次数值',
    `durable` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '耐久',

    `create_date` char(20) NOT NULL,
    `modify_date` char(20) NOT NULL,
    PRIMARY KEY (`id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


INSERT INTO `shdic_sc2015_item_info` (`id`, `ffuid`, `title`, `desc`, `is_his`, `category`, `sub_category`, `create_date`, `modify_date`) VALUES
(1, 10000, '瓶装饮用水1000ml', '某品牌的1000毫升装的瓶装饮用水', 0, 0, 0, '2015-03-19 11:10:26', '2015-03-19 11:10:26'),
(2, 10000, '方便面', '可补充大量能量', 0, 0, 0, '', ''),
(3, 10000, '完整的棒球棍', '一根完整的棒球棍，也许是个有用的东西！', 0, 0, 0, '2015-03-19 11:42:40', '2015-03-19 11:42:40'),
(4, 10008, '木质陷阱  ', '模仿食物链顶端的男人制作的木质陷阱，有一定几率猎杀小型丧尸', 0, 0, 0, '2015-03-20 10:37:29', '2015-03-20 10:37:29'),
(5, 10008, '绊索（营地防御类）', '用这种东东对付没有智慧的丧尸再合适不过了，等到丧尸倒地，去强势插入吧～', 0, 0, 0, '2015-03-20 10:44:03', '2015-03-20 10:44:03');

INSERT INTO `shdic_sc2015_item_info` (`id`, `ffuid`, `title`, `desc`, `is_his`, `category`, `sub_category`, `max_holding`, `max_stack`, `single_weight`, `quality`, `in_use`, `num1`, `num2`, `durable`, `create_date`, `modify_date`) VALUES (NULL, '10005', '餐刀', '家居必备，可用于切东西、撬罐头之类。贴身肉搏时可施展一击必杀，但是谁要是用它对付丧尸……', '0', '2', '12', '100', '10', '250', '1', '0', '1', '1', '300', '', '');


INSERT INTO `shdic_sc2015_item_info` (`id`, `ffuid`, `title`, `desc`, `is_his`, `category`, `sub_category`, `max_holding`, `max_stack`, `single_weight`, `quality`, `in_use`, `num1`, `num2`, `durable`, `create_date`, `modify_date`) VALUES (NULL, '10000', '一套衣服', '这套衣服比路边捡来的旧衣服还是有些优点的，不仅提供了四个口袋，还看起来曾经被仔细清洗过。', '0', '2', '5', '10', '1', '1500', '1', '0', '0', '0', '100', '', '');

CREATE TABLE IF NOT EXISTS `shdic_sc2015_event_info` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `ffuid` int(11) unsigned NOT NULL COMMENT '提交者user_id',

    `title` varchar(50) NOT NULL COMMENT '名称',
    `desc` varchar(250) NOT NULL COMMENT '描述',

    `is_his` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=his',
    `category` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '分类1',
    `sub_category` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '分类2',

    `create_date` char(20) NOT NULL,
    `modify_date` char(20) NOT NULL,
    PRIMARY KEY (`id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;



-- -- -- 眼部、面部、头部、防弹背心、衣服（四个口袋）、背包、 战术背心，挎包、腿包
-- CREATE TABLE IF NOT EXISTS `shdic_sc2015_user_equip` (
--     `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
--     `title` varchar(50) NOT NULL,
--     `is_his` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=his',
--     `category` int(11) unsigned NOT NULL DEFAULT '0',
--     `item_id` int(11) unsigned NOT NULL DEFAULT '0',
--     `original_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '原纪录id',

--     `ud1` varchar(32) NOT NULL DEFAULT '' COMMENT  '持有数量',
--     `ud2` varchar(32) NOT NULL DEFAULT '' COMMENT  '最大堆叠数',
--     `ud3` varchar(32) NOT NULL DEFAULT '' COMMENT  '类别标题',
--     `ud4` varchar(32) NOT NULL DEFAULT '' COMMENT  '单个重量',
--     `ud5` varchar(32) NOT NULL DEFAULT '' COMMENT  '品质',
--     `ud6` varchar(32) NOT NULL DEFAULT '' COMMENT  '',
--     `ud7` varchar(32) NOT NULL DEFAULT '',
--     `ud8` varchar(32) NOT NULL DEFAULT '',
--     `ud9` varchar(32) NOT NULL DEFAULT '',

--     `ud1` varchar(32) NOT NULL DEFAULT '' COMMENT  '眼部',
--     `ud2` varchar(32) NOT NULL DEFAULT '' COMMENT  '面部',
--     `ud3` varchar(32) NOT NULL DEFAULT '' COMMENT  '头部',
--     `ud4` varchar(32) NOT NULL DEFAULT '' COMMENT  '防弹背心',
--     `ud5` varchar(32) NOT NULL DEFAULT '' COMMENT  '衣服',
--     `ud6` varchar(32) NOT NULL DEFAULT '' COMMENT  '背包',
--     `ud7` varchar(32) NOT NULL DEFAULT '' COMMENT  '战术背心',
--     `ud8` varchar(32) NOT NULL DEFAULT '' COMMENT  '挎包',
--     `ud9` varchar(32) NOT NULL DEFAULT '' COMMENT  '腿包',
--     `create_date` char(20) NOT NULL DEFAULT '',
--     `modify_date` char(20) NOT NULL DEFAULT '',
--     `uid` int(11) unsigned NOT NULL DEFAULT '0',
--     `order_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT  '订单id',
--     `sort` tinyint(4) unsigned NOT NULL DEFAULT '99',
--     PRIMARY KEY (`id`)
--   ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;



CREATE TABLE IF NOT EXISTS `shdic_sc2015_pc_item_info` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `title` varchar(50) NOT NULL,
    `is_his` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=his',
    
    `main_category` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '记录分类',

    `category` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '分类1',
    `sub_category` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '分类2',

    `item_id` int(11) unsigned NOT NULL DEFAULT '0',
    `original_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '原纪录id',

    `max_holding` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '最大持有数量',
    `max_stack` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '最大堆叠数',
    `single_weight` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '单个重量g',
    `quality` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '品质',

    `in_use` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '正在使用/装备',

    `num1` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '主数值',
    `num2` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '次数值',
    `durable` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '耐久',

    `create_date` char(20) NOT NULL DEFAULT '',
    `modify_date` char(20) NOT NULL DEFAULT '',
    `uid` int(11) unsigned NOT NULL DEFAULT '0',
    `order_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT  '订单id',
    `sort` tinyint(4) unsigned NOT NULL DEFAULT '99',
    PRIMARY KEY (`id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


--High-density operation
CREATE TABLE IF NOT EXISTS `shdic_sc2015_hdop_buff` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `uid` int(11) unsigned NOT NULL COMMENT 'user_id',

    `title` varchar(50) NOT NULL COMMENT '提示',    
    `is_his` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=his',
    `category` int(11) unsigned NOT NULL COMMENT '分类',
    `op` tinyint(4) unsigned NOT NULL COMMENT '操作',
    `num` tinyint(4) unsigned NOT NULL COMMENT '变动值',

    `create_date` char(20) NOT NULL,
    `modify_date` char(20) NOT NULL,
    PRIMARY KEY (`id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE  `shdic_sc2015_hdop_buff` ADD  `int_ud1` int( 11 ) unsigned NOT NULL DEFAULT '0' AFTER  `num` ;
ALTER TABLE  `shdic_sc2015_hdop_buff` ADD  `int_ud2` int( 11 ) unsigned NOT NULL DEFAULT '0' AFTER  `num` ;


-- 细胞自动机（cellular automata）
-- drop table shdic_sc2015_map_block_info;

CREATE TABLE IF NOT EXISTS `shdic_sc2015_map_block_info` (
        `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `title` varchar(50) NOT NULL,
        `info_desc` varchar(250) NOT NULL,
        `code` char(32) NOT NULL,
        `is_his` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=his',
        `category` int(11) unsigned NOT NULL COMMENT '地图大类',
        `thumbnail_id` int(11) unsigned NOT NULL COMMENT '缩略图',
        `path_pos` int(11) unsigned NOT NULL COMMENT '通路编号',
        
        `enter_item_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '进入限制物品id',
        `loot_set_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '物品掉落设定id',

        `door_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '入口数量',
        `door_pos` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '入口位置编号',

        `has_child` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '是否有子地形',

        `bg_img` varchar(250) NOT NULL COMMENT '背景地址',
        `ca_x` int(11) unsigned NOT NULL COMMENT '细胞自动机x',
        `ca_y` int(11) unsigned NOT NULL COMMENT '细胞自动机y',
        `ca_z` int(11) unsigned NOT NULL COMMENT '细胞自动机z',

        `create_date` char(20) NOT NULL,
        `modify_date` char(20) NOT NULL,
        `parent_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '父地形id',
        `sort` tinyint(4) unsigned NOT NULL DEFAULT '99',
        PRIMARY KEY (`id`),
        UNIQUE KEY `code` (`code`)
      ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


ALTER TABLE  `shdic_sc2015_map_block_info` ADD  `explore_time` TINYINT( 4 ) UNSIGNED NOT NULL DEFAULT  '30' COMMENT  '预计探索时间' AFTER `door_pos` ;

CREATE TABLE IF NOT EXISTS `shdic_sc2015_map_info` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(50) NOT NULL,
  `code` char(32) NOT NULL,
  `is_his` tinyint(1) NOT NULL default '0' COMMENT '1=his',
  `category` int(11) unsigned NOT NULL,
  `thumbnail_id` int(11) unsigned NOT NULL COMMENT '缩略图',
  `block_id` int(11) unsigned NOT NULL default '0',
  `path_id` int(11) unsigned NOT NULL COMMENT '通路',
  `door_id` int(11) unsigned NOT NULL COMMENT '大门入口',
  `bg_img` varchar(250) NOT NULL COMMENT '背景地址',
  `pos_x` int(11) unsigned NOT NULL,
  `pos_y` int(11) unsigned NOT NULL,
  `ud1` varchar(32) NOT NULL,
  `ud2` varchar(32) NOT NULL,
  `ud3` varchar(32) NOT NULL,
  `ud4` varchar(32) NOT NULL,
  `ud5` varchar(32) NOT NULL,
  `ud6` varchar(32) NOT NULL,
  `ud7` varchar(32) NOT NULL,
  `ud8` varchar(32) NOT NULL,
  `ud9` varchar(32) NOT NULL,
  `create_date` char(20) NOT NULL,
  `modify_date` char(20) NOT NULL,
  `parent_id` int(11) unsigned NOT NULL default '0',
  `sort` tinyint(4) unsigned NOT NULL default '99',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2402 ;



CREATE TABLE IF NOT EXISTS `shdic_sc2015_debug_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cf1` char(20) NOT NULL COMMENT '创建时间',
  `app` varchar(50) NOT NULL,
  `log` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
