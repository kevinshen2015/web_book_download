<?php

if( !defined('AROOT') ) die('NO AROOT!');
if( !defined('DS') ) define( 'DS' , DIRECTORY_SEPARATOR );

// define constant
if( !defined('IN') ) define( 'IN' , true );

define( 'ROOT' , dirname( __FILE__ ) . DS );
define( 'CROOT' , ROOT . 'core' . DS  );


define( 'TBS_ROOT' , dirname( __FILE__ ) . DS );

// define 
error_reporting(E_ALL^E_NOTICE);
ini_set( 'display_errors' , true );

include_once( CROOT . 'lib' . DS . 'core.function.php' );
@include_once( AROOT . 'lib' . DS . 'app.function.php' );

include_once( CROOT . 'config' .  DS . 'core.config.php' );

@include_once( AROOT . 'config' . DS . 'app.config.sample.php' );
@include_once( AROOT . 'config' . DS . 'app.config.php' );

$pagestartime=microtime(); 
$time_log='';
$out=array();	//for tbs

$c = $GLOBALS['c'] = v('c') ? v('c') : c('default_controller');
$a = $GLOBALS['a'] = v('a') ? v('a') : c('default_action');

$c = basename(strtolower( z($c) ));
$a =  basename(strtolower( z($a) ));

$post_fix = '.class.php';

$cont_file = AROOT . 'controller'  . DS . $c . $post_fix;
$class_name = $c .'Controller' ; 
// echo 'cont_file='.$cont_file.PHP_EOL;
// echo 'class_name='.$class_name.PHP_EOL;

if( !file_exists( $cont_file ) )
{
	$cont_file = CROOT . 'controller' . DS . $c . $post_fix;
	if( !file_exists( $cont_file ) ) die('Can\'t find controller file - ' . $c . $post_fix );
} 

require_once( $cont_file );
if( !class_exists( $class_name ) ) die('Can\'t find class - '   .  $class_name );


$magic_controllers = array( 'api' , 'plugin' );
$o = new $class_name;
if( !method_exists( $o , $a ) && !in_array( $c , $magic_controllers ) ) die('Can\'t find method - '   . $a . ' ');

if( strlen(c('timezone')) > 1 && function_exists('date_default_timezone_set') )
@date_default_timezone_set( c('timezone') );

if(strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') !== FALSE && @ini_get("zlib.output_compression")) ob_start("ob_gzhandler");
call_user_func( array( $o , $a ) );

