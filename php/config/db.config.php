<?php
// 请不要直接修改本文件，改名为db.config.php后再修改，否则升级时将被覆盖。
$runtime_debug=false;
if(v('runtime_debug')){
	$runtime_debug=true;
}
if($runtime_debug) {
	echo PHP_EOL;
	foreach ($_SERVER as $key => $value) {
		echo $key.'='.$value.PHP_EOL;
	}
}

if(isset($GLOBALS['config']['runtime_env_check'])){
	$runtime_env_check=$GLOBALS['config']['runtime_env_check'];	
	if($runtime_debug) echo 'runtime_env_check =' . $runtime_env_check .' !'. PHP_EOL;
}else{
	if($runtime_debug) echo 'NOT Set runtime_env_check !'. PHP_EOL;
	
	$runtime_env_check='';

	if(defined('SERVER_SOFTWARE')){
		//SERVER_SOFTWARE = bae/3.0
		$runtime_env_check=strtolower(SERVER_SOFTWARE);
		if(stripos($runtime_env_check,'bae')!==false){
			$runtime_env_check='bae';
			if($runtime_debug){
				echo 'SERVER_SOFTWARE =' . SERVER_SOFTWARE .' !'. PHP_EOL;
			}
		}

	}elseif (defined('SAE_APPNAME') && defined('SAE_MYSQL_HOST_M') ) {
		$runtime_env_check='sae';
		
	}else{
		//local php env
		$tmp = 'k'.strtolower($_SERVER['HTTP_HOST']); 
		if(strpos($tmp, 'localhost')!=false || strpos($tmp, '192.168.')!=false){
			$runtime_env_check='local';
		}else{
			$runtime_env_check='dev';
		}
	}

}

/**

*/

if( !empty($runtime_env_check) && $runtime_env_check=='sae' ){	
	$GLOBALS['config']['db']['db_host'] = SAE_MYSQL_HOST_M;
	$GLOBALS['config']['db']['db_host_read'] = SAE_MYSQL_HOST_S;
	
	$GLOBALS['config']['db']['db_port'] = SAE_MYSQL_PORT;
	
	$GLOBALS['config']['db']['db_user'] = SAE_MYSQL_USER;
	$GLOBALS['config']['db']['db_password'] = SAE_MYSQL_PASS;
	$GLOBALS['config']['db']['db_name'] = SAE_MYSQL_DB;
	
}else if(!empty($runtime_env_check) && $runtime_env_check=='bae'){
	//BAE 3.0 duapp.com
	/*替换为你自己的数据库名*/

	define( 'APP_ROOT' ,  dirname( dirname( dirname( dirname( __FILE__ )  ) )  ) . DS );
	define( 'BLAB_ROOT' , APP_ROOT.'blab'. DS  );
	define( 'LP3K_ROOT' , APP_ROOT.'lp3k'.DS  );

	global $app_mysql_host,$app_mysql_port,$app_mysql_user,$app_mysql_pwd,$app_mysql_dbname;

	// define( 'User_AK' , '11f171588e204b0a95b65e0c15dd86b0' );
	// define( 'User_SK' , 'df8e89f42b044b48be58304fe3eb2857' );

	/*填入数据库连接信息*/
	$app_mysql_host = 'sqld.duapp.com';
	$app_mysql_port = 4050;
	$app_mysql_dbname = 'lBKMjAIqhOhuQeFZHhYF';		//1G
	$app_mysql_user = 'Wlb75KzSw6G2F7NCknMtEw0G';//用户名(api key)
	$app_mysql_pwd = 'bwwZvkzgTsBL8l6qOhU4NCYhiY75oLWF';//密码(secret key)
	
	/*以上信息都可以在数据库详情页查找到*/

	$GLOBALS['config']['db']['db_host'] = $app_mysql_host;
	$GLOBALS['config']['db']['db_host_read'] = $app_mysql_host;

	$GLOBALS['config']['db']['db_port'] = $app_mysql_port;

	$GLOBALS['config']['db']['db_user'] = $app_mysql_user;
	$GLOBALS['config']['db']['db_password'] = $app_mysql_pwd;
	$GLOBALS['config']['db']['db_name'] = $app_mysql_dbname;
	

}else if($runtime_env_check=='dev'){

	$GLOBALS['config']['db']['db_host'] = 'localhost';
	$GLOBALS['config']['db']['db_port'] = 3306;
	$GLOBALS['config']['db']['db_user'] = 'root';
	$GLOBALS['config']['db']['db_password'] = 'usbw';
	$GLOBALS['config']['db']['db_name'] = 'webdb';

}else{	//local dev

	$GLOBALS['config']['db']['db_host'] = 'localhost';
	$GLOBALS['config']['db']['db_port'] = 6606;
	$GLOBALS['config']['db']['db_user'] = 'root';
	$GLOBALS['config']['db']['db_password'] = 'usbw';
	$GLOBALS['config']['db']['db_name'] = 'lpdb_jcyq';
}

/**

*/

if($runtime_debug){
	echo 'db.config.runtime_env_check='.$runtime_env_check.PHP_EOL;
	echo 'db_host='. $GLOBALS['config']['db']['db_host'] .PHP_EOL;
	echo 'db_name='. $GLOBALS['config']['db']['db_name'] .PHP_EOL;
}

