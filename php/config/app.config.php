<?php
$runtime_env_check='';
$runtime_debug=false;
if(v('runtime_debug')){
	$runtime_debug=true;
}

if(isset($GLOBALS['config']['runtime_env_check'])){
	$runtime_env_check=$GLOBALS['config']['runtime_env_check'];	
	if($runtime_debug) echo 'runtime_env_check =' . $runtime_env_check .' !'. PHP_EOL;
}else{
	if($runtime_debug) echo 'NOT Set runtime_env_check !'. PHP_EOL;
	
	$runtime_env_check='';

	if(defined('SERVER_SOFTWARE')){
		//SERVER_SOFTWARE = bae/3.0
		$runtime_env_check=strtolower(SERVER_SOFTWARE);
		if(stripos($runtime_env_check,'bae')!==false){
			$runtime_env_check='bae';
			if($runtime_debug){
				echo 'SERVER_SOFTWARE =' . SERVER_SOFTWARE .' !'. PHP_EOL;
			}
		}

	}elseif (defined('SAE_APPNAME') && defined('SAE_MYSQL_HOST_M') ) {
		$runtime_env_check='sae';
		
	}else{
		//local php env
		$tmp = 'k'.strtolower($_SERVER['HTTP_HOST']); 
		if(strpos($tmp, 'localhost')!=false || strpos($tmp, '192.168.')!=false){
			$runtime_env_check='local';
		}else{
			$runtime_env_check='dev';
		}
	}

}


if($runtime_env_check == 'bae' ){
	//SERVER_SOFTWARE = bae/3.0
	$GLOBALS['config']['site_root_path'] = '';

	define('APP_AK', 'db2e3c65aa0f47bbab9bea0764dadaf7');
	define('APP_SK', '9c7bcac8432f48939c43b28d4e61844d');

	define( 'User_AK' , 'db2e3c65aa0f47bbab9bea0764dadaf7' );
	define( 'User_SK' , '9c7bcac8432f48939c43b28d4e61844d' );


}elseif ($runtime_env_check=='sae') {
	$GLOBALS['config']['site_root_path'] = '';
	
}else{
	//local php env
	$url = $_SERVER['PHP_SELF']; 
	$tt = str_ireplace('/index.php','',$url);
	$GLOBALS['config']['site_root_path'] = $tt;
}

if($runtime_debug) echo 'app.config.runtime_env_check =' . $runtime_env_check .' !'. PHP_EOL;

// 请不要直接修改本文件，改名为app.config.php后再修改，否则升级时将被覆盖。
$GLOBALS['config']['site_name'] = '生存营 Survival Camp';
$GLOBALS['config']['site_domain'] = $_SERVER['HTTP_HOST'];
$GLOBALS['config']['site_url'] = 'http://'.$GLOBALS['config']['site_domain'] . $GLOBALS['config']['site_root_path'];

$GLOBALS['config']['default_controller'] = 'guest';
$GLOBALS['config']['default_action'] = 'index';

$GLOBALS['config']['favicon'] = 'static/image/favicon.png';
$GLOBALS['config']['default_avatar'] = 'static/image/user.avatar.png';
$GLOBALS['config']['api_server'] = $GLOBALS['config']['site_url'] . '/index.php';
$GLOBALS['config']['api_check_new_verison'] = false;
// $GLOBALS['config']['teamtoy_url'] = 'http://tt2net.sinaapp.com';
$GLOBALS['config']['at_short_name'] = true ;
$GLOBALS['config']['can_modify_password'] = true ;
$GLOBALS['config']['timezone'] = 'Asia/Chongqing' ;
$GLOBALS['config']['dev_version'] = false ;
$GLOBALS['config']['default_language'] = 'zh_cn' ;

// session time
// you need change session lifetime in php.ini to0
$GLOBALS['config']['session_time'] = 60*60*24*3 ;

$GLOBALS['config']['plugin_path'] = AROOT . DS . 'plugin' . DS ;
$GLOBALS['config']['plugins'] = array( 'css_modifier' , 'simple_token');


$GLOBALS['config']['runtime_env_check'] =$runtime_env_check;

$GLOBALS['config']['db_prefix'] ='root';

