<?php
/**

        FOR  API 

**/


function init_page_start_limit(){
  global $limit,$offset,$page; 
  
  $limit = intval(v('start'))  ;

  $tt = intval(v('limit'))  ;
  if ($tt>0 && $tt<=500){
      $offset=$tt;
  }else{
    $offset = 15;
  }

  $page=0;
  $tt = intval(v('page'))  ;
  if ($tt>0 && $tt<=100){
      $page=$tt;
      $outVar['curr_page']=$tt;
  }else{
    $outVar['curr_page']=0;
  }
}

function fix_page_limit(){
  global $debugMode, $time_log , $c,$a, $outVar;
  global $db_table_prefix;
  global $limit,$offset,$page;  
  // echo '<!-- DEBUG: '.__METHOD__.'() page:'.$page.' -->'.PHP_EOL;
  // echo '<!-- DEBUG: '.__METHOD__.'() limit='.$limit.' -->'.PHP_EOL;

  if(intval($offset)<1) $offset = 15;
  if(intval($limit)<1) $limit = 0;

  $page=intval($page);
  if($page==0 && $limit==0 ){
    $pageNo=1;
  }else if($page<1 && $limit>0 ){
    $pageNo=round($limit/$offset+1);
  }else if($page>0){
    $pageNo=$page;
    $limit=($page-1)*$offset;
  }else{
    $pageNo=0;
  }
  // echo '<!-- DEBUG: '.__METHOD__.'() page:'.$page.' set to '.$pageNo.' -->'.PHP_EOL;
  // echo '<!-- DEBUG: '.__METHOD__.'() limit set to '.$limit.' -->'.PHP_EOL;
  $page=$pageNo;
  return true;
}

function get_pageview($ret_page,$target='html_fanye1',$js_func='gotopage'){
  global $debugMode, $time_log , $c,$a, $outVar;
  global $db_table_prefix;
  global $limit,$offset,$page;

  if(!isset($ret_page['count']) || !isset($ret_page['data'])){
    return false;
  }
  
  include_once 'pageview.inc.php';

  fix_page_limit();

  //创建分页器 
  $p = new PageView($ret_page['count'],15,$page,$ret_page['data'],$js_func); 
  //生成页码 
  $pageViewString = $p->echoPageAsDiv();  

  // $ret2_info = $ret_page['data'];
  // $outVar['html_fanye1']=$ret_page['count'].'#'.$pageViewString;
  if($target==null || empty($target)){
    return $pageViewString;
  }
  $outVar[$target]=$pageViewString;
  return true;
}



function check_user_access($uid=null){
    global $c,$a, $outVar;

    $is_vip=is_vip();

    if($is_vip){
      echo '<!-- welcome VIP -->'.PHP_EOL;  
      $outVar['welcome_4_vip']='<div align="center" style="text-align: center;background-color: #cde4b0;font-size: 16px;">你好！VIP！</div>';
      return 'is_vip';
    }else{
        $outVar['welcome_4_vip']='<div align="center" style="text-align: center;background-color: #cde4b0;font-size: 16px;"><a href="index.php?c=guest&a=vip">升级到本站VIP</a></div>';
    }

    if($uid===null){
      $uid=uid();
    }
    $uidip=intval($uid);
    if($uidip==0){
      $uidip=getIp();
    }
    $kv_key='user_access_count_'.$uidip;
    $flag=false;
    $user_access_count=0;
    if($tt=get_redis($kv_key)){
      $tt=intval($tt)+1;
      $user_access_count=$tt;
      if($tt==1){
        set_redis($kv_key.'_last_renew_time',date('Y-m-d H:i:s'));
      }
      $flag=set_redis($kv_key,$tt);
    }
    if(!$flag){
     $tt=kget($kv_key);
     $tt=intval($tt)+1;
     $user_access_count=$tt;
     if($tt==1){
        kset($kv_key.'_last_renew_time',date('Y-m-d H:i:s'));
     }
     kset($kv_key,$tt);  
    }

    echo '<!-- user_access_count='.$user_access_count.',kv_key='.$kv_key.' -->'.PHP_EOL; 
    if(isset($_SESSION['level'])){
      echo '<!-- _SESSION[level]='.$_SESSION['level'].' -->'.PHP_EOL;
    }

    if($user_access_count>50){
      return false;
    }else if($user_access_count>20){
      $outVar['welcome_vip']=get_welcome_vip($uidip);
    }
    return $user_access_count;
}

function get_welcome_vip($uidip){
  global $c,$a, $outVar;
  return render( $outVar , 'part' , 'welcome_vip');
}


function simple_json($msg,$code=0){
  return json_encode(array('ret_msg'=>$msg,'ret_code'=>$code));
}


/**

        sub function

**/

define( 'LOGIN_SPAN_HTML' , '<a href="index.php?c=guest&a=denglu">登录</a> | <a href="index.php?c=guest&a=zhuce">注册</a>' );

define( 'LOGIN_SPAN_HTML_STATIC' , '<a href="page_denglu.html">登录</a> | <a href="page_zhuce.html">注册</a>' );

function get_login_span_html(){
  $src='';
  if(isset($_SESSION[ 'uname' ]) && !empty($_SESSION[ 'uname' ])) {
        $src.= '*'.substr($_SESSION[ 'uname' ],-4);;
        $src.= ' | <a href="index.php?c=guest&a=zhuxiao">注销</a>';
    }else{
        $src.=LOGIN_SPAN_HTML;
    }  
    return $src;
}

function str_remove_space($str){
  return trimall($str);
}

//删除空格和回车
function trimall($str){
    $qian=array(" ","　","\t","\n","\r");
    return str_replace($qian, '', $str);   
}


function _arr_val($arr,$key){
  if(isset($arr[$key])){
    return $arr[$key];
  }
  return 'null';
}


/**


**/

function _dev_debug($method,$src){
  // echo '<!-- DEBUG: '.__METHOD__.'() ['.$book_title.'] ! -->'.PHP_EOL;
  // _dev_debug(__METHOD__,'['.$book_title.'] ! ');
  echo '<!-- DEBUG: '.$method.'() '.$src.' -->'.PHP_EOL;
}

function debug_span($src){
  echo '<!-- '.$src.' -->'.PHP_EOL;
}

function get_cache_FN($tmpUrl){
  $cacheFN=get_mini_cache_FN($tmpUrl);
  $cacheFN.='.html';
  $cacheFN=CACHE_ROOT.$cacheFN;
  return $cacheFN;
}
function get_mini_cache_FN($tmpUrl){
  $cacheFN=md5($tmpUrl);
  return $cacheFN;
}

function get_cache_link($tmpUrl){
  $cacheFN=get_mini_cache_FN($tmpUrl);
  $cacheFN.='.html';
  $cacheFN=CACHE_DIR.$cacheFN;
  return $cacheFN;
}

function retry_load_page1($pageUrl,$cacheFN,$count,$skip_sleep=null){
  if(intval($count)<1){
    return false;
  }

  $listSrc=_get_page($pageUrl);
  if(!empty($listSrc)){
    file_put_contents($cacheFN,$listSrc);
  }else{
    if($skip_sleep===null){
      sleep(10+$count);
    }else{
      $skip_sleep=intval($skip_sleep);
      if($skip_sleep>0){
        sleep($skip_sleep);
      }
    }
    // die(__METHOD__.'() Error #'.$pageUrl);
    $count --;
    $listSrc=retry_load_page($pageUrl,$cacheFN,$count);
  }
  return $listSrc;
}

function retry_load_page($pageUrl,$cacheFN,$count,$skip_sleep=null){
  global $snoopy;
  $tmp=retry_load_page1($pageUrl,$cacheFN,$count,$skip_sleep);
  if(!empty($tmp)){
    return $tmp;
  }
  if($snoopy===null) {
    include_once KIS_APP_ROOT.'lib'. DS.'Snoopy.class.php';
    $snoopy = new Snoopy();
  }
  // $snoopy->proxy_host = '127.0.0.1';
  // $snoopy->proxy_port = '8087';
  $snoopy->maxredirs = 2; //重定向次数
  // $snoopy->expandlinks = true; //是否补全链接 在采集的时候经常用到  // 例如链接为 /images/taoav.gif 可改为它的全链接 http://www.4wei.cn/images/taoav.gif
  $snoopy->maxframes = 5; //允许的最大框架数

  $snoopy->fetch($pageUrl);
  $listSrc=$snoopy->results;
  // $listSrc=_get_page($pageUrl);  
  if(!empty($listSrc)){
    file_put_contents($cacheFN,$listSrc);
  }
  return $listSrc;
}

function load_cache_page_by_url_die($pageUrl,$minSize=3000,$die=1){
  $cacheFN = get_cache_FN($pageUrl);
  if(file_exists($cacheFN)){
    $listSrc=file_get_contents($cacheFN);
    if(strlen($listSrc)>$minSize){
      return $listSrc;
    }
  }
  $listSrc=retry_load_page($pageUrl,$cacheFN,3);
  if(!empty($listSrc) && strlen($listSrc)>$minSize){
    file_put_contents($cacheFN,$listSrc);
    return $listSrc;
  }

  if($die){
    die(__METHOD__.'() Error #'.$pageUrl);  
  }
  return false;
}

function load_cache_page_by_url($pageUrl,$minSize=3000){
  return load_cache_page_by_url_die($pageUrl,$minSize,1);
}

function load_cache_page_by_url_test($pageUrl,$minSize=3000){
  return load_cache_page_by_url_die($pageUrl,$minSize,0);
}

function load_cache_part_page($pageUrl,$partKey,$minSize=5){
  $cacheFN = get_cache_FN($pageUrl.'$part$'.$partKey);
  if(file_exists($cacheFN)){
    $listSrc=file_get_contents($cacheFN);
    if(strlen($listSrc)>$minSize){
      return $listSrc;
    }
  }
  return false;
}

function save_cache_part_page($pageUrl,$partKey,$listSrc){
  $cacheFN = get_cache_FN($pageUrl.'$part$'.$partKey);
  file_put_contents($cacheFN,$listSrc);
}


/**
    
      for WEB_BOOK_DOWNLOAD

**/

function check_site_already_configured($siteArr){     //检查传入站点中，是否有已知站点
  $baseSiteArr=array();
  foreach ($siteArr as $tttUrl) {
    if(!empty($tttUrl) && is_exclude_site($tttUrl)===false){
      $baseSiteArr[]=$tttUrl;
    }
  }

  if(empty($baseSiteArr)){
    return false;
  }

  // $bestSiteArr=array();
  $sitesignArr=get_all_sitesign();
  if($sitesignArr){
    foreach ($sitesignArr as $siteValue) {
      $siteUrl=$siteValue['url'];
      $siteUrl= str_ireplace('http://', '', $siteUrl);
      foreach ($baseSiteArr as $key => $value) {
        if(stripos($value, $siteUrl)!==false){
          $siteValue['url']=$siteUrl;
          $siteValue['task_url']=$value;
          // $bestSiteArr[]=$siteValue;
          return $siteValue;
        }
      }
    }
  }

  return $baseSiteArr;
}

function get_all_sitesign(){
  global $sql_table_name;
  $retArr=array();
  $tmpSql2 = prepare('SELECT * FROM `'.$sql_table_name.'_sitesign` WHERE `is_his`!=?i order by `cf1` desc limit 500' ,array(1));
  if($retSql2 = get_data($tmpSql2) ){
    return $retSql2;
  }
  return false;
}

function get_config_or_die($configArr,$key){
  if(isset($configArr[$key])){
    return $configArr[$key];
  }

  echo debug_span(print_r($configArr,true));
  die(__METHOD__.'() Miss Key :'.$key);  
}

function get_book_by_sitesign($sitesign,$book_title){
  //不是需要排除的站点,是已知站点
  $retArr=array('ret_code'=>0,'ret_msg'=>'');

  _dev_debug(__METHOD__,'['.$book_title.'] !');
  
  if(!is_array($sitesign) || !isset($sitesign['task_url'])){
    $retArr['ret_code']=100;
    $retArr['ret_msg']='base check error !';
    return $retArr;
  }
  $task_url=$sitesign['task_url'];
  if(empty($task_url)){
    $retArr['ret_code']=101;
    $retArr['ret_msg']='base check error ! #2';
    return $retArr;
  }
  
  $listHtml= load_cache_page_by_url_test($task_url);
  if(empty($listHtml)) {
    $retArr['ret_code']=102;
    $retArr['ret_msg']='load list html error !';
    return $retArr;
  }
  _dev_debug(__METHOD__,'load_cache_page_by_url_test ok!');

  $ListStart=get_config_or_die($sitesign,'ListStart');
  $ListEnd=get_config_or_die($sitesign,'ListEnd');

  $tmpHtmlSrc=_cut_middle_str($listHtml,$ListStart,$ListEnd);
  if(empty($tmpHtmlSrc)) {
    $retArr['ret_code']=103;
    $retArr['ret_msg']='load list html error ! #2 is empty!';
    return $retArr;
  }

  $VolumeStart=get_config_or_die($sitesign,'VolumeStart');
  $VolumeEnd=get_config_or_die($sitesign,'VolumeEnd');

  // $tmpVolumeArr=explode($VolumeStart, $tmpHtmlSrc);
  // $tmpArr2=array();
  // foreach ($tmpVolumeArr as $keyVolume => $valueVolume) {
  //   if($keyVolume>0){
  //     $tmpArr3=explode($VolumeEnd, $valueVolume);
  //     $VolumeTitle = $tmpArr3[0];
  //     echo debug_span('Found Volume Title #'.$keyVolume . ' = '.$VolumeTitle);
  //     $tmpArr2[]=$VolumeTitle;
  //   }
  // } 
  // $tmpHtmlSrc=implode($tmpVolumeArr, PHP_EOL.'<hr color=red>'.PHP_EOL);

// <a style="" href="/id34100/1817596.html">第六十一章 一吻定情(文)</a>
// /id34100/1817596.html
  $hrefLinkArr=func_get_all_href_link($tmpHtmlSrc);
  _dev_debug(__METHOD__,'func_get_all_href_link() ok!');
  $siteUrl=get_config_or_die($sitesign,'url');
  $idx=0;
  $tmpJobsUrlArr=array();
  foreach ($hrefLinkArr as $key => $value) {
    if(!empty($value)){
      $job_url = 'http://' . $siteUrl . $value;
      $tmpJobsUrlArr[]=$job_url;
      $idx++;
      add_download_jobs($job_url,2,$book_title.' 第'.$idx.'章');  
    }
  }

  _dev_debug(__METHOD__,'add_download_jobs() ok!');

  // $VolumeStart=get_config_or_die($sitesign,'VolumeStart');
  // $VolumeEnd=get_config_or_die($sitesign,'VolumeEnd');

  // $tmpVolumeArr=explode($VolumeStart, $tmpHtmlSrc);
  // $tmpArr2=array();
  // foreach ($tmpVolumeArr as $keyVolume => $valueVolume) {
  //   // $tmpArr2[]=
  // } 
  $tmpHtmlSrc=implode($tmpJobsUrlArr, PHP_EOL.'<hr color=red>'.PHP_EOL);

  return $tmpHtmlSrc;

}

function get_book_by_search_web($sitesign,$taskinfo=null,$task_id=null){  
  // 既不是需要排除的站点,也不是已知站点
  $ret_html='';
  $this_debug =true;
  if($this_debug){
    _dev_debug(__METHOD__,'既不是需要排除的站点,也不是已知站点! ');
  }

  if(!is_array($sitesign)){
    _dev_debug(__METHOD__,' 传入参数非数组! ');
    return false;
  }else{
    _dev_debug(__METHOD__,' 传入参数数组:'. print_r($sitesign,true) );
  }
  
  foreach ($sitesign as $key => $bookUrl) {
    if(!empty($bookUrl)){
      // $tmpListHtml= load_cache_page_by_url_test($bookUrl);
      $ret_html .= show_html_all_link($bookUrl,$taskinfo,$task_id);
      $ret_html .= '<hr color="red">'.PHP_EOL;      
    }
  }
  return $ret_html;
}

function show_html_all_link($bookUrl,$taskinfo=null,$task_id=null){
  $this_debug =true;  
  if(empty($bookUrl)){
    if($this_debug){
      _dev_debug(__METHOD__,' HTML URL empty! ');
      return false;
    }
  }

  $retHtml='';

  $tmpListHtml= load_cache_page_by_url_test($bookUrl);
  $linkArr=array();
  $dropLinkArr=array();
  $key1='<a';
  $key2='</a>';
  $tmpArr=explode($key1, $tmpListHtml);
  $line_idx=0;
  $max_line_idx=0;
  foreach ($tmpArr as $key => $value) {
    if(stripos($value,$key2)!=false){
      $tmpArr2=explode($key2, $value);
      $tmpHtml=$key1.$tmpArr2[0].$key2;
      $tmpHtml=iconv("GB2312", "UTF-8//IGNORE", $tmpHtml);
      if(stripos($tmpHtml,'章')!=false && stripos($tmpHtml,'http:')==false){
        $linkArr[]=$tmpHtml;
        // $retHtml .=$tmpHtml.PHP_EOL;
        $retHtml .='<div class="product_list"> '.$tmpHtml.'</div>'.PHP_EOL;
        $line_idx++;
        if($max_line_idx>0 && $line_idx>$max_line_idx){
          $retHtml .='<br/>'.PHP_EOL;
          $line_idx=0;
        }
      }else{
        $dropLinkArr[]=$tmpHtml;
      }      
    }
  }
  
  // $retHtml = print_r($linkArr,true) . PHP_EOL ;
  // $retHtml = implode('<br/>'.PHP_EOL, $linkArr);
  // $retHtml .= print_r($dropLinkArr,true) . PHP_EOL ;
  // $retHtml .= print_r($listSrc,true) . PHP_EOL ;


  // $listSrc=snoopy_fetchlinks($bookUrl);
  // // 
  // foreach ($listSrc as $key => $value) {
  //   $retHtml .= '<a href="'.$value.'">'.$value.'</a><br/>'.PHP_EOL;
  // }
  
  $tmpHtml = '<a href="?c=guest&a=downThisBook&id='.intval($task_id).'&bookUrl='.$bookUrl.'">down this book</a>'.PHP_EOL;
  $retHtml .='<div class="product_list"> '.$tmpHtml.'</div>'.PHP_EOL;
  return $retHtml;
}

function snoopy_fetchlinks($bookUrl){
  global $snoopy;
  if($snoopy===null) {
    include_once KIS_APP_ROOT.'lib'. DS.'Snoopy.class.php';
    $snoopy = new Snoopy();
  }
  $snoopy->fetchlinks($bookUrl);
  $listSrc=$snoopy->results;
  // return print_r($listSrc,true);
  return $listSrc;
}


function pc_auto_work(){
  global $c,$a, $outVar;

  $html_src='';
  $debug_src='';
  echo '<!-- DEBUG: '.__METHOD__.'() -->'.PHP_EOL;
  
  $processCount=0;
  // SELECT `uuid`, `is_done`, `job_mode`, `title`, `cf1`, `cf2`, `job_url` FROM `shdic_wbd2016_download_jobs` WHERE 1
  $tmpSql = prepare('SELECT * FROM `shdic_wbd2016_download_jobs` WHERE `is_done`=?i order by cf1 desc limit 10' ,array(0));
  $html_src .=$tmpSql.PHP_EOL;
  if($ret_sql=get_data($tmpSql)){

    include_once( 'php'. DS.'crxapi2016.inc.php');  
    foreach ($ret_sql as $key => $value) {
      $task_url = $value['job_url'];
      $html_src .=$task_url.PHP_EOL;

      $tmpPageHtml= load_cache_page_by_url_test($task_url);
      if(!empty($tmpPageHtml)) {
        $ret_obj=api2016putJobsHtml($task_url,$tmpPageHtml);
        
        // UPDATE `shdic_wbd2016_download_jobs` SET `uuid`=[value-1],`is_done`=[value-2],`job_mode`=[value-3],`title`=[value-4],`cf1`=[value-5],`cf2`=[value-6],`job_url`=[value-7] WHERE 1
        $tmpSql2 = prepare('UPDATE `shdic_wbd2016_download_jobs` SET `is_done`=?i where  `uuid`=?s ',array(1,md5(strtolower($task_url))));
        run_sql($tmpSql2);
        $html_src .=$tmpSql2.PHP_EOL;
        sleep(1);
      }else{
        $tmpSql2 = prepare('UPDATE `shdic_wbd2016_download_jobs` SET `is_done`=?i where  `uuid`=?s ',array(2,md5(strtolower($task_url))));
        run_sql($tmpSql2);
        $html_src .=$tmpSql2.PHP_EOL;
        sleep(1);
      }
      $processCount++;
    }
    
  }
  if(!isset($outVar['data'])){
    $outVar['data']='';
  }
  $outVar['data'].=$html_src;
  if($processCount>0){
    $cf_now = date('Y-m-d H:i:s');
    $outVar['js_str']='setTimeout("location.reload()", 16000); console.log("'.$cf_now.'")';
  }
  html_render( $outVar , array('ui_tpl' => 'js' ,'debug_src'=>$debug_src));

  die(__METHOD__.'() Process End!');
}


function do_a_task($taskinfo,$task_id=0){
  global $c,$a, $outVar;
  // https://www.baidu.com/s?wd=%E6%9C%80%E5%BC%BA%E4%BC%A0%E6%89%BF%20%E5%85%A8%E9%9B%86txt%E4%B8%8B%E8%BD%BD&rsv_spt=1&rsv_iqid=0xbcbc46a400169292&issp=1&f=3&rsv_bp=1&rsv_idx=2&ie=utf-8&rqlang=cn&tn=baiduhome_pg&rsv_enter=1&oq=%E7%88%B1%E7%9A%84%E8%BF%BD%E8%B8%AA&rsv_t=7cdbBMHTC2m3SCSLJauFW9FXF8tI7Kg2nfng8uVnIYnShNg9iqHZ0%2FzU71S%2Bzonew3Ra&rsv_pq=e4be217c001520ff&rsv_sug3=4&rsv_sug1=3&rsv_sug7=100&rsv_sug2=1&prefixsug=%E6%9C%80%E5%BC%BA%E4%BC%A0%E6%89%BF&rsp=0&inputT=15703&rsv_sug4=15709

  $this_debug=true;

  if($this_debug){echo '<!-- DEBUG: '.__METHOD__.'() start! -->'.PHP_EOL;} 

  $debug_src='';
  $jobstateArr=array();
  $processPartId=1;
  $jobProcessIdx=0;

  $book_title = $taskinfo['book_title'];
  if(empty($book_title)){
    $jobstate=red_span('Miss book_title!!! ');
    $outVar['html_data']=$debug_src.PHP_EOL.PHP_EOL.PHP_EOL.$jobstate;
    return false;
  }

  $tmpUrl='http://www.baidu.com/s?wd='.$book_title.'+最新';

  $tmpSrc=load_cache_page_by_url($tmpUrl);

  // bds.comm.iaurl=["http:\/\/www.bxwx8.org\/binfo\/122\/122611.htm","http:\/\/www.80txt.com\/txtxz\/51421.html","http:\/\/www.ibook8.com\/xuanhuan\/xh\/22091.html"];
  $tmpJson=_cut_middle_str($tmpSrc,'bds.comm.iaurl=',';');

  $jobstateArr[]=$tmpJson;

  $NotUtf8SiteArr=array();
  $Utf8SiteArr=array();

  $ttArr=json_decode($tmpJson);
  // foreach ($ttArr as $tttUrl) {
  //  if(!empty($tttUrl) && is_exclude_site($tttUrl)===false){
  //    $Utf8SiteArr[]=$tttUrl;
  //    $tmpSrc2=load_cache_page_by_url($tttUrl);
  //    if(has_charset_utf8_html($tmpSrc2)){
  //      $tmpHtml=_cut_middle_str($tmpSrc2,'<title>','</title>');
  //      $jobstateArr[]=$tmpHtml;
  //      $jobstateArr[]=$tmpSrc2;
  //    }else{
  //      $NotUtf8SiteArr[]=$tmpSrc2;
  //    }
  //  }
  // }

  // $jobstate=implode($jobstateArr, PHP_EOL.'<hr color=red>'.PHP_EOL);
  
  // if(count($jobstateArr)<2 && count($NotUtf8SiteArr)>0){
  //  $jobstate.=PHP_EOL.'<hr color=red>'.PHP_EOL;
  //  $content=implode($NotUtf8SiteArr, PHP_EOL.'<hr color=red>'.PHP_EOL);
  //  $content = mb_convert_encoding($content, "UTF-8", "GBK");
  //  $jobstate.=$content;
  // }

  $siteArr=check_site_already_configured($ttArr);
  if(is_array($siteArr)){     //不是需要排除的站点
    
    if(isset($siteArr['task_url'])){    //是已知站点
      if($this_debug){echo '<!-- DEBUG: '.__METHOD__.'() 是已知站点! -->'.PHP_EOL;} 
      $tmpShowArr=array();
      $tmpShowArr[]=$siteArr;

      $html=kis_debug_table($tmpShowArr);
      $outVar['html_data']=$html;

      $html=get_book_by_sitesign($siteArr,$book_title);
      $outVar['html_data'].=$html;

    }else{
      //既不是需要排除的站点,也不是已知站点
      if($this_debug){
        echo '<!-- DEBUG: '.__METHOD__.'() 既不是需要排除的站点,也不是已知站点! -->'.PHP_EOL;
      } 

      $html=get_book_by_search_web($siteArr,$taskinfo,$task_id);
      // $outVar['html_data2']=$html;
      $tmpArr=explode('<hr color="red">'.PHP_EOL, $html);
      $outVar['html_data']=$tmpArr[0];
      $outVar['html_data2']=$tmpArr[1];
    }

  }else{
    //全是需要排除的站点!
    if($this_debug){
      echo '<!-- DEBUG: '.__METHOD__.'() 全是需要排除的站点 -->'.PHP_EOL;
    } 
    $html='全是需要排除的站点! not ready!';
    $outVar['html_data']=$html;
  }
  
  // $outVar['jobstate']=$debug_src.PHP_EOL.PHP_EOL.PHP_EOL.$jobstate;
  // $outVar['jobProcessPartId']=$processPartId;
  // $outVar['jobProcessIdx']=$jobProcessIdx;

  return false;
}



function down_this_book($taskinfo,$task_id=0){
  global $c,$a, $outVar;

  $this_debug=true;

  if($this_debug){_dev_debug(__METHOD__,'start !');} 

  $bookUrl=v('bookUrl');
  if(empty($bookUrl)){
    _dev_debug(__METHOD__,'bookUrl EMPTY !');
    $outVar['html_data']='bookUrl EMPTY !';
    return false;
  }

  $debug_src=__METHOD__.'()';
  $jobstateArr=array();
  $processPartId=1;
  $jobProcessIdx=0;

  $listSrc=snoopy_fetchlinks($bookUrl);
  // 
  $idx=0;
  $bookBuffer='';
  $bbIdx=0;
  $bbIdxFN='tmp/book';
  $bbIndexArr=array();
  if(!is_array($listSrc)){
    $debug_src.= ';not array!';
  }else{
    foreach ($listSrc as $key => $value) {
      $idx++;
      $debug_src.= ';#'.$idx;
      // $retHtml .= '<a href="'.$value.'">'.$value.'</a><br/>'.PHP_EOL;
      $tmpPageHtml= load_cache_page_by_url_test($value);
      $tmpPageTxt=$tmpPageHtml;
      $tmpPageTxt=replacehtmltag_ext($tmpPageHtml);
      $bookBuffer .= PHP_EOL.PHP_EOL.'No.' . $idx. PHP_EOL . $tmpPageTxt;
      if(strlen($bookBuffer)>5000*1024){
        $debug_src.= ';new';
        $tmpFN=$bbIdxFN.$bbIdx.'.txt';
        file_put_contents($tmpFN, $bookBuffer);
        $bbIndexArr[]=$tmpFN;
        $bbIdx++;
        $bookBuffer='';
      }
    }
  }


  if(!empty($bookBuffer)){
    $debug_src.= ';last';
    $tmpFN=$bbIdxFN.$bbIdx.'.txt';
    file_put_contents($tmpFN, $bookBuffer);
    $bbIndexArr[]=$tmpFN;
  }

  $debug_src.= ';end.';
  $outVar['html_data']=$debug_src.PHP_EOL.implode('<br/>', $bbIndexArr);
  return false;
}



function down_this_book2($taskinfo,$task_id=0){
  global $c,$a, $outVar;

  $this_debug=true;

  if($this_debug){_dev_debug(__METHOD__,'start !');} 

  $bookUrl=v('bookUrl');
  if(empty($bookUrl)){
    _dev_debug(__METHOD__,'bookUrl EMPTY !');
    $outVar['html_data']='bookUrl EMPTY !';
    return false;
  }

  $debug_src=__METHOD__.'()';
  $jobstateArr=array();
  $processPartId=1;
  $jobProcessIdx=0;

  $listSrc=snoopy_fetchlinks($bookUrl);
  // 
  $idx=0;
  $bookBuffer='';
  $bbIdx=0;
  $bbIdxFN='book';
  $bbIndexArr=array();
  if(!is_array($listSrc)){
    $debug_src.= ';not array!';
  }else{
    foreach ($listSrc as $key => $value) {
      $idx++;
      $debug_src.= ';#'.$idx;
      // $retHtml .= '<a href="'.$value.'">'.$value.'</a><br/>'.PHP_EOL;
      $tmpPageHtml= load_cache_page_by_url_test($value);

      // $tmpPageTxt=replacehtmltag_ext($tmpPageHtml);
      $tt=1000000+$bbIdx;
      $tmpFN='C:/tmp/'.$bbIdxFN.substr(''.$tt,-6).'.html';
      file_put_contents($tmpFN, $tmpPageHtml);      
      $bbIdx++;
      echo $tmpFN;
    }
  }

  return false;
}

function replacehtmltag_ext($context) {  
  /*
    $context = str_ireplace("<p>", "\n\n", $context);  
    $context = str_ireplace("</p>", "\n\n", $context);  
    $context = str_ireplace("<BR>", "\n", $context);  
    $context = str_ireplace("</BR>", "\n", $context);  
    $context = str_ireplace(" ", '  ', $context); //>  
    $context = str_ireplace(">", '', $context);  
    $context = str_ireplace("<B>", '', $context);  
    $context = str_ireplace("</B>", '', $context);  
    //$context = str_ireplace("</A>", '', $context);  
    $context = str_ireplace("</STRONG>", '', $context);  
    $context = str_ireplace("<STRONG>", '', $context);  
    $context = str_ireplace("</DIV>", ' ', $context);  
    $context = str_ireplace("</TABLE>", ' ', $context);  
    $context = str_ireplace("</TR>", ' ', $context);  
    $context = str_ireplace("</TD>", ' ', $context);  
    $context = str_ireplace("<TBODY>", "\n", $context);  
    $context = str_ireplace("</TBODY>", ' ', $context);  
    $context = preg_replace("/<DIV.*?>/", "\n", $context);  
    $context = preg_replace("/<TABLE.*?>/", "\n", $context);  
    $context = preg_replace("/<TR.*?>/", "\n", $context);  
    $context = preg_replace("/<TD.*?>/", "\t", $context);  
    $context = preg_replace("/<!--.*?-->/", "", $context);  
    $context = preg_replace("/<A.*?>/", "", $context);  
    $context = preg_replace("/<font.*?<\/font>/", "", $context);  
    $context = preg_replace("/<a.*>/", "", $context);  
    $context = preg_replace("/<p.*?>/", "\n\n", $context);  
    $context = preg_replace("/<style[\s\S]*?style>/", '', $context);  
    */

    $context = str_ireplace('&nbsp;', '', $context);    
    $i=0;
    while ( $i<= 10) {
      $context = str_ireplace('  ', '', $context);
      $context = str_ireplace("\n\n", '', $context);
      $i++;
    }
    $context = strip_tags($context);
    return $context;  
}  

/**
      dashboard
**/

function dashboard(){
  global $c,$a, $outVar;
  $html_src='';
  $countSql = prepare('SELECT count(*) as num FROM `shdic_wbd2016_download_jobs` WHERE `is_done`=?i ' ,array(0));
  $count=get_var($countSql);
  $html_src .='<h3>#0 ,Count: '.$count.'</h3>'.PHP_EOL;

  $countSql = prepare('SELECT count(*) as num FROM `shdic_wbd2016_download_jobs` WHERE `is_done`=?i ' ,array(1));
  $count=get_var($countSql);
  $html_src .='<h3>#1 ,Count: '.$count.'</h3>'.PHP_EOL;

  $countSql = prepare('SELECT count(*) as num FROM `shdic_wbd2016_download_jobs` WHERE `is_done`=?i ' ,array(2));
  $count=get_var($countSql);
  $html_src .='<h3>#2, Count: '.$count.'</h3>'.PHP_EOL;

  $outVar['html_data']=$html_src;     
  return $html_src;
}

function add_jobs($listUrl,$listSrc,$is_done=0,$cat=1){
  $cf_now = date('Y-m-d H:i:s');
  $kk=$listSrc;
  $kk=addslashes($kk);
  $kk=func_str_compress($kk);
  $listUrl=strtolower($listUrl);
  $uuid=md5($listUrl);
  $tmpSql = prepare("INSERT INTO `wbd_jobs`(`uuid`, `is_done`, `ud1`,`cf1`, `cf2`, `job_url`, `html_src`) VALUES (?s,?s,?s,?s,?s,?s,?s)",array($uuid,intval($is_done),intval($cat),$cf_now,$cf_now,$listUrl,$kk));
  echo red_span(__METHOD__.'()'.$listUrl. ' tmpSql='.$tmpSql).'<br/>'.PHP_EOL;  
  if(run_sql($tmpSql)){
    return true;
  }else{
    return false;
  }
}

/**
    
        user defined func

**/
function func_get_all_href_link($html){
  $retArr=array();
  $key2='</a>';
  $key3='href=';
  $key4='"';
  $key5="'";
  $tmpArr=explode($key2,$html);
  foreach ($tmpArr as $key => $value) {
    $tt=_cut_middle_str($value.$key2,$key3.$key4,$key4);
    if(empty($tt)){
      $tt=_cut_middle_str($value.$key2,$key3.$key5,$key5);
    }
    $retArr[]=$tt;
  }
  return $retArr;
}

function add_download_jobs($job_url,$job_mode=0,$title=''){
  if(empty($job_url)) return false;
  // INSERT INTO `shdic_wbd2016_download_jobs`(`uuid`, `is_done`, `job_mode`, `title`, `cf1`, `cf2`, `job_url`) VALUES ([value-1],[value-2],[value-3],[value-4],[value-5],[value-6],[value-7])
  
  $job_url=strtolower($job_url);
  $uuid=md5($job_url);
  $cf_now = date('Y-m-d H:i:s');
  $tmpSql2 = prepare('INSERT INTO `shdic_wbd2016_download_jobs`(`uuid`, `is_done`, `job_mode`, `title`, `cf1`, `cf2`, `job_url`) VALUES (?s,?s,?s,?s,?s,?s,?s)' ,array($uuid,0,intval($job_mode),$title,$cf_now,$cf_now,$job_url));
  return run_sql($tmpSql2);
}


?>
<?php
