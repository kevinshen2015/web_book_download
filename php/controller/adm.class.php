<?php
if( !defined('IN') ) die('bad request');
include_once( CROOT . 'controller' . DS . 'core.class.php' );

ini_set( 'display_errors' , true );
error_reporting(E_ALL ^ E_NOTICE);

class admController extends coreController
{
	function __construct()
	{

		// 载入默认的
		parent::__construct();
		
		$this->check_login();
				
	}

	function check_login()
	{
		$ret_arr=array();
		api_header();
		if( !is_login() ) {
  			echo api_result_json($ret_arr,'Need Login! 你访问的内容，需要先登录！',1001);
  			die();
		}

		if(isset($GLOBALS['config']['runtime_env_check'])){
			$runtime_env_check=$GLOBALS['config']['runtime_env_check'];
			if($runtime_env_check =='local'){
				//local dev ,logined ,pass
				return true;
			}
		}
		
		if( !is_admin() ){
  			die(api_result_json($ret_arr,'Only For Admin! 你访问的内容，需要管理员权限才能操作！',1002));	
		}
	}

	private function get_idx_by_val($arr,$val){
		if(is_array($arr))
		foreach ($arr as $key => $value) {
			if($value==$val){
				return $key;
			}
		}
		return false;
	}

/**

	Admin 生成JS文件

**/	

	function jsMiddleSchool()
	{
		$ret_arr=array();
		api_header();

		$code=v('adm');
		if($code=='2016'){
			$select_middleSchool=array();
			$dict_middleSchool=array();

		// select loc FROM `middle_school_info` GROUP BY loc ORDER BY id
			$tmp_sql="SELECT * FROM `middle_school_info` GROUP BY loc ORDER BY id ";
			$ret_sql = get_data($tmp_sql );
			if($ret_sql!=false){
				$idx = -1;
				$last_loc=null;
				foreach ($ret_sql as $key => $value) {
					
					if($last_loc!==$value['loc']){
						$idx++;
						$select_middleSchool[$idx]=$value['loc'];
						$dict_middleSchool[$idx]=array();
						$last_loc=$value['loc'];
					}

					// $dict_middleSchool[$idx][$value['id']]=$value['title'];
				}
			}


// SELECT * FROM `middle_school_info` order by loc desc,ybl2015+0 desc,ebl2015+0 desc
			$tmp_sql="SELECT * FROM `middle_school_info` order by loc desc,ybl2015+0 desc,ebl2015+0 desc ";
			$ret_sql = get_data($tmp_sql );
			if($ret_sql!=false){
				$idx = -1;
				$last_loc=null;
				foreach ($ret_sql as $key => $value) {
					
					if($last_loc!==$value['loc']){						
						$last_loc=$value['loc'];
						$idx=$this->get_idx_by_val($select_middleSchool,$last_loc);
					}

					// $dict_middleSchool[$idx][$value['id']]=$value['title'];
					$dict_middleSchool[$idx][]=$value['id'].'=='.$value['title'];
				}

				// print_r($select_middleSchool);
				// print_r($dict_middleSchool);
				foreach ($select_middleSchool as $key => $value) {
					echo 'mid_sch_select_key['.$key."] = '".$value."';".PHP_EOL;
				}

				foreach ($dict_middleSchool as $key => $value) {
					echo 'mid_sch_option_key['.$key."] = Array();".PHP_EOL;
					foreach ($value as $k2 => $v2) {
						echo "mid_sch_option_key[".$key."]['".$k2."'] = '".$v2."';".PHP_EOL;	
					}
					
				}
				die();
				die(api_result_json($ret_sql,'sql ok!'));
			}
		}
			
		die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()'.$code,1100));
	}


	function jsMajorConfig()
	{
		$ret_arr=array();
		api_header();

		$code=v('adm');
		if($code=='2016'){

			$tmp_sql="SELECT * FROM `major_config` limit 3000";
			$ret_sql = get_data($tmp_sql );
			if($ret_sql!=false){
				// die(api_result_json($ret_sql,'sql ok!'));
				$idx = 0;
				$cat1=null;
				foreach ($ret_sql as $k2 => $v2) {
					$cat_now = substr($v2['code'],0,2);
					if($cat1 != $cat_now){
						$cat1=$cat_now;
						$idx ++;
						$idx2 = 0;						
						echo 'maj_conf_select_key['.$idx."] = '".$v2['title']."';".PHP_EOL;
						echo 'maj_conf_option_key['.$idx."] = Array();".PHP_EOL;
					}else{
						$tt = $v2['code']."==".$v2['title'];
						$tt = substr($tt,2);
						echo "maj_conf_option_key[".$idx."][".$idx2."] = '".$tt."';".PHP_EOL;
					$idx2++;	
					}
					
				}
				die();
			}
		}			
		die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()'.$code,1100));
	}


	function js_university_comp_ranking()
	{
		$ret_arr=array();
		api_header();

		$code=v('adm');
		if($code=='2016'){

			$tmp_sql="SELECT * FROM `university_comp_ranking` order by ranking limit 3000";
			$ret_sql = get_data($tmp_sql );
			if($ret_sql!=false){
				// die(api_result_json($ret_sql,'sql ok!'));
				$idx = 0;
				$cat1=null;
				foreach ($ret_sql as $k => $v) {
					$idx ++;
					echo 'uni_rank_select_key['.$idx."] = '".$v['title']."';".PHP_EOL;
					// $tt=$v['title'].'**';
					$tt=$v['location'].'=='.$v['nature'];
					echo 'uni_rank_option_key['.$idx."] = '".$tt."';".PHP_EOL;
				}
				die();
			}
		}			
		die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()'.$code,1100));
	}


	function js_uni_base_info()
	{
		$ret_arr=array();
		api_header();

		$code=v('adm');
		if($code=='2016'){

			$tmp_sql="SELECT `code`, `ud1` as nature, `ud2` as location, `ud3` , `ud6` as title,`salary5`,`ind0` as industry FROM `shdic_crx_p2015qq176806817_school` order by code limit 3000";			
			$ret_sql = get_data($tmp_sql );
			if($ret_sql!=false){
				// die(api_result_json($ret_sql,'sql ok!'));
				$idx = 0;
				$cat1=null;

				echo 'var uni_bsk = new Object(); //select_key'.PHP_EOL;
				echo 'var uni_bok = new Array(); //option_key'.PHP_EOL;

				foreach ($ret_sql as $k => $v) {
					$score1='0';
					$score2='0';					
					$tmp_sql2=prepare("SELECT * FROM `school_info` Where code=?s limit 1",array($v['code']));
					if($ret_sql2 = get_line($tmp_sql2 )){
						if(isset($ret_sql2['min_score1']) && intval($ret_sql2['min_score1'])>0){
							$score1=intval($ret_sql2['min_score1']);
						}
						if(isset($ret_sql2['min_score2']) && intval($ret_sql2['min_score2'])>0){
							$score2=intval($ret_sql2['min_score2']);
						}
					}

					$idx ++;
					echo 'uni_bsk['.$idx."] = '".$v['code']."';".PHP_EOL;
					$tt=trim($v['title']).'|';
					$tt.=$v['location'].'|'.$v['nature'].'|';
					$tt.=$v['salary5'].'|'.$v['industry'].'|';
					$tt.=$score1.'|'.$score2.'|';

					$ud3='kk'.$v['ud3'];
					if(stripos($ud3, '本科')!==false){
						$tt.='bk';
					}
					if(stripos($ud3, '专科')!==false){
						$tt.='zk';
					}
					echo 'uni_bok['.$idx."] = '".$tt."';".PHP_EOL;
				}
				die();
			}
		}			
		die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()'.$code,1100));
	}

/**

		admin 维护操作

**/

	private function make_build_url($fname,$root=''){
		$app_root=$root.'index.php?a=';	
		$ext=mt_rand(1,10000);	
		$ext.='&admin=C0861D93492893B048257E660006B6E7';	
		$ext.='&build_type=build_static_html';
		// $ext.='&build_type=Domino_Server_Page';
		// $run_time=date('Y-m-d H:i:s');
		// $ext.='&st='.$run_time;
		return $app_root.$fname.'&_='.$ext;
	}

	private function iframe_src($tmp_url,$w=1024,$h=768){
		$html_src='<iframe class="rightlist" marginheight="0" marginwidth="0" frameborder="0" scrolling="auto"  height="'.$h.'px" width="'.$w.'px" src="" src1="'.$tmp_url.'"></iframe>'.PHP_EOL;
		return $html_src;
	}

	function build_static_html(){
		api_header();
		echo '<!-- DEBUG: '.__METHOD__.'() -->'.PHP_EOL;
		// die();
		$page_url=$this->make_build_url('index');

        $html_tpl='js.tpl.html';
        $outVar['page_title'] = 'Adm.建立静态文件';
		$debug_src='';
		$html_src=$this->iframe_src($page_url);
		$outVar['html_data']=$html_src;
		if(1){
			$cf_now = date('Y-m-d H:i:s');
			$js='
	var delay=86;

	$(".rightlist").each(function(){
		var $this = $(this);
		setTimeout(function(){
			$this.attr("src",$this.attr("src1"));
        },delay);
        delay +=100;

	});';
			$outVar['js_str']='console.log("'.$cf_now.'");'.$js;
		}
		html_render( $outVar , array('ui_tpl' => $html_tpl,'debug_src'=>$debug_src));

		die(__METHOD__.'() Process End!');
	}

	function auto_fix()		//日常基础维护
	{
		api_header();
		echo '<!-- DEBUG: '.__METHOD__.'() -->'.PHP_EOL;

		// UPDATE `major_stat` SET zf_ut=null,`nsbl_ut`=null,`ind_ut`=null WHERE 1

		$html_tpl='js';
        $html_tpl='js.tpl.html';
        $outVar['page_title'] = 'Adm@日常基础维护';
		$debug_src='';

		$sub_func_idx=v('job');
		switch ($sub_func_idx) {
			case '1':
				$func_name='adm_update_major_stat_ud1_ud3';
				break;
			
			case '2':
				$func_name='adm_update_major_stat_ud4';
				break;

			case '3':
				$func_name='adm_update_major_stat_ind';
				break;

			case '4':
				$func_name='adm_update_major_stat_all_without_ind';
				break;

			default:
				$func_name='';
				break;
		}

		// $html_src=adm_update_major_stat_ud4();
		if(!empty($func_name)){
			$html_src=call_user_func($func_name);
		}else{
			$html_src='skip';
		}
		$outVar['html_data']=$html_src;
		if(strpos($html_src,'UPDATE ')!==false){
			// $outVar['js_str']='setInterval("location.reload()", 6000); ';
			$cf_now = date('Y-m-d H:i:s');
			$outVar['js_str']='setTimeout("location.reload()", 9000); console.log("'.$cf_now.'")';
		}
		html_render( $outVar , array('ui_tpl' => $html_tpl 
			,'debug_src'=>$debug_src));

		die(__METHOD__.'() Process End!');
	}


	function reinit_table_major_stat()		//检查更新专业统计表
	{
		$ret_arr=array();
		api_header();

		$tmp_sql="SELECT title,COUNT(*) as num  FROM `school_major` GROUP BY `title`";		
		if($ret_sql = get_data($tmp_sql )){

			$cf_now = date('Y-m-d H:i:s');
			$idx=0;

			foreach ($ret_sql as $k => $v) {
				$idx++;
				$title = $v['title'];
				echo red_span('#'.$idx.' '.$title).'<br/>'.PHP_EOL;
				$tmp3_sql=prepare("SELECT count(*) as num FROM `major_stat` WHERE `title` =?s limit 1 ",array($title));
				$ret_count=get_var($tmp3_sql);
				if(intval($ret_count)<1){
					echo 'count='.$ret_count . PHP_EOL;
					if($ret_count<1){
						$new_sql=prepare("INSERT INTO `major_stat` (`code`, `is_done`, `verkey`, `title`, `cf1`, `cf2`) VALUES (?s,0,1,?s,?s,?s);",array(kis_uuid(),$title,$cf_now,$cf_now));
						if($ret_new_sql = run_sql($new_sql )){
							echo $title . ' init ok !'.PHP_EOL;
						}else{
							echo $title . ' init ERROR !!! '.PHP_EOL;
						}
					}
				}else{
					echo 'exist ok'. PHP_EOL;
				}
			}

		}else{
			die(api_result_json($ret_arr,'检索失败！'.__METHOD__.'()'.$tmp_sql,1200));
		}	

		die(__METHOD__.'() Process End!');
	}


	function fix_table_sch_inf_admission_score()	
	{
/*
//大学分数线,用于大学首页搜索

ALTER TABLE `school_info`
ADD COLUMN `min_score`  mediumint(4) NULL COMMENT '分数线' AFTER `sch_id`;

ALTER TABLE `school_info`
MODIFY COLUMN `min_score`  mediumint(4) NULL DEFAULT NULL COMMENT '去年/最低分数线' AFTER `sch_id`,
ADD COLUMN `min_score1`  mediumint(4) NULL COMMENT '文科分数线' AFTER `min_score`,
ADD COLUMN `min_score2`  mediumint(4) NULL COMMENT '理科分数线' AFTER `min_score1`,
ADD COLUMN `subject_cat`  tinyint(1) NULL COMMENT '1=文科2=理科' AFTER `min_score2`;

*/
		$ret_arr=array();
		api_header();

		$code=v('adm');
		if(empty($code) || intval($code)!=2016){
			die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()'.$code,1100));
		}

		// SELECT `uuid`, `code`, `school_title`, `major_title`, `pici`, `x2`, `plan2012`, `offer2012`, `lowest2012`, `plan2013`, `offer2013`, `lowest2013`, `plan2014`, `offer2014`, `lowest2014`, `yibenxian2012`, `erbenxian2012`, `yibenxian2013`, `erbenxian2013`, `yibenxian2014`, `erbenxian2014` FROM `school_admission_score` WHERE 1
		// select * from `school_admission_score` where `major_title` = 'all'
		$tmp_sql="SELECT * FROM `school_admission_score` where `major_title` = 'all' limit 3000";
		$ret_sql = get_data($tmp_sql );
		if($ret_sql===false){
			die(api_result_json($ret_arr,'检索失败！'.__METHOD__.'()'.$tmp_sql,1200));
		}	

// UPDATE `school_info` SET `code`=[value-1],`is_done`=[value-2],`verkey`=[value-3],`title`=[value-4],`ud1`=[value-5],`ud2`=[value-6],`ud3`=[value-7],`ud4`=[value-8],`ud5`=[value-9],`ud6`=[value-10],`ud7`=[value-11],`ud8`=[value-12],`ud9`=[value-13],`cf1`=[value-14],`cf2`=[value-15],`majorInfo`=[value-16],`sch_id`=[value-17],`min_score`=[value-18] WHERE 1

		$year_arr=array();
		$this_year=intval(date('Y'));
		for ($i=2012; $i <=$this_year ; $i++) { 
		    $year_arr[]=$i;
		}
		arsort($year_arr);
		$last_year=null;

		$idx = 0;
		$cat1=null;
		foreach ($ret_sql as $k => $v) {
			$sch_code = $v['code'];

			$tmp3_sql=prepare("SELECT * FROM `school_info` WHERE `code` =?s limit 1 ",array($sch_code));
			$ret_sch_info=get_line($tmp3_sql);
			if($ret_sch_info!==false){

				$sch_min_score=intval($ret_sch_info['min_score']);
				if($last_year===null){
					foreach ($year_arr as $tmp_year) {
						if($last_year) break;
						if(isset($v['lowest'.$tmp_year])){							
							$last_year = $tmp_year;
							break;
						}
					}
					echo __METHOD__.'() last_year='.$last_year.PHP_EOL;
				}
				$min_score = $v['lowest'.$last_year];
				$min_score = intval($min_score);

				if($min_score<$sch_min_score){
					$sch_min_score=$min_score;
				}

				if(stripos($v['x2'], '理工类')!=false){
					$tmp2_sql=prepare("UPDATE `school_info` SET `min_score2`=?i, `is_subject_cat2`=?i, `min_score`=?i WHERE `code` =?s limit 1 ",array($min_score,1,$sch_min_score,$sch_code));
				}else{
					$tmp2_sql=prepare("UPDATE `school_info` SET `min_score1`=?i, `is_subject_cat1`=?i, `min_score`=?i WHERE `code` =?s limit 1 ",array($min_score,1,$sch_min_score,$sch_code));
				}

				$idx++;
				echo red_span('#'.$idx.' '.$v['school_title']).'<br/>'.PHP_EOL;
				if($ret2_sql = run_sql($tmp2_sql )){
					// echo $v['school_title'] . ' fix ok '.$min_score.PHP_EOL;
				}else{
					echo $v['school_title'] . ' fix ERROR!!! '.$min_score.PHP_EOL;
				}

			}
			
		}

		die();
	}



	function fix_table_sch_inf_xingzhi_tese_remarks()	
	{
/*
xingzhi 	=school_ruanyingjian.ud2
tese 		=school_ruanyingjian.ud5
remarks 	=enrollment_directory,招生目录综合-备注

ALTER TABLE `school_info`
ADD COLUMN `remarks`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '招生目录综合-备注' AFTER `yd_tips4`;

ALTER TABLE `school_info`
ADD COLUMN `tese`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '软硬件-ud5' AFTER `yd_tips4`;

ALTER TABLE `school_info`
ADD COLUMN `xingzhi`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '软硬件-ud2' AFTER `yd_tips4`;
*/
		$ret_arr=array();
		api_header();
		$debug_log='';

		$code=v('adm');
		if(empty($code) || intval($code)!=2016){
			die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()'.$code,1100));
		}

		// select * from `enrollment_directory` where `maj_title` = ''
		$tmp_sql="SELECT * FROM `enrollment_directory` where `maj_title` = '' limit 3000";
		if($ret_sql = get_data($tmp_sql) ){
			$idx = 0;			
			foreach ($ret_sql as $k => $v) {
				$sch_code = $v['sch_code'];
				$idx++;
				echo red_span('#'.$idx.' '.$v['sch_title']).'<br/>'.PHP_EOL;

				if(!empty($v['remarks'])){
					$tmp3_sql=prepare("SELECT * FROM `school_info` WHERE `code` =?s limit 1 ",array($sch_code));
					if($ret_sch_info=get_line($tmp3_sql)){

						$tmp2_sql=prepare("UPDATE `school_info` SET `remarks`=?s WHERE `code` =?s limit 1 ",array($v['remarks'],$sch_code));

						if($ret2_sql = run_sql($tmp2_sql )){
							echo ' fix ok '.'<br/>'.PHP_EOL;
						}else{
							echo ' fix Error!!! '.'<br/>'.PHP_EOL;
						}

					}
				}
			}

		}else{
			// die(api_result_json($ret_arr,'检索失败！'.__METHOD__.'()'.$tmp_sql,1200));
			$debug_log.='检索失败！'.__METHOD__.'(enrollment_directory)'.$tmp_sql;
		}



		$tmp_sql="SELECT * FROM `school_ruanyingjian` where ud2<>'' or ud5<>'' limit 3000";
		if($ret_sql = get_data($tmp_sql) ){
			$idx = 0;			
			foreach ($ret_sql as $k => $v) {
				$sch_code = $v['code'];
				$idx++;
				echo red_span('#'.$idx.' '.$v['title']).'<br/>'.PHP_EOL;

				if(true){
					$tmp3_sql=prepare("SELECT * FROM `school_info` WHERE `code` =?s limit 1 ",array($sch_code));
					if($ret_sch_info=get_line($tmp3_sql)){

						$tmp2_sql=prepare("UPDATE `school_info` SET `xingzhi`=?s,`tese`=?s WHERE `code` =?s limit 1 ",array($v['ud2'],$v['ud5'],$sch_code));

						if($ret2_sql = run_sql($tmp2_sql )){
							echo ' fix ok '.'<br/>'.PHP_EOL;
						}else{
							echo ' fix Error!!! '.'<br/>'.PHP_EOL;
						}

					}
				}
			}

		}else{
			// die(api_result_json($ret_arr,'检索失败！'.__METHOD__.'()'.$tmp_sql,1200));
			$debug_log.='检索失败！'.__METHOD__.'(school_ruanyingjian)'.$tmp_sql;
		}

		die(__METHOD__.'() process end!');
	}


	function fix_table_enrollment_directory_major_config()	//enrollment_directory
	{
/*
添加 major_config

ALTER TABLE  `enrollment_directory` ADD  `maj_cat_code` CHAR( 8 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT  '专业分类代码';
*/
		$ret_arr=array();
		api_header();
		$debug_log='';

		$code=v('adm');
		if(empty($code) || intval($code)!=2016){
			die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()'.$code,1100));
		}

		// select * from `enrollment_directory` where `maj_title` = ''
		$tmp_sql="SELECT * FROM `enrollment_directory` where `maj_title` <> '' and (`maj_cat_code` is null or `maj_cat_code`='' ) limit 30";
		if($ret_sql = get_data($tmp_sql) ){
			$idx = 0;			
			foreach ($ret_sql as $k => $v) {
				$maj_title = $v['maj_title'];
				$idx++;
				echo red_span('#'.$idx.' '.$v['sch_title']).'**'.$maj_title.'<br/>'.PHP_EOL;

				if(isset($v['remarks'])){
					$tmp3_sql=prepare("SELECT * FROM `major_config` WHERE `title` =?s limit 1 ",array($maj_title));
					if($ret_sch_info=get_line($tmp3_sql)){
						$maj_cat_code=$ret_sch_info['code'];
					}else{
						echo 'Not Found !!! '.$tmp3_sql.'<br/>'.PHP_EOL;
						$maj_cat_code='null';
					}
					$tmp2_sql=prepare("UPDATE `enrollment_directory` SET `maj_cat_code`=?s WHERE `id` =?s limit 1 ",array($maj_cat_code,$v['id']));

					if($ret2_sql = run_sql($tmp2_sql )){
						echo ' fix ok ';
					}else{
						echo ' fix Error !';
					}
					echo $tmp2_sql.'<br/>'.PHP_EOL;
				}else{
					die('miss field !'.__METHOD__.'()');
				}
			}

		}else{
			// die(api_result_json($ret_arr,'检索失败！'.__METHOD__.'()'.$tmp_sql,1200));
			$debug_log.='检索失败！'.__METHOD__.'(enrollment_directory)'.$tmp_sql;
		}

		die(__METHOD__.'() process end!');
	}

	function fix_table_enrollment_directory_major_fenshuxian()	//enrollment_directory
	{
/*
添加 去年分数线数据

ALTER TABLE `enrollment_directory`
ADD COLUMN `fenshuxian`  int(11) NULL DEFAULT NULL COMMENT '去年的分数线' AFTER `maj_cat_code`;
*/
		$ret_arr=array();
		api_header();
		$debug_log='';

		$code=v('adm');
		if(empty($code) || intval($code)!=2016){
			die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()'.$code,1100));
		}

		$last_year=date('Y');
		$last_year=$last_year-1;

		// $debug_where = prepare(" AND `sch_code` =?s ",array('10a5e9a88b924660f611134c922443ca'));
		$debug_where='';

		// select * from `enrollment_directory` where `maj_title` = ''
		$tmp_sql="SELECT * FROM `enrollment_directory` where `maj_title` <> '' ".$debug_where." and (`fenshuxian` is null or `fenshuxian`='' ) limit 20000";
		if($ret_sql = get_data($tmp_sql) ){
			$idx = 0;			
			foreach ($ret_sql as $k => $v) {
				$sch_code = $v['sch_code'];
				$maj_title = $v['maj_title'];
				if(stripos($maj_title, '(')!=false){
					$tmparr=explode('(', $maj_title);
					$maj_title=$tmparr[0];
				}
				if(stripos($maj_title, '（')!=false){
					$tmparr=explode('（', $maj_title);
					$maj_title=$tmparr[0];
				}

				$idx++;
				echo red_span('#'.$idx.' '.$v['sch_title']).'**'.$maj_title.'<br/>'.PHP_EOL;
				if(intval($maj_title)>0){
					print_r($v);
					echo '<br/>'.PHP_EOL;
				}
// SELECT `id`, `code`, `school_title`, `major_title`, `pici`, `x2`, `plan`, `offer`, `lowest`, `yibenxian`, `erbenxian`, `year` FROM `school_admission_score_real` WHERE 1
				if(isset($v['remarks'])){
					$tmp3_sql=prepare("SELECT * FROM `school_admission_score_real` WHERE `code` =?s AND major_title like ?s AND year=?i limit 1 ",array($sch_code,'%'.$maj_title.'%',$last_year));
					if($ret_sch_info=get_line($tmp3_sql)){
						$lowest=$ret_sch_info['lowest'];
						$tmp2_sql=prepare("UPDATE `enrollment_directory` SET `fenshuxian`=?s WHERE `id` =?s limit 1 ",array($lowest,$v['id']));

						if($ret2_sql = run_sql($tmp2_sql )){
							echo ' fix ok '.'<br/>'.PHP_EOL;
						}else{
							echo ' fix Error!!! '.'<br/>'.PHP_EOL;
						}

					}else{
						echo $tmp3_sql.'<br/>'.PHP_EOL;
					}
				}
			}

		}else{
			// die(api_result_json($ret_arr,'检索失败！'.__METHOD__.'()'.$tmp_sql,1200));
			$debug_log.='检索失败！'.__METHOD__.'(enrollment_directory)'.$tmp_sql;
		}

		die(__METHOD__.'() process end!');
	}

	function fix_table_enrollment_directory_major_code()	//enrollment_directory
	{
/*
添加 大学专业的code

ALTER TABLE `enrollment_directory`
ADD COLUMN `maj_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '大学专业code' AFTER `maj_cat_code`;
*/
		$ret_arr=array();
		api_header();
		$debug_log='';

		$code=v('adm');
		if(empty($code) || intval($code)!=2016){
			die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()'.$code,1100));
		}

		$last_year=date('Y');
		$last_year=$last_year-1;

		// select * from `enrollment_directory` where `maj_title` = ''
		$tmp_sql="SELECT * FROM `enrollment_directory` where `maj_title` <> '' and (`maj_code` is null or `maj_code`='' ) limit 30";
		if($ret_sql = get_data($tmp_sql) ){
			$idx = 0;			
			foreach ($ret_sql as $k => $v) {
				$sch_code = $v['sch_code'];
				$maj_title = $v['maj_title'];
				$idx++;
				echo red_span('#'.$idx.' '.$v['sch_title']).'**'.$maj_title.'<br/>'.PHP_EOL;
				if(intval($maj_title)>0){
					print_r($v);
					echo '<br/>'.PHP_EOL;
				}

				if(isset($v['remarks'])){
					$tmp3_sql=prepare("SELECT * FROM `school_major` WHERE `ud1` =?s AND title like ?s limit 1 ",array($sch_code,'%'.$maj_title.'%'));
					if($ret_sch_info=get_line($tmp3_sql)){
						$maj_code=$ret_sch_info['code'];
					}else{
						echo 'Not Found !!! '.$tmp3_sql.'<br/>'.PHP_EOL;
						$maj_code='null';
					}
					$tmp2_sql=prepare("UPDATE `enrollment_directory` SET `maj_code`=?s WHERE `id` =?s limit 1 ",array($maj_code,$v['id']));

					if($ret2_sql = run_sql($tmp2_sql )){
						echo ' fix ok ';
					}else{
						echo ' fix Error !';
					}
					echo $tmp2_sql.'<br/>'.PHP_EOL;
				}else{
					die('Error #2 @'.__METHOD__.'()');
				}
			}

		}else{
			// die(api_result_json($ret_arr,'检索失败！'.__METHOD__.'()'.$tmp_sql,1200));
			$debug_log.='检索失败！'.__METHOD__.'(enrollment_directory)'.$tmp_sql;
		}

		die(__METHOD__.'() process end!');
	}

	function load_user_op_list()
	{
		global $limit,$offset;
		$ret_arr=array();
		api_header();
		$debug_log='';

		if(!is_admin()){
			die(api_result_json($ret_arr,'Error! 检索参数无效！'.__METHOD__.'()'.$code,1100));
		}
		
		$table = real_table_name('user');
		$where='WHERE `is_closed` = 0 AND `level`<9';

		$usr_mod=v('mod');
		switch ($usr_mod) {
			case '2':
				$where.=' AND `level`>4 ';
				break;
			
			default:
				$where.=' AND `level`<5 ';
				break;
		}
		
		init_page_start_limit();
		fix_page_limit();
// SELECT `id`, `name`, `pinyin`, `email`, `password`, `avatar_small`, `avatar_normal`, `level`, `timeline`, `settings`, `is_closed`, `mobile`, `tel`, `eid`, `weibo`, `memo`, `groups` FROM `user` WHERE 1

		$count_sql = 'SELECT count(*) as count FROM '.$table.' '.$where .' LIMIT 1 ';
		// if($debugMode) $debug_log.='count_sql:'.$count_sql;
		$count_ret=get_var($count_sql);
		if($count_ret===false){
			die(api_result_json($ret_arr,'Error! 检索参数无效！'.__METHOD__.'()'.$count_sql,1200));
		}

		if($count_ret==0){
			die(api_result_json($ret_arr,'没有找到符合检索条件的数据！',1210));
			 // debug message:'.__METHOD__.'()'.$count_sql
		}
	
		$tmp_sql = "SELECT `id`, `name`,  `email`,  `level` FROM ".$table.' '.$where .' LIMIT '.intval($limit).', '.intval($offset);
		if($debugMode) $debug_log.='sql:'.$tmp_sql;
		// die($tmp_sql);

		$outVar['html_fanye1']='复杂搜索只显示搜索结果的第一页！';
		$ret_sql=get_data($tmp_sql);
		if($ret_sql!==false){

			$tmpPageArr=array();
			$tmpPageArr['count']=count($ret_sql);
			$tmpPageArr['data']=$ret_sql;
			$page_div=get_pageview($tmpPageArr,'','gotopage');

			die(api_result_json($ret_sql,'sql ok!'.$debug_log,0,$count_ret,$page_div));
		}
		
		die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()'.$tmp_sql,1100));

	}

	function set_user()
	{
		$ret_arr=array();
		api_header();
		$debug_log='';

		if(!is_admin()){
			die(api_result_json($ret_arr,'Only for Admin !'.__METHOD__.'()'.$code,110));
		}

		$usrid=v('usrid');
		$op=v('op');
		if(empty($usrid) || intval($usrid)<1 ||empty($op) || intval($op)<1){
			die(api_result_json($ret_arr,'Error! 检索参数无效！'.__METHOD__.'()',1100));
		}

		$new_level=1;
		switch ($op) {
			case '1':
				$new_level=5;
				break;
			
			default:
				# code...
				break;
		}

		$table = real_table_name('user');
		$sql = "UPDATE `".$table."` SET `level` = '". intval($new_level)."' WHERE `id` = '" . intval( $usrid ) . "' LIMIT 1";
		if(run_sql( $sql )){
			die('设置成功！');
		}else{
			die('设置失败！'.$sql);
		}

		die(__METHOD__.'() process end!');
	}

/**


**/

	function show_radis()
	{
		$ret_arr=array();
		api_header();
		$debug_log='';

		$ret = redis_get_allkeys('user_access_count_*');

		if($ret!==false){
			$idx=0;
			foreach ($ret as $akey) {
				$tmpval=get_redis($akey);
				if($tmpval===false){
					$tmpval='false';
				}
				echo '#'.$idx.', '.$akey.'='.$tmpval.PHP_EOL;
			}
		}

		die(__METHOD__.'() process end!');
	}	

}


?>