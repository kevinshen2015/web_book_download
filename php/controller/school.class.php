<?php
if( !defined('IN') ) die('bad request');
include_once( CROOT . 'controller' . DS . 'core.class.php' );

ini_set( 'display_errors' , true );
error_reporting(E_ALL ^ E_NOTICE);

class schoolController extends coreController
{
	function __construct()
	{

		// 载入默认的
		parent::__construct();
		
		$this->check_login();
				
	}

	function check_login()
	{
		if( !is_login() ) {
			// die('wa hahahah '.__METHOD__.'(DEBUG)');
			$ret_arr=array();
			api_header();
  			echo api_result_json($ret_arr,'你访问的内容，需要先登录！',1001);
  			die();
		}	
	}
	
	function major_rank_6()
	{
		// die('wa hahahah '.__METHOD__.'(DEBUG)');
		// $data['title'] = $data['top_title'] = __('MEMBER_PAGE_TITLE');
		// $data['js'][] = 'jquery.tagsinput.js';
		// render( $data , 'web' , 'card' );

		// api_major_list();
		api_header();

		$ret_arr=array();

		$year=intval(v('year'));
		if ($year<2012 && $year>2014){
	  		die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'() '.$year,1001));
		}

		$sc=intval(v('sc'));
		if($sc<1 || $sc>2){
			die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'() '.$sc,1002));	
		}
		if($sc==1){
			$sc='文史类';
		}else if($sc==2){
			$sc='理工类';
		}
		$sc='%'.$sc.'%';

		$school_id=v('sid');
		$key='lowest'.$year;

		// SELECT * FROM `school_admission_score` LIMIT 0, 1000
		if(!empty($school_id) && strlen($school_id)>=30){
			$tmp_sql = prepare('SELECT uuid,code,major_title,'.$key.' as lowest FROM  `school_admission_score` WHERE code=?s and x2 like ?s order by '.$key.' desc limit 50' ,array($school_id,$sc));
			
			if($ret_sql = get_data($tmp_sql )){
				die(api_result_json($ret_sql,'sql ok!'));
			}else if($ret_sql===false){
				// die(api_result_json($ret_arr,'没有符合条件的数据！'.__METHOD__.'()'.$tmp_sql,1101));
				die(api_result_json($ret_arr,'没有符合条件的数据！',120));
			}
		}

		die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()'.$school_id,1100));
	}
	

	function software_hardware()
	{
		$ret_arr=array();
		api_header();
		$code=v('scode');
		// SELECT `code`, `title`, `xiaoqu`, `yinshi`, `zhusu`, `wangluo`, `xueshu`, `chuangye`, `kaoyan`, `kaogongwuyuan`, `chuguo`, `jiuye`, `xuefei`, `jiaotong`, `zhuanyejicha`, `jieshao` FROM `school_ruanyingjian` WHERE 1
		if(!empty($code) && strlen($code)>=30){
			$tmp_sql=prepare("SELECT `code`, `title`, `xiaoqu`, `yinshi`, `zhusu`, `wangluo`, `xueshu`, `chuangye`, `kaoyan`, `kaogongwuyuan`, `chuguo`, `jiuye` as jiuye_bl, `xuefei`, `jiaotong`, `zhuanyejicha` FROM `school_ruanyingjian` WHERE `code` =?s limit 1 ",array($code));

			if($ret_sql = get_data($tmp_sql )){
				die(api_result_json($ret_sql,'sql ok!'));
			}else if($ret_sql===false){
				die(api_result_json($ret_arr,'没有符合条件的数据！',120));
			}
		}
			
		die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()'.$code,1100));
	}


	function get_info_by_uuid()
	{
		$ret_arr=array();
		api_header();

		$code=v('scode');
// SELECT `code`, `is_done`, `verkey`, `title`, `ud1`, `ud2`, `ud3`, `ud4`, `ud5`, `ud6`, `ud7`, `ud8`, `ud9`, `cf1`, `cf2`, `majorInfo`, `sch_id` FROM `school_info` WHERE 1
		if(!empty($code) && strlen($code)>=30){
			$tmp_sql=prepare("SELECT `code`, `title`, `ud1`, `ud2`, `ud3`, `ud4`, `ud6` FROM `school_info` WHERE `code` =?s limit 1 ",array($code));
			
			if($ret_sql = get_data($tmp_sql )){
				die(api_result_json($ret_sql,'sql ok!'));
			}else if($ret_sql===false){
				die(api_result_json($ret_arr,'没有符合条件的数据！',120));
			}
		}
			
		die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()'.$code,1100));
	}



	function api_major_info()
	{
		$ret_arr=array();
		api_header();

		$code=v('scode');
    	if(!empty($code) && strlen($code)==32 ){
	    	$tmp_sql = prepare('SELECT `code`, `title` FROM `school_major` WHERE `ud1`=?s  order by `title` limit 500' ,array($code));
	        // if($debugMode) $debug_src.='major sql:'.$tmp_sql;
	        
	        if($ret_sql = get_data($tmp_sql )){
				die(api_result_json($ret_sql,'sql ok!'));
			}else if($ret_sql===false){
				die(api_result_json($ret_arr,'没有符合条件的数据！',120));
			}
    	}
	
		die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()'.$code,1100));
	}

	function get_enrollment_directory_major_list()
	{
		$ret_arr=array();
		api_header();

		$code=v('scode');
    	if(!empty($code) && strlen($code)==32 ){
	    	// $tmp_sql = prepare('SELECT * FROM `enrollment_directory` WHERE `sch_code`=?s and maj_title<>'' order by `id` limit 500' ,array($code));
	    	// SELECT m.code as uuid,d.maj_title as title, d.maj_cat_code, d.pici FROM enrollment_directory d INNER JOIN school_major m on d.sch_code=m.ud1 and d.maj_title=m.title and d.sch_code='004d3f1dbd0ac01e26d15dc2092d9038'

	    	$tmp_sql = prepare('SELECT m.code as `code`,d.maj_title as title, d.maj_cat_code, d.pici FROM enrollment_directory d INNER JOIN school_major m on d.sch_code=m.ud1 and d.maj_title=m.title and d.sch_code=?s limit 500' ,array($code));

	        // if($debugMode) $debug_src.='major sql:'.$tmp_sql;
	        
	        if($ret_sql = get_data($tmp_sql )){
				die(api_result_json($ret_sql,'sql ok!'));
			}else if($ret_sql===false){
				die(api_result_json($ret_arr,'没有符合条件的数据！',120));
			}
    	}
	
		die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()'.$code,1100));
	}


	function ext_info()
	{
		$ret_arr=array();
		api_header();
		$code=v('scode');
		// SELECT `code`, `title`, `ud1`, `ud2`, `ud3`, `ud4`, `ud5`, `ud6`, `ud7`, `ud8`, `ud9`, `xiaoqu`, `yinshi`, `zhusu`, `wangluo`, `xueshu`, `chuangye`, `kaoyan`, `kaogongwuyuan`, `chuguo`, `jiuye`, `xuefei`, `jiaotong`, `zhuanyejicha` FROM `school_ruanyingjian` WHERE 1
// ud1		ud2			ud3			ud4			ud5			ud6			ud7			ud8			ud9
// 所在地区	院校性质	院校隶属	学历层次	院校特色	院校类型	本科入学	咨询电话	招生网站

		if(!empty($code) && strlen($code)==32){
			$tmp_sql=prepare("SELECT `code`, `title`, `ud1`, `ud2`, `ud3`, `ud4`, `ud5`, `ud6`, `ud7`, `ud8`, `ud9` FROM `school_ruanyingjian` WHERE `code` =?s limit 1 ",array($code));
			
			if($ret_sql = get_data($tmp_sql )){
				die(api_result_json($ret_sql,'sql ok!'));
			}else if($ret_sql===false){
				die(api_result_json($ret_arr,'没有符合条件的数据！',120));
			}
		}
			
		die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()'.$code,1100));
	}



}


?>