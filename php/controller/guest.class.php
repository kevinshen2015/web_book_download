<?php
if( !defined('IN') ) die('bad request');
include_once( AROOT . 'controller'.DS.'app.class.php' );
include_once APP_ROOT . DS .'ext'.DS.'sms'.DS.'sms.inc.php';

class guestController extends appController
{
	function __construct()
	{
		parent::__construct();
	}
	
	function index()
	{
		// if( is_mobile_request() ) return forward( 'client/' );
		if( is_login() ) return forward( '?c=dashboard' );
		
		// do login
		$data['title'] = $data['top_title'] = __('LOGIN_PAGE_TITLE');
		$data['css'][] = 'login_screen.css';

		$data['langs'] = @glob( AROOT . 'local/*.lang.php'  );

		$data['outVar']=array();
		$data['outVar']['email']=v('email');
		$data['outVar']['password']=v('password');
		$data['html_data']= '';

		return render( $data , 'web' , 'default' );
	}

	function reg()
	{
		// if( is_mobile_request() ) return forward( 'client/' );
		if( is_login() ) return forward( '?c=dashboard' );
		
		// do login
		$data['title'] = $data['top_title'] = '注册';

		$body = '';

		$body .= ' 很高兴在这里看到你！<br/>';
		$body .= '<br/><br/>'. '在线注册功能还没开始做 :) ，请在官方贴吧报名帖报名!  <br/>';
		$tt = 'http://tieba.baidu.com/p/3625670235';
		$body .= '<br/><br/><a href="'.$tt.'" target="new">官方贴吧报名帖'.$tt.'</a>';

		$data['html_data']= $body;
		return render( $data , 'web' , 'base' );
	}

	function i18n()
	{
		@session_write_close(); 
		$c = z(t(v('lang')));

		if( strlen($c) < 1 )
		{
			$c = c('default_language');
			if( strlen($c) < 1 ) $c = 'zh_cn';	
		}
		
		if( !isset(  $GLOBALS['language'][$c] ) )
		{
			$lang_file = AROOT . 'local' . DS . basename($c) . '.lang.php';
			if( file_exists( $lang_file ) )
				include_once( $lang_file );
		}

		$data['js_items'] = js_i18n( $GLOBALS['language'][$c] );

		return render( $data , 'ajax' , 'js' );

	}
	
	function login()
	{
		if( $user = login( v('email') , v('password') ) )
		{			
			foreach( $user as $key => $value ){
				$_SESSION[$key] = $value;
			}

			$disp_name = $_SESSION['uname']  .' ( ' . safe_email($_SESSION['email']) .' ) ';
			$_SESSION['dispname'] = $disp_name;
			update_user_online();

			return ajax_echo( __('LOGIN_OK_NOTICE') .jsforword('?c=dashboard'));

		}elseif( $user === null )
		{
			return ajax_echo( __('API_CONNECT_ERROR_NOTICE') );
		}
		else
		{
			return ajax_echo( __('LOGIN_BAD_ARGS_NOTICE') );
		}
	}
	
	function login2()
	{
		$tt = array();
		if( $user = login( v('email') , v('password') ) )
		{
			foreach( $user as $key => $value ){
				$_SESSION[$key] = $value;
			}	

			update_user_online();

			foreach( $user as $key => $value ){
				$tt[$key] = $value;
			}
			$tt['message']=__('LOGIN_OK_NOTICE');
			$tt['login_time']=date('Y-m-d H:i:s');
	
			if(isset($tt['alert']) && isset($login_bonus['msg']))
				$tt['alert'].=$login_bonus['msg'];

			return ajax_echo(json_encode($tt));

		}elseif( $user === null )
		{
			$tt['message']=__('API_CONNECT_ERROR_NOTICE');
			return ajax_echo(json_encode($tt));
		}
		else
		{
			$tt['message']=__('LOGIN_BAD_ARGS_NOTICE');
			return ajax_echo(json_encode($tt));
		}
	}

	function reg2()
	{
		$tt = array('err_code'=>100,'message'=>'UNKNOW!');
		if( $user = pc_reg( v('email') , v('password') ) )
		{
			if(isset($user['err_code']) && intval($user['err_code'])>0){
				$tt['message']=$user['msg'];			
			}else{
				$tt['message']='注册成功！';
				$tt['err_code']=0;
				// $tt['login_time']=date('Y-m-d H:i:s');	
			}
			return ajax_echo(json_encode($tt));

		}elseif( $user === null )
		{
			$tt['message']=__('API_CONNECT_ERROR_NOTICE');
			return ajax_echo(json_encode($tt));
		
		}else
		{
			$tt['message']=__('LOGIN_BAD_ARGS_NOTICE');
			return ajax_echo(json_encode($tt));
		}
	}

	function logout()
	{
		foreach( $_SESSION as $key=>$value )
		{
			unset( $_SESSION[$key] );
		}
		
		forward('?c=guest');
	}

	function install()
	{
		if( is_installed() )
			return info_page( __('INSTALL_FINISHED') );
		elseif( intval(v('do')) == 1 )
		{
			db_init();			
		}
		else
		{
			$data['title'] = $data['top_title'] =  __('INSTALL_PAGE_TITLE') ;
			$data['outVar']=array();
			$data['outVar']['phpversion_lt_520']= version_compare(PHP_VERSION, '5.2.0' , '<')  ?' class="bad" ':'';
			$data['outVar']['phpversion']=phpversion();

			$data['outVar']['api_server_ck']='';
			if(   strtolower(str_replace('//' , '/' , $_SERVER['PHP_SELF'] )) != strtolower(str_replace('http://'.$GLOBALS['config']['site_domain'] , '' , c('api_server'))  ) ) {
				$data['outVar']['api_server_ck']= ' class="bad" '; 
			}

			$data['outVar']['api_server_url']='http://'.c('site_domain').$_SERVER['PHP_SELF'];
			$data['outVar']['api_server_conf']=c('api_server');

			return render( $data , 'web' , 'guest.install' );
		}
	}

/**

**/

	function school_search($search_opt=null)
	{
		// global $limit,$offset,$page; 
		$ret_arr=array();
		api_header();

		init_page_start_limit();

		// $sql_str_arr=array('`min_score`>?i');
		$sql_str_arr=array('?i');
		$sql_val_arr=array(1);
		$opt=array();

		if($search_opt===null){
			$json = v('key') ;

			if(empty($json) || strlen($json)<5){
				die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()'.$json,1100));
			}
		
			$tt = v('mod') ;
			if(!empty($tt)){
				$tt=intval($tt);
				if($tt>0){
					$opt['mode']=$tt;
				}
			}
		}else{
			if(isset($search_opt['mode'])){
				$opt['mode']=intval($search_opt['mode']);
			}
			if(isset($search_opt['json'])){
				$json=$search_opt['json'];
			}
		}

		// SELECT `code`, `is_done`, `verkey`, `title`, `ud1`, `ud2`, `ud3`, `ud4`, `ud5`, `ud6`, `ud7`, `ud8`, `ud9`, `cf1`, `cf2`, `majorInfo`, `sch_id`, `min_score`, `min_score1`, `min_score2`, `is_subject_cat1`, `is_subject_cat2` FROM `school_info` WHERE 1
		    
		// echo print_r($json);
		$json = urldecode($json);
		// echo print_r($json);
		$search_key=json_decode($json,true);
		// echo print_r($search_key);	die();
/*
Array
(
    [cat5] => 
    [evaluation_score] => 0
    [cat2min] => 300
    [cat2max] => 800
    [province] => 非本地
    [zysy_cat2] => 985
    [zysy_cat3] => 民办 
    [xuefei] => 5千-1万元 

    [mode] => 1
    [ajax] => js
    [scode] => 32a50a77422ad2a4b4bfd21f02a7066d
    [tiqian] => 提前录
    [zysy_major_cat] => 1001
    [zysy_major_sub_cat] => 01010300
    [zysy_subject_cat] => 01
)
*/

		$opt['result_mode']='base';
		
		foreach ($search_key as $key => $value) {
		    $tmpk=strtolower($key);
		    $value=trim($value);
		    switch ($tmpk) {

		      case 'cat4':				//批次	
		      case 'tiqian':			//[tiqian] => 提前录
		        if($value!='全部'){
		        	//`enrollment_directory`
		        	$opt['pici']=$value;	        	
		        	$opt['result_mode']='zysy_sch_ext_sql';
		        }
		        break;



		      case 'zysy_major_cat':		//专业类别
		      case 'zysy_major_sub_cat':	//专业性质
		      case 'zysy_subject_cat':		//报考科目
				$opt['result_mode']='zysy_maj_sql';
		      	break;

			  case 'xuefei':		//学费
		        if($value!='全部'){
		        	if($value=="&lt;5千元"){
		        		$sql_str_arr[]='b.xuefei < ?i';
			          	$sql_val_arr[]='5000';			        	
					}elseif($value=="&gt;1万元"){
						$sql_str_arr[]='b.xuefei > ?i';
			          	$sql_val_arr[]=10000;
					}else{
						$sql_str_arr[]='b.xuefei > ?i';
			          	$sql_val_arr[]=5000;
					}
					$opt['result_mode']='zysy_maj_sql';
		        }
		        break;



		      case 'province':			//所在地区
		        if($value!='全部'){
		        	if($value=='本地'){
		        		$value='%上海%';
		        		$sql_str_arr[]='`ud2` like ?s';
		          		$sql_val_arr[]=$value; 
		        	}else if($value=='非本地'){
		        		$value='%上海%';
		        		$sql_str_arr[]='`ud2` not like ?s';
		          		$sql_val_arr[]=$value;  
		        	}else {
		        		$sql_str_arr[]='`ud2`=?s';
		          		$sql_val_arr[]=$value;
		        	}
		        }
		        break;

			  case 'zysy_cat2':		//院校特色
		        if($value!='全部'){
		        	$sql_str_arr[]='tese like ?s';
		          	$sql_val_arr[]='%'.$value.'%';
		        }
		        break;

		      case 'zysy_cat3':		//办学性质
		        if($value!='全部'){
		        	$sql_str_arr[]='xingzhi like ?s';
		          	$sql_val_arr[]='%'.$value.'%';
		        }
		        break;

		      case 'cat2min':			//去年分数线
		        $tmpInt=intval($value);
		        if($tmpInt>0){
		          $sql_str_arr[]='`min_score` >= ?i';
		          $sql_val_arr[]=$tmpInt;
		          $opt['cat2']=1;
		        }
		        break;
		      case 'cat2max':			//去年分数线
		        $tmpInt=intval($value);
		        if($tmpInt>0){
		          $sql_str_arr[]='`min_score` <= ?i';
		          $sql_val_arr[]=$tmpInt;
		          $opt['cat2']=1;
		        }
		        break;




		      case 'cat1':
		        if($value!='全部'){
		          $sql_str_arr[]='`ud1` like ?s';
		          $sql_val_arr[]=$value.'院校';
		        }
		        break;

		      case 'sousuokk':
		      	if(!empty($value)) {
					$sql_str_arr[]='`title` like ?s';
		        	$sql_val_arr[]='%'.$value.'%';
		      	}
		        break;

		      case 'cat5':
		        if($value=='文科'){
		          $sql_str_arr[]='`is_subject_cat1` =?i';
		          $sql_val_arr[]=1;
		          $opt['cat5']=1;
		        }else if($value=='理科'){
		          $sql_str_arr[]='`is_subject_cat2` =?i';
		          $sql_val_arr[]=1;
		          $opt['cat5']=2;
		        }
		        break;
		        
		    }
		}


		//fix 分文理科查分数线
		if (isset($opt['cat5'])) {
			foreach ($sql_str_arr as $key => $value) {
				if(stripos($value, '`min_score`')!==false){
					$sql_str_arr[$key]=str_ireplace('`min_score`', '`min_score'.$opt['cat5'].'`', $value);
				}
			}
		}

		// echo 'DEBUG!'.__METHOD__.'()'.print_r($sql_str_arr);
		// echo 'DEBUG!'.__METHOD__.'()'.print_r($sql_val_arr);
		// echo 'DEBUG!'.__METHOD__.'()'.print_r($opt);
		echo $this->school_search_real($sql_str_arr,$sql_val_arr,$opt);
		die();

	}


	private function school_search_real($sql_str_arr,$sql_val_arr,$opt=null){
	    global $debugMode, $time_log , $c,$a, $outVar;
	    global $db_table_prefix;
		global $limit,$offset;

		$ret_arr=array();
		if(empty($sql_str_arr) || !is_array($sql_str_arr)){
			die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()',1100));
		}
		$debugMode=true;
		$debug_log='';
		$html_src='';
		$table_name = 'school_info';

		//$opt['result_mode']='zysy_maj_sql';
		if(isset($opt['result_mode'])  && !empty($opt['result_mode'])){
			switch ($opt['result_mode']) {
				case 'zysy_maj_sql':
					// echo 'DEBUG:'.__METHOD__.'() zysy_maj_sql!'.PHP_EOL;
					# code...
					break;

				case 'zysy_sch_ext_sql':
					// echo 'DEBUG:'.__METHOD__.'() zysy_sch_ext_sql!'.PHP_EOL;
					// return $this->school_search_zhiyuan($sql_str_arr,$sql_val_arr,$opt);
					break;
				
				default:	//base search
					// echo 'DEBUG:'.__METHOD__.'() base search!'.PHP_EOL;
					$where='WHERE '.prepare(m_implode(' AND ', $sql_str_arr) ,$sql_val_arr);
					$where.=' ORDER BY min_score desc ';
					if(isset($opt['mode']) && $opt['mode']==2){
						$fields_str='`code`, `title`,`min_score`, `ud6`';
						$offset=5;
					}else{
						
						$fields_str='`code`, `title`,`min_score`, `ud1`, `ud2`, `ud3`, `ud6`,`is_subject_cat1` as c1 ,`is_subject_cat2` as c2,`min_score1` as s1,`min_score2` as s2, `remarks`';
						$offset=20;
					}
					// die($where);
					$table_name = 'school_info';
					fix_page_limit();
					$ret_page=get_HP_page_result($table_name,'code',$where,$limit,$offset,$fields_str);

					$tmpPageArr=array();
					$tmpPageArr['count']=count($ret_page);
					$tmpPageArr['data']=$$ret_page;
					$page_div=get_pageview($tmpPageArr,'','zysy_gotopage');
					$ret_page['page_div']=$page_div;

					return json_encode($ret_page);
					break;
			}
		}

/*	  
SELECT * 
FROM  `school_info` a
JOIN  `enrollment_directory` b ON ( a.code = b.sch_code ) 
WHERE  a.code='4e39f9f3637369816a7269ca33f08383' and b.maj_title=''
LIMIT 0 , 30
*/

		// echo 'DEBUG:'.__METHOD__.'() last part!'.PHP_EOL;

		$tmpArr=array();
		foreach ($sql_str_arr as $v) {
			$tt = ' '.$v;
			$tmp = str_ireplace(' `', 'a.', $tt);
			$tmp = str_ireplace('`', '', $tmp);
			$tmpArr[]=$tmp;
		}
		$where=prepare(m_implode(' AND ', $tmpArr) ,$sql_val_arr);
		$where .= " AND b.maj_title='' ";
		// die($where);

		if(isset($opt['pici']) && !empty($opt['pici'])){
			$value=$opt['pici'];
			$where .= " AND " . prepare(" b.pici like ?s " ,array('%'.$value.'%'));
		}

		$count_sql = 'SELECT count(*) as count FROM `school_info` a inner JOIN  `enrollment_directory` b ON ( a.code = b.sch_code ) WHERE '.$where .' LIMIT 1 ';
		if($debugMode) $debug_log.='count_sql:'.$count_sql;
		$count_ret=get_var($count_sql);
		if($count_ret===false){
			die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()'.$count_sql,1200));
		}

		if($count_ret==0){
			die(api_result_json($ret_arr,'没有找到符合检索条件的数据！ #debug message:'.__METHOD__.'()'.$count_sql,1210));
		}

		$where.=' ORDER BY min_score desc ';
		fix_page_limit();

		$tmp_sql = 'SELECT a.code, a.title,a.min_score, a.ud1, a.ud2, a.ud3, a.ud6,a.is_subject_cat1 as c1 ,a.is_subject_cat2 as c2,a.min_score1 as s1,a.min_score2 as s2, a.remarks,b.pici FROM `school_info` a inner JOIN  `enrollment_directory` b ON ( a.code = b.sch_code ) WHERE '.$where .' LIMIT '.intval($limit).', '.intval($offset);
		if($debugMode) $debug_log.='sql:'.$tmp_sql;
		// die($tmp_sql);

		$outVar['html_fanye1']='复杂搜索只显示搜索结果的第一页！';
		$ret_sql=get_data($tmp_sql);
		if($ret_sql!==false){
			$tmpPageArr=array();
			$tmpPageArr['count']=count($ret_sql);
			$tmpPageArr['data']=$ret_sql;
			$page_div=get_pageview($tmpPageArr,'','zysy_gotopage');

			die(api_result_json($ret_sql,'sql ok!'.$debug_log,0,$count_ret,$page_div));
		}
		
		die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()'.$tmp_sql,1100));
   
	}

	private function school_search_zhiyuan_0517del($sql_str_arr,$sql_val_arr,$opt){
		die('error!'.__METHOD__.'()');
		$debugMode=true;
		$debugMsg='';
		if($debugMode) $debugMsg .= __METHOD__.'()';
		$tmpArr=array();
		foreach ($sql_str_arr as $v) {
			$tt = ' '.$v;
			if(stripos($tt, 'b.')===false){
				$tmp = str_ireplace(' `', 'a.', $tt);
				$tmp = str_ireplace('`', '', $tmp);
			}else{
				$tmp = $tt;
			}
			$tmpArr[]=$tmp;
		}
		$where=prepare(m_implode(' AND ', $tmpArr) ,$sql_val_arr);
		
		$count_sql = 'SELECT count(*) as count FROM `school_info` a inner JOIN `school_ruanyingjian`  b ON ( a.code = b.code ) WHERE '.$where .' LIMIT 1 ';
		$count_ret=get_var($count_sql);
		if($debugMode) $debugMsg .= 'count_sql='.$count_sql;
		if($count_ret===false){
			die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()'.$debugMsg,1200));
		}

		if($count_ret==0){
			die(api_result_json($ret_arr,'没有找到符合检索条件的数据！'.$debugMsg,1210));
		}

		$where.=' ORDER BY min_score desc ';

		$tmp_sql = 'SELECT a.code, a.title,a.min_score, a.ud1, a.ud2, a.ud3, a.ud6,a.is_subject_cat1 as c1 ,a.is_subject_cat2 as c2,a.min_score1 as s1,a.min_score2 as s2,b.ud2 as xingzhi,b.ud5 as tese FROM `school_info` a inner JOIN  `school_ruanyingjian` b ON ( a.code = b.code ) WHERE '.$where .' LIMIT 0 , 15';
		if($debugMode) $debugMsg.='sql:'.$tmp_sql;
		// die($tmp_sql);
		$outVar['html_fanye1']='复杂搜索只显示搜索结果的第一页！';

		$ret_sql=get_data($tmp_sql);
		if($ret_sql!==false){
			die(api_result_json($ret_sql,'sql ok!'.$debug_log));
		}
		
		die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()'.$debugMsg,1100));
	}


/**


**/

	function zysy_maj_search(){
		$ret_arr=array();
		api_header();
/*
Array
(
    [scode] => 32a50a77422ad2a4b4bfd21f02a7066d

    [zysy_major_cat] => 1001
    [zysy_major_sub_cat] => 01010300
    [zysy_subject_cat] => 01
)
*/
		$opt=array();
		$sch_code=v('scode');
		if(empty($sch_code) ){
			die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()',1100));
		}
		$zysy_major_cat = v('zysy_major_cat') ;
		$zysy_major_sub_cat = v('zysy_major_sub_cat') ;
		$zysy_subject_cat = v('zysy_subject_cat') ;
		$xuefei = v('xuefei') ;
		
		$sql_str_arr=array('`sch_code`=?s');
		$sql_val_arr=array($sch_code);

		$sql_str_arr[]='`maj_title`<>?s';
		$sql_val_arr[]='';

/*
		if(!empty($zysy_major_sub_cat)){	//专业性质
			// $tmp_sn=substr($zysy_major_sub_cat, 0,4);
		      	
			$sql_str_arr[]="`maj_cat_code` = ?s ";
			$sql_val_arr[]=trim($zysy_major_sub_cat);
				
		}else if(!empty($zysy_major_cat)){	//专业类别
			$tmp_sn=substr($zysy_major_cat, 2);
	      	$sql_str_arr[]="`maj_cat_code` like ?s ";
			$sql_val_arr[]=$tmp_sn.'%';
		}
*/
		if(!empty($zysy_subject_cat)){		//报考科目
			if($zysy_subject_cat!='0'){
				$sql_str_arr[]='`subject_group` like ?s';
				$sql_val_arr[]='%'.$zysy_subject_cat.'%';
	        }
		}

		if(!empty($xuefei)){				//学费
			$value=$xuefei;
			if($value!='全部'){
	        	if($value=="&lt;5千元"){
	        		$sql_str_arr[]='`charge_std` < ?i';
		          	$sql_val_arr[]=5000;
				}elseif($value=="&gt;1万元"){
					$sql_str_arr[]='( `charge_std`=0 or `charge_std` > ?i )';
		          	$sql_val_arr[]=10000;
				}else{
					// $sql_str_arr[]='`charge_std` > ?i';
					$sql_str_arr[]='( `charge_std`=0 or `charge_std` > ?i )';
		          	$sql_val_arr[]=5000;
				}
			}
		}

		// echo 'DEBUG!'.__METHOD__.'()'.print_r($sql_str_arr);
		// echo 'DEBUG!'.__METHOD__.'()'.print_r($sql_val_arr);
		// echo 'DEBUG!'.__METHOD__.'()'.print_r($opt);
		echo $this->zysy_maj_search_real($sql_str_arr,$sql_val_arr,$opt);
		die();

	}


	private function zysy_maj_search_real($sql_str_arr,$sql_val_arr,$opt=null){
	    global $debugMode, $time_log , $c,$a, $outVar;
	    global $db_table_prefix;
		global $limit,$offset;

		$ret_arr=array();
		if(empty($sql_str_arr) || !is_array($sql_str_arr)){
			// die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()',1100));
		}
		$debugMode=true;
		$debug_log='';
		$html_src='';
	
		$where=prepare(m_implode(' AND ', $sql_str_arr) ,$sql_val_arr);

		$count_sql = 'SELECT count(*) as count FROM `enrollment_directory` WHERE '.$where .' LIMIT 1 ';
		if($debugMode) $debug_log.='count_sql:'.$count_sql;
		$count_ret=get_var($count_sql);
		if($count_ret===false){
			die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()'.$count_sql,1200));
		}

		if($count_ret===0){
			die(api_result_json($ret_arr,'没有找到符合检索条件的数据！ #debug message:'.__METHOD__.'()'.$count_sql,1210));
		}

		// $where.=' ORDER BY min_score desc ';

		// SELECT `id`, `sch_code`, `sch_sn`, `sch_title`, `pici`, `x2`, `subject_group`, `maj_sn`, `maj_title`, `length_of_schooling`, `plan_num`, `charge_std`, `accommodation_std`, `foreign_language`, `is_oral_test`, `remarks`, `maj_cat_code` FROM `enrollment_directory` WHERE 1



		$tmp_sql = 'SELECT * FROM `enrollment_directory` WHERE '.$where .' LIMIT 0, 300';
		if($debugMode) $debug_log.='sql:'.$tmp_sql;
		// die($tmp_sql);

		$outVar['html_fanye1']='复杂搜索只显示搜索结果的第一页！';
		$ret_sql=get_data($tmp_sql);
		if($ret_sql!==false){
			// $tmpPageArr=array();
			// $tmpPageArr['count']=count($ret_sql);
			// $tmpPageArr['data']=$ret_sql;
			// $page_div=get_pageview($tmpPageArr,'','zysy_gotopage');
			$page_div='';

			die(api_result_json($ret_sql,'sql ok!'.$debug_log,0,$count_ret,$page_div));
		}
		
		die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()'.$tmp_sql,110));
   
	}

	/**


	**/

	private function now_time(){
		$cf_now = date('Y-m-d H:i:s');
		return $cf_now;
	}	

	function test_cache(){
		$ret_arr=array();
		api_header();

		$key=v('key');
		$val=v('val');
		if($flag=set_redis($key,$val)){
			echo $this->now_time().' '.__METHOD__.'() set_redis ok!'.PHP_EOL;

			if($ret_val=get_redis($key)){				
				echo $this->now_time().' '.__METHOD__.'() get_redis ok!'.PHP_EOL;
				if($ret_val==$val){
					echo $this->now_time().' '.__METHOD__.'() get_redis ok #2!'.PHP_EOL;
				}
			}

		}else{
			echo ''.__METHOD__.'() set_redis Error!'.PHP_EOL;
		}
		die(PHP_EOL.__METHOD__.'() Process End!');
	}

	function zysy_maj_batch_search(){
		$ret_arr=array();
		api_header();
/*
Array
(
    [scode] => code1;code2;...

    [zysy_major_cat] => 1001
    [zysy_major_sub_cat] => 01010300
    [zysy_subject_cat] => 01
)
*/
		$opt=array();
		$sch_code=v('scode');
		if(empty($sch_code) ){
			die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()',1100));
		}
		$zysy_major_cat = v('zysy_major_cat') ;
		$zysy_major_sub_cat = v('zysy_major_sub_cat') ;
		$zysy_subject_cat = v('zysy_subject_cat') ;
		$xuefei = v('xuefei') ;
		
		// $sql_str_arr=array('`sch_code`=?s');
		// $sql_val_arr=array($sch_code);
		$sql_str_arr=array();
		$sql_val_arr=array();

		if(stripos($sch_code,';')==false){
			die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()'.$sch_code,1101));
		}
		$scode_arr=explode(';', $sch_code);
		// $tmp_str='`sch_code` in (?s,?s)';
		$tmp2str_arr=array();
		$tmp2val_arr=array();
		foreach ($scode_arr as $tmp_scode) {
			if(!empty($tmp_scode)){
				$tmp2str_arr[]='?s';
				$tmp2val_arr[]=$tmp_scode;
			}
		}

		$sql_str_arr[]='`maj_title`<>?s';
		$sql_val_arr[]='';
/*
		if(!empty($zysy_major_sub_cat)){	//专业性质
			if($zysy_major_sub_cat=='all' || $zysy_major_sub_cat=='1000' || $zysy_major_sub_cat=='全部'){
				$zysy_major_sub_cat=null;
			}
		}

		if(!empty($zysy_major_sub_cat)){	//专业性质
			// $tmp_sn=substr($zysy_major_sub_cat, 0,4);		      	
			$sql_str_arr[]="`maj_cat_code` = ?s ";
			$sql_val_arr[]=trim($zysy_major_sub_cat);
				
		}else if(!empty($zysy_major_cat)){	//专业类别
			$tmp_sn=substr($zysy_major_cat, 2);
	      	$sql_str_arr[]="`maj_cat_code` like ?s ";
			$sql_val_arr[]=$tmp_sn.'%';
		}
*/
		if(!empty($zysy_subject_cat)){		//报考科目
			if($zysy_subject_cat!='0'){
				$sql_str_arr[]='`subject_group` like ?s';
				$sql_val_arr[]='%'.$zysy_subject_cat.'%';
	        }
		}

		if(!empty($xuefei)){				//学费
			$value=$xuefei;
			if($value!='全部'){
	        	if($value=="&lt;5千元"){
	        		$sql_str_arr[]='`charge_std` < ?i';
		          	$sql_val_arr[]=5000;
				}elseif($value=="&gt;1万元"){
					$sql_str_arr[]='( `charge_std`=0 or `charge_std` > ?i )';
		          	$sql_val_arr[]=10000;
				}else{
					// $sql_str_arr[]='`charge_std` > ?i';
					$sql_str_arr[]='( `charge_std`=0 or `charge_std` > ?i )';
		          	$sql_val_arr[]=5000;
				}
			}
		}

		// echo 'DEBUG!'.__METHOD__.'()'.print_r($sql_str_arr);
		// echo 'DEBUG!'.__METHOD__.'()'.print_r($sql_val_arr);
		// echo 'DEBUG!'.__METHOD__.'()'.print_r($opt);
		echo $this->zysy_maj_batch_search_real($sql_str_arr,$sql_val_arr,$opt);
		die();

	}


	private function zysy_maj_batch_search_real($sql_str_arr,$sql_val_arr,$opt=null){
	    global $debugMode, $time_log , $c,$a, $outVar;
	    global $db_table_prefix;
		global $limit,$offset;

		$ret_arr=array();
		if(empty($sql_str_arr) || !is_array($sql_str_arr)){
			// die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()',1100));
		}
		$debugMode=true;
		$debug_log='';
		$html_src='';
	
		$where=prepare(m_implode(' AND ', $sql_str_arr) ,$sql_val_arr);

		$count_sql = 'SELECT count(*) as count FROM `enrollment_directory` WHERE '.$where .' LIMIT 1 ';
		if($debugMode) $debug_log.='count_sql:'.$count_sql;
		$count_ret=get_var($count_sql);
		if($count_ret===false){
			die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()'.$count_sql,1200));
		}

		if($count_ret===0){
			die(api_result_json($ret_arr,'没有找到符合检索条件的数据！ #debug message:'.__METHOD__.'()'.$count_sql,1210));
		}

		// $where.=' ORDER BY min_score desc ';

		// SELECT `id`, `sch_code`, `sch_sn`, `sch_title`, `pici`, `x2`, `subject_group`, `maj_sn`, `maj_title`, `length_of_schooling`, `plan_num`, `charge_std`, `accommodation_std`, `foreign_language`, `is_oral_test`, `remarks`, `maj_cat_code` FROM `enrollment_directory` WHERE 1



		$tmp_sql = 'SELECT * FROM `enrollment_directory` WHERE '.$where .' LIMIT 0, 300';
		if($debugMode) $debug_log.='sql:'.$tmp_sql;
		// die($tmp_sql);

		$outVar['html_fanye1']='复杂搜索只显示搜索结果的第一页！';
		$ret_sql=get_data($tmp_sql);
		if($ret_sql!==false){
			// $tmpPageArr=array();
			// $tmpPageArr['count']=count($ret_sql);
			// $tmpPageArr['data']=$ret_sql;
			// $page_div=get_pageview($tmpPageArr,'','zysy_gotopage');
			$page_div='';

			die(api_result_json($ret_sql,'sql ok!'.$debug_log,0,$count_ret,$page_div));
		}
		
		die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()'.$tmp_sql,1100));
   
	}

	/**


	**/

	function major_search()
	{
		$ret_arr=array();
		api_header();

		$mode = intval(v('mod'));
		$search_key = v('key') ;
		switch ($mode) {
			case '1':
				$this->api_major_list($search_key);
				break;

			case '2':
				if(empty($search_key) || strlen($search_key)<2){
					die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()'.$search_key,1100));
				}
				$this->major_basic_search($search_key);
				break;
			
			default:
				die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()'.$mode,1100));
				break;
		}
	}

	private function major_basic_search($search_key=null,$limit=5)
	{
		$ret_arr=array();
		api_header();

		if($search_key===null){
			$json = v('key') ;

			if(empty($json) || strlen($json)<2){
				die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()'.$json,1100));
			}
			$search_key=$json;
		}
	
		// SELECT `code`, `is_done`, `verkey`, `title`, `ud1`, `ud2`, `ud3`, `ud4`, `ud5`, `ud6`, `ud7`, `ud8`, `ud9`, `cf1`, `cf2`, `majorInfo`, `sch_id`, `min_score`, `min_score1`, `min_score2`, `is_subject_cat1`, `is_subject_cat2` FROM `school_info` WHERE 1

		$table_name = 'major_config';
		$count_sql = prepare('SELECT count(*) as count FROM `'.$table_name.'` WHERE `title` like ?s limit 1' ,array('%'.$search_key.'%'));
		$count_ret=get_var($count_sql);
		if($count_ret===false){
			die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()'.$count_sql,1200));
		}

		if($count_ret==0){
			die(api_result_json($ret_arr,'没有找到符合检索条件的数据！ #debug message:'.__METHOD__.'()'.$count_sql,1210));
		}
		
		$tmp_sql = prepare('SELECT * FROM `'.$table_name.'` WHERE `title` like ?s limit 15 ' ,array('%'.$search_key.'%'));
	
		$outVar['html_fanye1']='复杂搜索只显示搜索结果的第一页！';

		$ret_sql=get_data($tmp_sql);
		if($ret_sql!==false){
			die(api_result_json($ret_sql,'sql ok!'));
		}		
		die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()'.$tmp_sql,1100));

	}


	private function api_major_list($search_key=null){
	  
	  // major_cat
	  // SELECT `code`, `title` FROM `major_config` WHERE 1
	  // SELECT * FROM  `major_config` WHERE  `code` LIKE '%000000' order by code limit 30

	$sql_str_arr=array('SUBSTRING(`code`,5,2 )!=?s');
	$sql_val_arr=array('00');

	$opt=array();

	$json=$search_key;
	if(!empty($search_key)){
		$sql_str_arr[]='`title` like ?s';
		$sql_val_arr[]='%'.$tmpInt.'%';    
	}

	$tmpInt=intval(v('cat1mc'));
	if($tmpInt>0){
		$cat1mc=substr(''.$tmpInt,2);
		$sql_str_arr[]='`code` like ?s';
		$sql_val_arr[]=$cat1mc.'%';
		$opt['cat1mc']=1;
	}
  	
	$table_name='major_config';
    $where='WHERE '.prepare(m_implode(' AND ', $sql_str_arr) ,$sql_val_arr);
    // die($where);
    $fields_str=' * ';
    $limit=0;
    $offset=15;
    $ret_page=get_HP_page_result($table_name,'code',$where,$limit,$offset,$fields_str);
    // die(json_encode($ret_page));
    if($ret_page['return_code']!=0 || $ret_page['count']==0 ){
    	// echo 'DEBUG!'.__METHOD__.'()'.print_r($ret_page); 
    	die(json_encode($ret_page));
    }

 	if(isset($ret_page['data']) && is_array($ret_page['data']) ){
 		$ret_major_cat =$ret_page['data'];
	    $ret_arr=array();
	    foreach ($ret_major_cat as $key => $value) {
	      $tmp_arr=array();
	      foreach ($value as $k1 => $v1) {
	        $tmp_arr[$k1]=$v1;
	        if($k1=='title'){
	          $tmp2_sql = prepare('SELECT avg_salary5 from major_stat where title=?s limit 1' ,array($v1));
	          $ret2_sql=get_line($tmp2_sql);
	          if($ret2_sql!=false){
	            foreach ($ret2_sql as $k3 => $v3) {
	              if(!isset($tmp_arr[$k3])){
	                $tmp_arr[$k3]=$v3;
	              }
	            }
	          }          
	        // }else if($k1=='code'){          
	          // $k3='top_title';
	          // if(!isset($tmp_arr[$k3])){
	          //   $tmp_ret_arr =get_top3_major_info($value,'&nbsp;'); 
	          //   foreach ($tmp_ret_arr as $k4 => $v4) {
	          //     if($k4==0){
	          //       $tmp_arr[$k3]=$v4['title'];
	          //       $tmp_arr[$k3.'_code']=$v4['code'];
	          //     }else{
	          //       $tmp_arr['top'.$k4.'_title']=$v4['title'];
	          //       $tmp_arr['top'.$k4.'_title_code']=$v4['code'];
	          //     }
	          //   }
	          // }
	        }
	      }
	      // $k3='avg_salary5';
	      // if(!isset($tmp_arr[$k3])){
	      //     $tmp_arr[$k3]='';
	      // }
	      $ret_arr[]=$tmp_arr;
	    }
	    echo api_result_json($ret_arr,'get sql');
	    die();
	  }
	  $ret_arr=array();
	  echo api_result_json($ret_arr,'system error!',110);
	  die();
	     
	}


	function rand_code()
	{
		$ret_arr=array();
		api_header();
		$debugMode=true;
		$tel =v('tel');
		$randCode =v('code');
		$resetPW =false;

		if(empty($tel) || empty($randCode)){
			die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()',1100));
		}
		$tt=v('op');
		if(!empty($tt) && strtolower($tt)=='reset'){
			$resetPW=true;
		}

		@session_start();
	    $token = session_id();

	 //    echo "您刚才输入的是：".$randCode.PHP_EOL;
	 //    echo "<font color=green>".$_SESSION["authnum_session"]."</font><br/>"; 
		// echo PHP_EOL;

		if(isset($_SESSION["authnum_session"])){	
			if(strtolower($randCode)==strtolower($_SESSION["authnum_session"])){
				//判断session值与用户输入的验证码是否一致;

				$randCode=mt_rand(1000,9999);
				// $tmp_uid=substr($tel,3);
				// die(api_result_json($ret_arr,$tmp_uid));

				op_favorites_uuid_rel(0,3,$randCode.'#'.$tel,1);	//$uid,$cat,$uuid,$op

				if($resetPW){
					$debugMsg=red_span('当前是密码重置模式！');	
					$result = sendSMS_web($tel ,$randCode);
				}else{
					$debugMsg=red_span('当前是用户注册模式！');	
					$result = sendSMS($tel ,$randCode);
				}
				$debugMsg.=red_span('<br/>以下为手机短信验证码发送跟踪信息<br/>').$result;	

				if(stripos($result, 'smsMessageSid')!==false){					
					die(api_result_json($ret_arr,'图形验证码通过！手机短信验证码发送成功！'.$debugMsg));

				}else if(stripos($result, 'statusCode')!==false && stripos($result, ':160040<')!==false){
					//error code :160040<br>error msg :【短信】验证码超出同模板同号码天发送上限
					$debugMsg='<br/>可以使用上次收到的手机短信验证码继续注册！<br/>'.$debugMsg;
					die(api_result_json($ret_arr,'图形验证码通过！手机短信验证码发送失败！'.$debugMsg));
				}

				die(api_result_json($ret_arr,'图形验证码通过！手机短信验证码发送失败！'.$result,1102));
			}
		}

		die(api_result_json($ret_arr,'图形验证码无效！'.__METHOD__.'()',1101));
		
	}

	function registernew()
	{
		$ret_arr=array();
		api_header();
		$debugMode=true;
		$validate =v('smsvalidate');
		$email = v('email') ;
		$pw=v('pswd');
		$resetPW =false;

		if(empty($email) || empty($pw) || empty($validate)){
			// die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()',1100));
			die("<font color=red>检索参数无效！</font>"); 
		}

		$tt=v('op');
		if(!empty($tt) && strtolower($tt)=='reset'){
			$resetPW=true;		//密码重置
		}

		@session_start();
	    $token = session_id();
		
		$check=op_favorites_uuid_rel(0,3,$email,3);

		if(isset($check['uuid']) && $check['uuid']==$validate.'#'.$email){
			echo "<font color=green>验证码通过验证!</font><br/>";
			$_SESSION[ 'token' ] = $token;

			$ret_data=pc_reg( $email, $pw);
			$user_info=null;

            if(isset($ret_data['err_code']) && intval($ret_data['err_code'])>0){

            	if($resetPW){		//密码重置
					$user_info=reset_password_by_email($email, $pw);

					if($user_info==false){
						die("<font color=red>密码重置失败！ #1001</font>"); 
					}

					// $_SESSION['uid']
					$_SESSION[ 'uid' ] = $user_info['id'];
					$_SESSION[ 'uname' ] = $email;
					$_SESSION[ 'email' ] = $email;
					$_SESSION[ 'level' ] = 1;
					foreach ($user_info as $k => $v) {
						$_SESSION[ 'u_'.$k ] = $v;
					}

					update_user_online();

					echo "<font color=green>".$email." 密码重置成功！</font><br/>";
					echo jsforword('index.php?a=index');
					die();


            	}

				// $ret_data['msg']='邮件地址已经存在！';
				$tt=$ret_data['msg'];
				if(stripos(v('email'), '@')==false){
					$tt=str_replace('邮件地址', '手机号', $tt);
				}
            	die("<font color=red>注册失败！ ".$tt."</font>"); 
            	// die(api_result_json($ret_arr,'注册失败！ '.$tt.' '.__METHOD__.'()',1001));
            }

          	$user_info=get_full_info_by_email_password2($email, $pw);
			if($user_info==false){
				die("<font color=red>注册失败！ #1001</font>"); 
				// die(api_result_json($ret_arr,'注册失败！ '.__METHOD__.'()',1002));
			}

			// $_SESSION['uid']
			$_SESSION[ 'uid' ] = $user_info['id'];
			$_SESSION[ 'uname' ] = $email;
			$_SESSION[ 'email' ] = $email;
			$_SESSION[ 'level' ] = 1;
			foreach ($user_info as $k => $v) {
				$_SESSION[ 'u_'.$k ] = $v;
			}

			update_user_online();

			// die(api_result_json($ret_arr,'注册成功！'.$email));
			echo "<font color=green>".$email." 注册成功！</font><br/>";
			echo jsforword('index.php?a=index');
			die();
		}

		die("<font color=red>注册失败！ #110</font>"); 
		// die(api_result_json($ret_arr,'检索参数无效！'.__METHOD__.'()',1101));
	}

	function login_new()
	{
		$ret_arr=array();
		api_header();
		$debugMode=true;
		$validate =v('validate');
		$email = v('email') ;
		$pw=v('pswd');
		$ret_src='';

		if(empty($email) || empty($pw) || empty($validate)){
			return ajax_echo("<font color=red>检索参数无效！</font>"); 
		}	

		if(isset($_SESSION["authnum_session"])){
			if(strtolower($validate)!=strtolower($_SESSION["authnum_session"])){
				//判断session值与用户输入的验证码是否一致;
				return ajax_echo("<font color=red>输入有误</font>"); 
			}
		
			// $user_info=get_full_info_by_email_password($email, $pw);
			if( $user_info = login( $email , $pw ) )
			{
				foreach( $user_info as $key => $value ){
					$_SESSION[$key] = $value;
				}

				update_user_online();

				$ret_src ="<font color=green>".$email." 登陆成功！</font><br/>";
				$ret_src .= jsforword('index.php?a=index');
				return ajax_echo($ret_src);

			}elseif( $user_info === null )
			{
				return ajax_echo(__('API_CONNECT_ERROR_NOTICE'));
			}
			else
			{
				return ajax_echo(__('LOGIN_BAD_ARGS_NOTICE'));
			}
		}

		return ajax_echo("<font color=red>登陆失败！ #110</font>"); 
	}

	function dev_md5()
	{
		$ret_arr=array();
		api_header();
		$debugMode=true;
		$validate =v('s');
		$ret_src='';

		if(empty($validate)){
			return ajax_echo("<font color=red>检索参数无效！</font>"); 
		}	
		$ret_src=md5($validate);
		$ret_src = strtolower($ret_src) . ' / '. strtoupper($ret_src);
		return ajax_echo('<font color=red>'.$ret_src.'</font>'); 
	}

/**

**/

	private function make_build_url($fname,$root=''){
		$app_root=$root.'index.php?a=';	
		$ext=mt_rand(1,10000);	
		$ext.='&admin=C0861D93492893B048257E660006B6E7';	
		$ext.='&build_type=build_static_html';
		// $ext.='&build_type=Domino_Server_Page';
		// $run_time=date('Y-m-d H:i:s');
		// $ext.='&st='.$run_time;
		return $app_root.$fname.'&_='.$ext;
	}

	private function iframe_src($tmp_url,$w=1024,$h=768){
		$html_src='<iframe class="rightlist" marginheight="0" marginwidth="0" frameborder="0" scrolling="auto"  height="'.$h.'px" width="'.$w.'px" src="" src1="'.$tmp_url.'"></iframe>'.PHP_EOL;
		return $html_src;
	}

	function build_static_html(){
		api_header();
		echo '<!-- DEBUG: '.__METHOD__.'() -->'.PHP_EOL;
		// die();

		$page=v('page');
		if(empty($page)){
			$html_arr=explode(';', 'index;denglu;zhuce;daxueshouye;zhuanyeshouye;zixun;dingweishouye;zhiyuanshouye');
		}else{
			$html_arr=array($page);
		}

		// $page_url=$this->make_build_url('index');

        $html_tpl='js.tpl.html';
        $outVar['page_title'] = 'Adm.建立静态文件';
		$debug_src='';
		$html_src='';
		// $html_src=$this->iframe_src($page_url);
		foreach ($html_arr as $html_fn) {
			if(!empty($html_fn)){
				$page_url=$this->make_build_url($html_fn);
				$html_src.=PHP_EOL;
				$html_src.=$this->iframe_src($page_url);
			}
		}

		$outVar['html_data']=$html_src;
		if(1){
			$cf_now = date('Y-m-d H:i:s');
			$js='
	var delay=86;

	$(".rightlist").each(function(){
		var $this = $(this);
		setTimeout(function(){
			$this.attr("src",$this.attr("src1"));
        },delay);
        delay +=100;

	});';
			$outVar['js_str']='console.log("'.$cf_now.'");'.$js;
		}
		html_render( $outVar , array('ui_tpl' => $html_tpl,'debug_src'=>$debug_src));

		die(__METHOD__.'() Process End!');
	}

}