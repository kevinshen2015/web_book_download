<?php
if( !defined('IN') ) die('bad request');
include_once( CROOT . 'controller' . DS . 'core.class.php' );

ini_set( 'display_errors' , true );
error_reporting(E_ALL ^ E_NOTICE);

class memberController extends coreController
{
	function __construct()
	{

		// 载入默认的
		parent::__construct();		
		$this->check_login();
				
	}

	function check_login()
	{
		if( !is_login() ) {
			// die('wa hahahah '.__METHOD__.'(DEBUG)');
			$ret_arr=array();
  			die(api_result_json($ret_arr,'你访问的内容，需要先登录！',1001));
		}	
	}
	
	function site_list()
	{
		global $limit,$offset,$page;  
		$ret_arr=array();
		api_header();

		$uid=uid();
		if ($uid==false || $uid==0){
	  		die(api_result_json($ret_arr,'登陆用户无效！'.__METHOD__.'() '.$uid,1001));
		}


		init_page_start_limit();
		fix_page_limit();
		$table_name='shdic_wbd2016_sitesign';
		$where='WHERE '.prepare('`is_his`!=?i' ,array(1));
		$fields_str='`id`, `url_md5` as code, `name`, `url`, `ListStart`, `ListEnd`, `ContentStart`, `ContentEnd`, `NeedDelStr`, `VolumeStart`, `VolumeEnd`, `BriefUrlStart`, `BriefUrlEnd`, `AuthorStart`, `AuthorEnd`, `BriefStart`, `BriefEnd`, `BookImgUrlStart`, `BookImgUrlEnd`, `cf1`, `is_his`';
		$ret_page=get_HP_page_result($table_name,'id',$where,$limit,$offset,$fields_str);

		// get_pageview($ret_page);

		// echo print_r($ret_page);

  		// echo api_result_json($ret_page['data'],'sql ok!');
  		echo json_encode($ret_page);
  		die();

	}

	
}


?>