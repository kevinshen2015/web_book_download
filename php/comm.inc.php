<?php

if( !defined('AROOT') ) die('NO AROOT!');
if( !defined('DS') ) define( 'DS' , DIRECTORY_SEPARATOR );

// define constant
define( 'IN' , true );

function get_sql_table_name(){
    global $sql_table_name;
    $sql_table_name = 'shdic_wbd2016'; 
    return $sql_table_name;
}

$debug_sql_str='';

function init_return_array()
{
	$ret = array('return_code' =>0,'return_message' =>'','total_count'=>0,'datas'=>'null');
	return $ret;
}

function a_title_html($name,$title){
	return '<a href="javascript:void(0);" title="'.$title.'">'.$name.'</a>';
}

function fix_sql_filed($search, $replace, $subject){
	$replace = "'" .$replace. "'";
	return str_ireplace($search, $replace, $subject);
}

function get_hidden_input_value_by_name($name,$html)
{
/*
		<input type="hidden" name="OfferID" value="152606" />
		<input type="hidden" name="InventoryID" value="37233" />
		<input type="hidden" name="CommodityID1" value="44" />
		<input type="hidden" name="CommodityID2" value="5" />
		<input type="hidden" name="Level1" value="1" />
		<input type="hidden" name="Level2" value="1" />
		<input type="hidden" name="PriceAmount" value="1.07784" />
*/
	$ret='';
	$key1='<input type="hidden" name="'. $name. '" value="' ;
	$key2='"';
	$ret = _cut_middle_str($html,$key1,$key2);
	return $ret;
}

function get_next_page_info($loc_url,$html,&$ret)
{
	$ret['page_has_next'] = '0';
	$ret['page_next_url'] = '';	
	$ret['page_total_num'] = '0';
	$ret['new_pagination'] = '';

	$key2='<div class="Pagination">';
	$htm = _cut_middle_str($html,$key2,'<div class="Clear">');
	$ret['new_pagination'] = $htm;

//<span class="Next"><a href="/Markets/Commodities/?InventoryID=37233&amp;Page=6">后页 &gt;</a></span>
//<span class="Next">后页 &gt;</span>
//<span class="Next"><a href="/Markets/Commodities/?InventoryID=37233&Page=3">后页 &gt;</a></span>	
	$htm2 = _cut_middle_str($htm,'<span class="Next">','</span>');
	if(stripos($htm2, 'Page=')!==false){
		$tmp_arr = explode('"', $htm2);
		if(count($tmp_arr)>1){
			$ret['page_has_next'] = '1';
			//http://civitas.soobb.com/Markets/Commodities/?InventoryID=37233&Page=16
			$kk=$tmp_arr[1];
			if(stripos($kk, '&amp;')){
				$kk=str_ireplace('&amp;', '&', $kk);
			}
			$ret['page_next_url'] = 'http://civitas.soobb.com'.$kk;
		// }else{
		// 	$ret['page_next_url'] = $htm2;
		}
		
	}

//<span class="Count">(共 301 条)</span>
	$htm3 = _cut_middle_str($htm,'<span class="Count">','</span>');
	if(stripos($htm2, '条')!==false){
		$tmp_arr = explode(' ', $htm3);
		if(count($tmp_arr)>1){
			$tt = intval($tmp_arr[1]);
			$ret['page_total_num'] = $tt;
		}
		
	}
}

function get_embedded_url($html,$return_count=1)
{
//$$$$$$$$$[http://civitas.soobb.com/Estates/24112/Details/]$$$$$$$$$	
	$ret = '';
	$tmp_key='$$$$$$$$$';
	$tmp_count = intval($return_count);

	$tmp_src = substr($html, 0,200);	//FIXME
	if(stripos($tmp_src, $tmp_key)===false){
		// echo 'DEBUG,' . __FUNCTION__ . '() miss key !'.PHP_EOL;
		return $ret;
	}

	if($tmp_count==1){
		$tmp_url = _cut_middle_str($tmp_src,$tmp_key.'[',']'.$tmp_key);
		if(empty($tmp_url)){
			// echo 'DEBUG,' . __FUNCTION__ . '() _cut_middle_str get empty !'.PHP_EOL;
			return $ret;
		}else{
			// echo 'DEBUG,' . __FUNCTION__ . '() _cut_middle_str get ['.$tmp_url.'] !'.PHP_EOL;
			$ret=$tmp_url;
		}
	}else if($tmp_count>1){
		if($tmp_count>100){
			$tmp_count = 100;
		}
		$ret = array();
		// $tmp_src = substr($html, 0,200*intval($count));	//FIXME
		$tmp_arr = explode($tmp_key, $html);
		$tmp_id = 0;
		foreach ($tmp_arr as $key => $value) {
			$tmp_id++;
			if($tmp_id>$tmp_count){
				break;
			}
			$part_html = $tmp_key.$value.$tmp_key;
			$tmp_url = _cut_middle_str($part_html,$tmp_key.'[',']'.$tmp_key);
			if(empty($tmp_url)){
				$ret[] = $tmp_url;
			}
		}
	}
	
	return $ret;
}

function kis_cmp_intval($var1,$var2){
	if($var1==$var2){
		return 0;
	}else{
		return $var1<$var2 ? -1 : 1;
	}
}

function unescape($str) { //定义unescape函数
	$str = urldecode($str);
	preg_match_all("/(?:%u.{4}|&#x.;|&#d+;|.+)/U",$str,$r);
	$ar = $r[0];
	foreach($ar as $k=>$v) {
		if(substr($v,0,2) == "%u"){
			$ar[$k] = iconv("UCS-2BE","utf-8",pack("H4",substr($v,-4)));
		}elseif(substr($v,0,3) == "&#x"){
			$ar[$k] = iconv("UCS-2BE","utf-8",pack("H4",substr($v,3,-1)));
		}elseif(substr($v,0,2) == "&#"){
			$ar[$k] = iconv("UCS-2BE","utf-8",pack("n",substr($v,2,-1)));
		}
	}
	return join("",$ar);
}

function func_str_compress($string){
	if(strlen($string)<500){
		return $string;
	}
	// echo 'DEBUG,' . __FUNCTION__ . '() Original size:' . strlen($string).PHP_EOL;
	$compressed=gzcompress($string);
	// echo 'DEBUG,' . __FUNCTION__ . '() Compressed size:' . strlen($compressed).PHP_EOL;

	if($compressed!==false){
		// echo 'DEBUG,' . __FUNCTION__ . '() Original str:' . $string.PHP_EOL;
		// echo 'DEBUG,' . __FUNCTION__ . '() Compressed str:' .$compressed.PHP_EOL;
		// $original = gzuncompress($compressed);
		// echo 'DEBUG,' . __FUNCTION__ . '() UN Compressed str:' .$original.PHP_EOL;
		$base64_str = base64_encode($compressed);
		// echo 'DEBUG,' . __FUNCTION__ . '() base64_encode str:' .$base64_str.PHP_EOL;

		if(strlen($string)>strlen($base64_str)){
			return $base64_str;
		}
		
	}
	return $string;
}

function func_str_un_compress($string){
	if(strlen($string)<500){
		return $string;
	}
	// echo 'DEBUG,' . __FUNCTION__ . '() base64_encode size:' . strlen($string).PHP_EOL;
	// echo 'DEBUG,' . __FUNCTION__ . '() base64_encode str:' .$string.PHP_EOL;
	$compressed = base64_decode($string);

	// echo 'DEBUG,' . __FUNCTION__ . '() base64_decode size:' . strlen($compressed).PHP_EOL;
	// echo 'DEBUG,' . __FUNCTION__ . '() base64_decode str:' .$compressed.PHP_EOL;

	if($compressed==false){
		$compressed=$string;
		echo __FUNCTION__.'() skip base64_decode!';
	}
	// 
	// echo 'DEBUG,' . __FUNCTION__ . '() Compressed size:' . strlen($compressed).PHP_EOL;
	// echo 'DEBUG,' . __FUNCTION__ . '() Compressed str:' .$compressed.PHP_EOL;
	$original = gzuncompress($compressed);
	// echo 'DEBUG,' . __FUNCTION__ . '() Original size:' . strlen($original).PHP_EOL;
	// echo 'DEBUG,' . __FUNCTION__ . '() Original str:' . $original.PHP_EOL;
	// mini_debug($original);
	if($original!==false){
		return $original;
	}else{
		return false;
	}
	// return $string;
}

function get_count_kv_sql($api_id,$app_sql){
	$ret=array();
	$ret['count']=0;
	$ret['message']='';
	$ret['sql']=$app_sql;

	$kv_key = 'civitas_'.$api_id.'_count';
	$value=kget($kv_key);
	$v = intval($value);
	if($v>0) {
		$ret['count']=$v;
		$ret['message']='read kv.'.$kv_key .' ok !';
		$count = $v-1;
		kset($kv_key,$count);
		return $ret;
	}

	$count_sql = 'SELECT COUNT(*) AS num FROM ( ' . PHP_EOL . $app_sql . PHP_EOL . ') kstmp ';
	$count = get_var($count_sql);
	$ret['count']=intval($count);

	kset($kv_key,$count);

	if($count>0){		
		$ret['message'] ='read sql.count ok ! #'.$count . ' save kv ok !';
	}else{
		$ret['message']='read sql.count ok ! #'.$count;
	}
	return $ret;
}


function get_sql_count_info($app_sql){
	$ret=array();
	$ret['count']=0;
	$ret['message']='';
	$ret['sql']=$app_sql;

	$count_sql = 'SELECT COUNT(*) AS num FROM ( ' . PHP_EOL . $app_sql . PHP_EOL . ') kstmp ';
	$count = get_var($count_sql);
	$ret['count']=intval($count);
	$ret['message']='read sql.count ok ! #'.$count;
	return $ret;
}


function check_api_enable($api){
	// $ret = array();
	// $ret['api']=strtolower($api);
	$kv_key = 'shdic_api_'.strtolower($api);
	$value=kget($kv_key);
	if(empty($value)){
		kset($kv_key,'1');	//default value
		// $ret['message']=$kv_key .' init!';
		// $ret['result']=true;
		return true;
	}
	$v = intval($value);
	if($v==0){ 
		// $ret['message']=$kv_key .' NOT !';
		// $ret['result']=false;
		return false;
	}
	return true;
}

function debug_log($app,$msg){
	global $debug_log_table;
	if($debug_log_table==null){
		init_debug_log_table();
	}
	$cf1 = date('Y-m-d H:i:s');
	$sql =prepare(" INSERT INTO `shdic_crx_debug_log_table` ( `app`, `cf1`, `log`) VALUES (?s,?s,?s) "
		,array($app,$cf1,$msg));
	return run_sql($sql);
}

$debug_log_table=null;
function init_debug_log_table(){
	global $debug_log_table;

	//初始化数据库
	if(!get_data("SHOW TABLES LIKE 'shdic_crx_debug_log_table'")){
      
    	run_sql(" 
CREATE TABLE IF NOT EXISTS `shdic_crx_debug_log_table` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cf1` char(20) NOT NULL COMMENT '创建时间',
  `app` char(16) NOT NULL,
  `log` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
    	");
	}

	$debug_log_table='shdic_crx_debug_log_table';
}


function tag_val(&$arr,$tag){
	return isset( $arr[$tag] ) ? $arr[$tag] : false;
}


function more_debug($str){
	return $str;
}

function html_a_src($href,$title,$new_win=true){
	$src='<a href="'.trim($href).'" ';
	if($new_win===true){
		$src.=' target="_blank" ';
	}else{
		if(!empty($new_win)){
			$src.=' target="'.trim($new_win).'" ';
		}
	}
	$src.='>'.$title.'</a>';
	return $src;
}

function _replace_middle_str($src, $str1, $str2,$new=''){
    $pos1 = stripos($src,$str1);
    $pos2 = stripos($src,$str2);

    if ($pos1!=false && $pos2!=false && $pos1<$pos2){
        $tmp = substr($src,0,$pos1);
        $tmp .= $new;
        $tmp .= substr($src,$pos2);
        $src = $tmp;
    }else{
        $src=false;
    }
    return $src;
}

function get_app_web_root(){
    // $res_root='http://www.shdic.com/webapp/p2015eafahjjflheajmehbbnalifjgpakbpj/';
    $web_root='http://www.shdic.com';
    // $web_root='http://shdic.96.lt';
    if(!empty($web_root)){
        $web_root .="/webapp/p2015eafahjjflheajmehbbnalifjgpakbpj/";
    }
    return $web_root;
}

/**

	new code for this web app @20160506

*/
function render2016(&$outVar,$opt=null){
    global $debugMode, $time_log , $c,$a;
    global $db_table_prefix;

	if(!isset($outVar['page_title'] )) $outVar['page_title'] = 'Demo';
	if(!isset($outVar['page_desc'] )) $outVar['page_desc'] = 'page_desc';
	if(!isset($outVar['html_data'] )) $outVar['html_data'] = 'html_data is nothing';
	if(!isset($outVar['html_res_root'] )) $outVar['html_res_root'] = 'html/res/';	
	if(!isset($outVar['page_nav'] )) $outVar['page_nav'] = '<a href="index.php">首页</a> ';
	if(!isset($outVar['urlparms'] )) $outVar['urlparms']='c='.$c.'&a='.$a;
	if(!isset($outVar['login_span_src'] ))$outVar['login_span_src']=get_login_span_html();

	$outVar['outVar']=$outVar;

	// echo '<!-- DEBUG: '.__METHOD__.'()'.PHP_EOL;
	// echo print_r($opt,true);
	// echo '-->'.PHP_EOL;

	$quick_exit=false;
	if(isset($opt['ui_tpl']) && !empty($opt['ui_tpl'])){
		$ui_tpl = $opt['ui_tpl'];
	}else{
		$ui_tpl=$a;		
	}
	if(stripos($ui_tpl, 'tpl.htm')!==false){
		$tt = 'html/'.$ui_tpl;
	}else{
		$tt = 'html/web/' . $ui_tpl . '.tpl.html';	
	}
	if(file_exists($tt)){
		$src = render( $outVar , 'web' , $ui_tpl);
		
	}else{
		$quick_exit=true;
		$outVar['html_data']="tpl 未找到！";
		$src = render( $outVar , 'part' , 'notready');
	}

	$tt = render( $outVar , 'part' , 'headnav');
	$ttf = render( $outVar , 'part' , 'foot');
	$src =str_replace('<!-- $headnav$ -->', $tt, $src);
	$src =str_replace('<!-- $foot$ -->', $ttf, $src);

	$part_delimiter='<!-- $';
	$part_end_delimiter='$ -->';
	if(!$quick_exit && stripos($src, $part_delimiter)!==false){
		$tmparr=explode($part_delimiter, $src);
		foreach ($tmparr as $tmparrkey => $tmparrvalue) {
			if(stripos($tmparrvalue, $part_end_delimiter)!==false){
				//Php版本较低不支持 FABP 2016 06 10
				 //$tmp_key = explode($part_end_delimiter, $tmparrvalue)[0];
				//FAbp start
				//$tmp_key = $tmp_key[0] = explode($part_end_delimiter, $tmparrvalue);
				$ttr = explode($part_end_delimiter, $tmparrvalue);
				$tmp_key = $ttr['0'];
				//FAbp END

				$tmp_key = trim($tmp_key);
				if(!empty($tmp_key)){
					$tmp_key_file = 'html/part/' . $tmp_key . '.tpl.html';
					if(file_exists($tmp_key_file)){						
						$tmp_key_src = render( $outVar , 'part' , $tmp_key);
					}else{
						$tmp_key_src=" miss ".$tmp_key;
					}	
					$src =str_replace('<!-- $'.$tmp_key.'$ -->', $tmp_key_src, $src);
				}
			}else{
				//skip
			}
		}
	}

	if(isset($outVar['adminMode']) && isset($outVar['build_static_html']) && $outVar['build_static_html']){
		build_static_html($src,$outVar,$opt);
	}

	return $src;
}

function html_render(&$outVar,$opt=null){
	echo render2016($outVar,$opt);
	if(isset($opt['debug_src'])){
		echo $opt['debug_src'];
	}
	exit;
}

function m_implode($ch, &$ar) {
  foreach($ar as &$v) {
    if(is_array($v)) $v = m_implode($ch, $v);
  }
  return implode($ch, $ar);
}



function js_src( $js ){
	return '<script>'.PHP_EOL . $js .PHP_EOL. '</script>'.PHP_EOL;
}

function api_header(){
	$action = empty( $_REQUEST['ajax'] ) ? '' : strtolower( $_REQUEST['ajax'] );
	if($action){
		if($action=='js'){
			$ContentType ='application/x-javascript';
		}else{
			$ContentType ='text/plain';
		}
	}else{
		$ContentType ='text/html';
	}
	header("Content-Type:".$ContentType.";charset=utf-8");
}

?>
<?php
