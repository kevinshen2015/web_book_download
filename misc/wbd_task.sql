-- phpMyAdmin SQL Dump
-- version 4.0.4.2
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2016 年 06 月 08 日 08:03
-- 服务器版本: 5.6.13
-- PHP 版本: 5.4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `lpdb_jcyq`
--

-- --------------------------------------------------------

--
-- 表的结构 `wbd_task`
--

CREATE TABLE IF NOT EXISTS `wbd_task` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` char(32) NOT NULL,
  `uid` int(11) unsigned NOT NULL,
  `book_title` varchar(255) DEFAULT NULL,
  `book_url` varchar(255) DEFAULT NULL,
  `site_id` int(11) unsigned NOT NULL,
  `cf1` char(20) DEFAULT NULL,
  `cf2` char(20) DEFAULT NULL,
  `is_his` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `wbd_task`
--

INSERT INTO `wbd_task` (`id`, `uuid`, `uid`, `book_title`, `book_url`, `site_id`, `cf1`, `cf2`, `is_his`) VALUES
(1, '', 16, '最强传承', 'http://www.mangg.com/id34100/', 0, NULL, NULL, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
